'use strict';

import { isoDateToday, subtractDaysFromIsoDate } from '../src/dateUtils';
import assert from 'assert';

describe('dateUtils', function() {
  describe('isoDateToday', function() {
    it('Should correct get the date today', function() {
      const today: string = isoDateToday();
      const now:Date = new Date();
      const y:string = now.getFullYear().toString();
      const m:string = (now.getMonth() + 1).toString().padStart(2, '0');
      const d:string = now.getDate().toString().padStart(2, '0');
      const expected = `${y}-${m}-${d}`;
      assert.equal(today, expected);
    });
  });

  describe('subtractDaysFromIsoDate', function() {
    it('Should correctly remove one year from the current year', function() {
      const initialDate = '2022-05-29';
      const targetDate = '2021-05-29';
      const from:string = subtractDaysFromIsoDate(initialDate, 365);
      assert.equal(from, targetDate);
    });
  });
});

