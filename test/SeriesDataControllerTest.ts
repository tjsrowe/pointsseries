'use strict';

import { Config, ConfigSeries } from 'config/Config';

import { CategoryController } from '../controller/CategoryController';
import { ConfigManager } from '../controller/ConfigManager';
import { SeriesDataController } from '../controller/SeriesDataController';
import { SeriesDetail } from '../src/model/series';
import assert from 'assert';
import fs from 'fs';

describe('controller/SeriesDataController', () => {
  const catconfig:any = JSON.parse(fs.readFileSync('./test/data/category.config.json', 'utf-8'));
  const sdc:SeriesDataController = new SeriesDataController();
  describe('getDisplayname', () => {
    it('Should get alias', () => {
      const cfg:ConfigSeries = {
        alias: 'xyz',
        allowsDateOverride: true,
        id: 1,
      };
      const displayname:string = sdc.getDisplayname(cfg);
      assert.equal(displayname, 'xyz');
    });
  });

  describe('convertEventsToSeriesResult', () => {
    it('Should not include sensitive data when converting events.', () => {
      // const seriesEventFile:SeriesEventFile = {
      //   eventRawData
      // };
      // const seflist:SeriesEventFile[] = [];
      // seflist.push(seriesEventFile);

      // const se:SeriesEvent = { ...seriesEventFile };

      // const se:SeriesEvent[] = convertEventsToSeriesResult(seflist);
      // assert(se.)
    });
  });

  describe('getSeriesDetailFromConfig', () => {
    const cc:CategoryController = new CategoryController();
    sdc.CategoryController = cc;
    it('Should return a category list in the SeriesDetail for a config', () => {
      const config:Config = ConfigManager.getDefaultConfig(catconfig);
      config.series[0].name = 'Test series name';
      config.series[0].id = 2;
      const detail:SeriesDetail = sdc.getSeriesDetailFromConfig(config.series[0]);
      assert.equal(cc.hasCustomCategoryList(config.series[0]), true);
      assert.equal(detail.categories.length, 8, JSON.stringify(config.series[0]));
      assert.equal(detail.categories.length, 8, JSON.stringify(detail.categories));
    });
  });

  describe('getDisplayname', () => {
    it('Must have a display name in a ConfigSeries', () => {
      const cs:ConfigSeries = {
        allowsDateOverride: true,
        id: 1
      };
      assert.throws(() => sdc.getDisplayname(cs));
    });

    it('Accepts name field as display name in a ConfigSeries', () => {
      const TEST_VALUE = 'CHECK VALUE';
      const cs:ConfigSeries = {
        allowsDateOverride: true,
        id: 1,
        name: TEST_VALUE,
      };
      assert.equal(sdc.getDisplayname(cs), TEST_VALUE);
    });

    it('Accepts alias field as display name in a ConfigSeries', () => {
      const TEST_VALUE = 'CHECK VALUE';
      const cs:ConfigSeries = {
        alias: TEST_VALUE,
        allowsDateOverride: true,
        id: 1,
      };
      assert.equal(sdc.getDisplayname(cs), TEST_VALUE);
    });

    it('Accepts description field as display name in a ConfigSeries', () => {
      const TEST_VALUE = 'CHECK VALUE';
      const cs:ConfigSeries = {
        allowsDateOverride: true,
        description: TEST_VALUE,
        id: 1,
      };
      assert.equal(sdc.getDisplayname(cs), TEST_VALUE);
    });
  });

  describe('getLink', () => {
    it('Must have an alias or id to create a link', () => {
      const cs:any = {
        allowsDateOverride: true,
      };
      assert.throws(() => sdc.getLink(cs));
    });

    it('Uses an alias first if an id and alias is present', () => {
      const cs:any = {
        alias: 'foo',
        allowsDateOverride: true,
        id: 2,
      };

      const link:string = sdc.getLink(cs);
      assert.equal(link, 'foo');
    });

    it('Uses id and as link when no alias present', () => {
      const cs:any = {
        allowsDateOverride: true,
        id: 2,
      };

      const link:string = sdc.getLink(cs);
      assert.equal(link, '2');
    });
  });
});
