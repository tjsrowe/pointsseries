'use strict';

import { AbstractEntrant, EventEntrant, License } from '../src/model';

import { EntrantDataController } from '../controller/EntrantDataController';
import { EntrantMatchController } from '../controller/EntrantMatchController';
import { LicenseDataController } from '../controller/LicenseDataController';
import assert from 'assert';

type Entrant = AbstractEntrant;

function createTestEntrant(Firstname: string, Surname: string): Entrant {
  const mock: EventEntrant = {
    Category: 'Test cat',
    Firstname: Firstname,
    Result: 'DNF',
    Surname: Surname,
  };
  return mock;
}

describe('controller/EntrantMatchController', function () {
  const emc = new EntrantMatchController();
  const edc = new EntrantDataController();
  emc.setEntrantDataController(edc);
  describe('matchPartialNames', function () {
    const e1: Entrant = createTestEntrant('Joe', 'Bloggs');
    const e2: Entrant = createTestEntrant('Joe', 'Bloggs');

    it('should match an exactly equal name set', function () {
      assert.strictEqual(emc.matchNames(e1, e2), true, 'Exactly equal names should match but fails.');
    });
  });

  describe('matchPartialNamesCase', function () {
    const e1: Entrant = createTestEntrant('Joe', 'Bloggs');
    const e2: Entrant = createTestEntrant('Joe', 'BLOGGS');

    it('should match an exactly equal name set', function () {
      assert.strictEqual(emc.matchNames(e1, e2), true, 'Equal names with different case should match but fails.');
    });
  });

  describe('createFullNameFromPartials', function () {
    const e1: Entrant = createTestEntrant('Joe', 'Bloggs');

    it('Creates correct full name', function () {
      assert.strictEqual(
        emc.createFullNameFromPartials(e1),
        'Joe BLOGGS',
        'Equal names with different case should match but fails.'
      );
    });
  });

  describe('matchPartialToFull', function () {
    const e1: Entrant = createTestEntrant('Joe', 'Bloggs');

    it('should find a match when comparing a full name and field set names', function () {
      const e2: EventEntrant = {
        Category: 'Test cat',
        FullName: 'Joe Bloggs',
        Result: 'DNF',
      };
      assert.strictEqual(
        emc.comparePartialToFullName(e1, e2),
        true,
        'Matched full name to name fields should match but fails.'
      );
    });
  });

  describe('matchEntrantPerson', function() {
    it('Should match entrant with different firstname but same license', function() {
      const lic:License = {
        LicenseNumber: '1234',
        LicenseType: 'AC',
      };
      const e1: EventEntrant = {
        Category: 'Test cat',
        LicenseHash: lic,
        FullName: 'Dan Bloggs',
        Result: '7'
      };
      const e2: EventEntrant = {
        Category: 'Test cat',
        LicenseHash: lic,
        FullName: 'Daniel Bloggs',
        Result: '2'
      };

      assert(emc.matchEntrantPerson(e1, e2));
    });

    it('Should reject entrant with same name but different licenses of the same type', function() {
      const lic1:License = {
        LicenseNumber: '1234',
        LicenseType: 'AC',
      };
      const lic2:License = {
        LicenseNumber: '2345',
        LicenseType: 'AC',
      };
      const e1: EventEntrant = {
        Category: 'Test cat',
        LicenseHash: lic1,
        FullName: 'Daniel Bloggs',
        Result: '7'
      };
      const e2: EventEntrant = {
        Category: 'Test cat',
        LicenseHash: lic2,
        FullName: 'Daniel Bloggs',
        Result: '2'
      };

      assert(!emc.matchEntrantPerson(e1, e2));
    });
  });

  describe('matchEntrantLicenses', function() {
    const ldc:LicenseDataController = new LicenseDataController();
    emc._licenseDataController = ldc;
    it('Should match entrant licenses with two of same license', function() {
      const lic:License = {
        LicenseNumber: '1234',
        LicenseType: 'AC',
      };

      const e1:AbstractEntrant = {
        Category: 'Test category',
        LicenseHash: lic,
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        LicenseHash: lic,
      };

      assert(emc.matchEntrantLicenses(e1, e2));
    });

    it('Should reject licenses with different numbes', function() {
      const lic1:License = {
        LicenseNumber: '1234',
        LicenseType: 'AC',
      };
      const lic2:License = {
        LicenseNumber: '2345',
        LicenseType: 'AC',
      };

      const e1:AbstractEntrant = {
        Category: 'Test category',
        LicenseHash: lic1,
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        LicenseHash: lic2,
      };

      assert(!emc.matchEntrantLicenses(e1, e2));
    });
  });

  describe('hasMatchingSurname', function() {
    it('Should match entrant with mixed case', function() {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'TEST',
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'Test',
      };
      assert(emc.hasMatchingSurname(e1, e2), 'Surnames should be seen as matching');
    });

    it('Should match entrant with same case', function() {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'TEST',
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'TEST',
      };
      assert(emc.hasMatchingSurname(e1, e2), 'Surnames should be seen as matching');
    });

    it('Should match entrant with whitespace', function() {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        Surname: ' TEST',
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'TEST',
      };
      assert(emc.hasMatchingSurname(e1, e2), 'Surnames should be seen as matching despite leading whitespace');
    });

    it('Should not match differing surnames', function() {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'TEST',
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        Surname: 'ZORK',
      };
      assert.equal(emc.hasMatchingSurname(e1, e2), false, 'Surnames should be reported as matching');
    });
  });

  describe('matchNames', function() {
    it('Should match normal name partials', function() {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        Firstname: 'Joe',
        Surname: 'Bloggs',
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        Firstname: 'Joe',
        Surname: 'BLOGGS',
      };

      assert(emc.matchNames(e1, e2));
    });

    it('Should reject different surname', function() {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        Firstname: 'Joe',
        Surname: 'Bloggs',
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        Firstname: 'Joe',
        Surname: 'ZORK',
      };

      assert(!emc.matchNames(e1, e2));
    });
  });

  describe('matchDobIfAvailable', function() {
    it('Should match same DOB', () => {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        DOB: '2004-02-02'
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        DOB: '2004-02-02'
      };
      assert(emc.matchDobIfAvailable(e1, e2));
    });

    it('Should not match DOB values', () => {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        DOB: '2004-03-04'
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        DOB: '2004-02-02'
      };
      assert(!emc.matchDobIfAvailable(e1, e2));
    });

    it('Should match same DOB_Hash', () => {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        DOB_Hash: 'cdefab'
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        DOB_Hash: 'cdefab'
      };
      assert(emc.matchDobIfAvailable(e1, e2));
    });

    it('Should not match different DOB_Hash values', () => {
      const e1:AbstractEntrant = {
        Category: 'Test category',
        DOB_Hash: 'efabcd'
      };
      const e2:AbstractEntrant = {
        Category: 'Test category',
        DOB_Hash: 'cdefab'
      };
      assert(!emc.matchDobIfAvailable(e1, e2));
    });
  });

  describe('hasDob', function() {
    it('Should match when a raw DOB is provided', () => {
      const e:AbstractEntrant = {
        Category: 'Test category',
        DOB: '2003-03-07',
      };
      assert(edc.hasDob(e));
    });

    it('Should return false when no DOB is provided', () => {
      const e:AbstractEntrant = {
        Category: 'Test category',
      };
      assert(!edc.hasDob(e));
    });

    it('Should not match a DOB hash', () => {
      const e:AbstractEntrant = {
        Category: 'Test category',
        DOB_Hash: 'abcdef'
      };
      assert(!edc.hasDob(e));
    });
  });

  describe('hasDobHash', function() {
    it('Should match when a raw DOB is provided', () => {
      const e:AbstractEntrant = {
        Category: 'Test category',
        DOB: 'abcdef',
      };
      assert(edc.hasDob(e));
    });

    it('Should return false when no DOB is provided', () => {
      const e:AbstractEntrant = {
        Category: 'Test category',
      };
      assert(!edc.hasDob(e));
    });

    it('Should not match a plain DOB value', () => {
      const e:AbstractEntrant = {
        Category: 'Test category',
        DOB: 'abcdef'
      };
      assert(!edc.hasDobHash(e));
    });
  });
});
