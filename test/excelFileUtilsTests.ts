'use strict';
import * as assert from 'assert';
import { ExcelFileUtils } from '../src/ExcelFileUtils';

describe('ExcelFileUtil', function () {
  describe('createCacheKey()', function () {
    it('Should return full file path with sheet in filename', function () {
      assert.equal(
        'C:\\blah\\somefile.xlsx!somesheet',
        ExcelFileUtils.createCacheKey('C:\\blah\\somefile.xlsx!somesheet')
      );
    });
    it('Should return relative file path with sheet in filename', function () {
      assert.equal('somerelativefile.xlsx!somesheet', ExcelFileUtils.createCacheKey('somerelativefile.xlsx!somesheet'));
    });
    it('Should return sheet from filename not param', function () {
      assert.equal(
        'somerelativefile.xlsx!sheetinfilename',
        ExcelFileUtils.createCacheKey('somerelativefile.xlsx!sheetinfilename', 'worksheet')
      );
    });
    it('Should return sheet from param', function () {
      assert.equal(
        'somerelativefile.xlsx!paramworksheet',
        ExcelFileUtils.createCacheKey('somerelativefile.xlsx', 'paramworksheet')
      );
    });
  });
});
