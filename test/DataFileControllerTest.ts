'use strict';

import { CacheableDataFileLoad, DataFileController } from '../controller/DataFileController';
import { CSVDataFileController } from '../controller/CSVDataFileController';
import { DataFileCacheController } from '../controller/DataFileCacheController';
import { ExcelDataFileController } from '../controller/ExcelDataFileController';
import { JsonDataFileController } from '../controller/JsonDataFileController';
import assert from 'assert';
import path from 'path';

describe('DataFileController', function () {
  const dfc = new DataFileController();
  const dfcc = new DataFileCacheController();
  const xdc = new ExcelDataFileController();
  const cdc = new CSVDataFileController();
  const jdc = new JsonDataFileController();
  dfc.excelDataFileController = xdc;
  dfc.csvDataFileController = cdc;
  dfc.jsonDataFileController = jdc;
  dfc.dataFileCacheController = dfcc;

  describe('loadEvent', function () {
    it('Should load excel file from disk.', function () {
      const f: Promise<any> = dfc.loadFile('test/data/test-event.xlsx!ResultsImport');

      f.then((d) => {
        assert.notEqual(d, undefined, 'Should return some data from excel file.');
      }).catch((err) => {
        assert.fail(`Failed loading test data: ${JSON.stringify(err)}`);
      });
      // return f;
    });

    it('Should load CSV file from disk.', async function () {
      const d:any = await dfc.loadFile('test/data/test-event.csv');
      assert.notEqual(d, undefined, 'Should return some data from CSV file.');
    });

    it('Should load JSON file from disk.', async function () {
      const d:any = dfc.loadFile('test/data/test-event.json');
      assert.notEqual(d, undefined, 'Should return some data from JSON file.');
    });
  });

  describe('loadCacheDataFile', function() {
    it('shouldSkipCache', async function() {
      const dataFile:string = path.join('test', 'data', 'test-event.xlsx!ResultsImport');
      const skipCache = true;
      const cdfl:CacheableDataFileLoad = await dfc.loadCachedDataFile(dataFile, skipCache);
      assert.notEqual(cdfl.dataFileJsonData, undefined, 'Should have dataFileJsonData');
    });
  });
});
