'use strict';

import assert from 'assert';
import excelDateToISODate from '../src/excelDateToIsoDate';

describe('excelDateToISODate', function() {
  describe('createString', function() {
    it('Should create date for 2nd March', function() {
      const output:string = excelDateToISODate(44230);
      assert.equal(output, '2021-02-03');
    });
  });
});
