'use strict';

import { DateRange, Filter } from '../types';
import { getDefaultDateRange, isoDateToday, subtractDaysFromIsoDate } from '../src/dateUtils';
import { EventFilterController } from '../controller/EventFilterController';
import { ISO8601Date } from '../src/isoDateStringToDate';
import assert from 'assert';

describe('EventFilterController', function() {
  const eventFilterController:EventFilterController = new EventFilterController();

  describe('getDateRange', function() {
    const dateRange:DateRange = getDefaultDateRange();
    const filter:Filter = {
      FromDate: '2021-01-01',
      ToDate: '2021-12-31'
    };

    it('Should take filter value as precedent', function() {
      const outputRange:DateRange = eventFilterController.getDateRange(dateRange, filter);
      assert.equal(outputRange.fromDate, '2021-01-01');
      assert.equal(outputRange.toDate, '2021-12-31');
    });

    it('Should get 12 months from today when no dates specified', function() {
      const emptyFilter:Filter = {};
      const output:DateRange = eventFilterController.getDateRange(undefined, emptyFilter);
      const dateToday:ISO8601Date = isoDateToday();
      const fromDate:ISO8601Date = subtractDaysFromIsoDate(dateToday, 365);
      assert.equal(output.fromDate, fromDate);
      assert.equal(output.toDate, dateToday);
    });
  });
});
