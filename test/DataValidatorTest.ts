'use strict';

import * as assert from 'assert';

import { DataValidator } from '../src/dataValidator';

describe('DataValidator', function () {
  describe('testIsNumber()', function () {
    it('Should only accept number data types as being seen as numbers', function () {
      assert.strictEqual(DataValidator.isNumber('123'), false);
    });
    it('Should see number data types as numbers', function () {
      assert.strictEqual(DataValidator.isNumber(123), true);
    });
    it('Should reject blanks', function () {
      assert.strictEqual(DataValidator.isNumber(''), false);
    });
  });
  describe('testIsNumeric()', function () {
    it('Should see like-numbers as numbers', function () {
      assert.strictEqual(DataValidator.isNumeric('123'), true);
    });
    it('Should reject charactors', function () {
      assert.strictEqual(DataValidator.isNumeric('12gh3'), false);
    });
    it('Should reject blanks', function () {
      assert.strictEqual(DataValidator.isNumeric(''), false);
    });
  });
  describe('testIsInt()', function () {
    it('Should see numeric strings as integers', function () {
      assert.strictEqual(DataValidator.isInt('123'), true);
    });
    it('Should see number data types as numbers', function () {
      assert.strictEqual(DataValidator.isInt(123), true);
    });
    it('Should not accept floats', function () {
      assert.strictEqual(DataValidator.isInt(12.34), false);
    });
  });
});
