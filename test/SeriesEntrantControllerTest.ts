'use strict';

import { CategorySet } from '../src/utils/categorySet';
import { Series } from '../src/utils/series';
import { SeriesEntrantController } from '../controller/SeriesEntrantController';
import { createCategorySet } from '../controller/CategoryController';
import entrantdata from './data/entrantdata.json';

const sec:SeriesEntrantController = new SeriesEntrantController();

const addEntrants = (series:Series, count?:number):void => {
  if (count == undefined) {
    count = entrantdata.length;
  }
  for (let i = 0;i < count;i++) {
    series.addEntrant(entrantdata[i]);
  }
};

describe('controller/SeriesEntrantController', function() {
  describe('sortSeriesEntrant', function() {
    const cs:CategorySet = createCategorySet([
      'Elite Men',
      'Junior Men',
      'Junior Women'
    ]);

    // it('Should sort entrant list by category and points', function() {
    //   const series:Series = new Series(cs);
    //   addEntrants(series);

    //   assert.equal(series.Entrants[0].Firstname, 'PersonA');
    //   assert.equal(series.Entrants[1].Firstname, 'PersonC');
    //   assert.equal(series.Entrants[2].Firstname, 'PersonB');
    //   sec.sortSeriesEntrants(series);
    //   assert.equal(series.Entrants[0].Firstname, 'PersonA');
    //   assert.equal(series.Entrants[1].Firstname, 'PersonB');
    //   assert.equal(series.Entrants[2].Firstname, 'PersonC');
    // });

    // it('Should sort two entrants in the recognised category', function() {
    //   const series:Series = new Series(cs);
    //   cs.addCategoryByName('Elite Men');
    //   cs.addCategoryByName('Junior Men');
    //   cs.addCategoryByName('Junior Women');

    //   series.addEntrant(entrantdata[1]);
    //   series.addEntrant(entrantdata[2]);
    //   sec.sortSeriesEntrants(series);
    //   assert.equal(series.Entrants[0].Firstname, 'PersonB');
    //   assert.equal(series.Entrants[1].Firstname, 'PersonC');
    // });
  });
});

