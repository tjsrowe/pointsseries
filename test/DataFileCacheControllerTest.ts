'use strict';

import * as os from 'os';
import * as path from 'path';

import { Config, ConfigManager } from '../controller/ConfigManager';
import { DataFileCacheController } from '../controller/DataFileCacheController';

import assert from 'assert';

const DFCC = new DataFileCacheController();

describe('DataFileCacheController', function () {
  describe('isCachedInMemory()', function () {
    const config: Config = ConfigManager.getDefaultConfig({});
    const testFilename = 'foo.json';
    const testObjectData: any = {
      foo: 'blah',
      something: 1,
    };
    it('Should verify that some data is cached in memory after being persisted', function () {
      DFCC.config = config;
      DFCC.cacheFile(testFilename, testObjectData);
      assert(DFCC.isCachedInMemory(testFilename));
    });
    it('Should verify that some data is cached to disk after being persisted', function () {
      DFCC.config = config;
      DFCC.cacheFile(testFilename, testObjectData);
      assert(DFCC.isCachedOnDisk(testFilename));
    });
    it('Should verify that a cache hit is found in either area', function () {
      DFCC.config = config;
      DFCC.cacheFile(testFilename, testObjectData);
      assert(DFCC.isCached(testFilename));
    });
  });

  describe('getCachePathForFile()', function () {
    const configWithCachePath: Config = ConfigManager.getDefaultConfig({
      cacheLocation: 'X:\\cacheDir',
    });

    it('Should not append two paths', function () {
      DFCC.config = configWithCachePath;
      assert.strictEqual(
        DFCC.getCachePathForFile('Y:\\foo\\dataFile.xlsx'),
        'X:\\cacheDir' + path.sep + 'Y__foo_dataFile_xlsx.json',
        'Path does not match expected'
      );
    });
    it('Should not append two paths when sheetname given', function () {
      DFCC.config = configWithCachePath;
      assert.strictEqual(
        DFCC.getCachePathForFile('Y:\\foo\\dataFile.xlsx!ResultImport'),
        'X:\\cacheDir' + path.sep + 'Y__foo_dataFile_xlsx_ResultImport.json',
        'Path does not match expected'
      );
    });

    const configWithNoCachePath: Config = ConfigManager.getDefaultConfig({
      cacheLocation: undefined,
    });
    it('Should work with no cache path', function () {
      DFCC.config = configWithNoCachePath;
      assert.strictEqual(
        DFCC.getCachePathForFile('Y:\\foo\\dataFile.xlsx!ResultImport'),
        os.tmpdir() + path.sep + 'Y__foo_dataFile_xlsx_ResultImport.json',
        'Path does not match expected'
      );
    });

    const configWithCacheLocRelative: Config = ConfigManager.getDefaultConfig({
      cacheLocation: 'cacheDir/',
    });
    it('Should work with only a relative cache dir', function () {
      DFCC.config = configWithCacheLocRelative;
      assert.strictEqual(
        DFCC.getCachePathForFile('Y:\\foo\\dataFile.xlsx!ResultImport'),
        'cacheDir/Y__foo_dataFile_xlsx_ResultImport.json',
        'Path does not match expected'
      );
    });

    const configWithCacheLocTrailingSlash: Config = ConfigManager.getDefaultConfig({
      cacheLocation: 'X:\\cacheDir\\',
    });
    it('Should work if cache dir is given with a trailing slash', function () {
      DFCC.config = configWithCacheLocTrailingSlash;
      assert.strictEqual(
        DFCC.getCachePathForFile('Y:\\foo\\dataFile.xlsx!ResultImport'),
        'X:\\cacheDir\\Y__foo_dataFile_xlsx_ResultImport.json',
        'Path does not match expected'
      );
    });

    const configWithRootCacheLoc: Config = ConfigManager.getDefaultConfig({
      cacheLocation: '/rootCache/',
    });
    it('Should work with cache set to a root dir', function () {
      DFCC.config = configWithRootCacheLoc;
      assert.strictEqual(
        DFCC.getCachePathForFile('Y:\\foo\\dataFile.xlsx!ResultImport'),
        '/rootCache/Y__foo_dataFile_xlsx_ResultImport.json',
        'Path does not match expected'
      );
    });
  });

  describe('sanitiseFilePath()', function () {
    it('Should not remove .json extension', function () {
      assert.strictEqual(DFCC.sanitiseFilePath('somefile.xlsx'), 'somefile_xlsx.json');
      assert.strictEqual(DFCC.sanitiseFilePath('somexljson.xlsx.json'), 'somexljson_xlsx.json');
      assert.strictEqual(DFCC.sanitiseFilePath('somejson.json'), 'somejson.json');
    });

    it('Should escape path separators', function () {
      assert.strictEqual(DFCC.sanitiseFilePath('dir/blah.json'), 'dir_blah.json');
      assert.strictEqual(DFCC.sanitiseFilePath('dir\\blah.json'), 'dir_blah.json');
    });

    it('Should escape drive letter', function () {
      assert.strictEqual(DFCC.sanitiseFilePath('X:\\onDrive\\blah.json'), 'X__onDrive_blah.json');
      assert.strictEqual(DFCC.sanitiseFilePath('X:\\onDrive\\excel.xlsx'), 'X__onDrive_excel_xlsx.json');
    });
  });
});
