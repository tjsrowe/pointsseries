'use strict';

import { DataFileCacheController } from '../controller/DataFileCacheController';
import { DataFileController } from '../controller/DataFileController';
import { EventDataController } from '../controller/EventDataController';
import { EventDataDetail } from '../src/model';
import { ExcelDataFileController } from '../controller/ExcelDataFileController';
import assert from 'assert';

describe('EventDataController', function () {
  const edc = new EventDataController();
  const dfc = new DataFileController();
  const dfcc = new DataFileCacheController();
  const xdc = new ExcelDataFileController();
  edc.DataFileController = dfc;
  dfc.excelDataFileController = xdc;
  dfc.dataFileCacheController = dfcc;

  describe('loadEvent', function () {
    const detail: EventDataDetail = {
      cacheFileName: 'test-cache.json',
      format: 'XCO',
      id: 'xy123',
      isFinal: false,
      location: 'location',
      name: 'test data',
      resultFileName: 'test-event.xlsx!ResultsImport',
      state: 'XXX',
      // pointsFactor: 1.0,
    };

    it('Should have more than one entrant.', async function () {
      const edd:EventDataDetail = await edc.requestEvent(detail, 'test/data');
      assert.equal(edd != undefined, true);
    });
  });
});
