import { FileResultDataAliases, ResultDataAliases } from 'config/Config';

import { ControllerManager } from '../controller/ControllerManager';
import { DataFileValidationController } from '../controller/DataFileValidationController';
import assert from 'assert';

describe('DataFileValidationController', () => {
  const cm: ControllerManager = new ControllerManager();
  cm.createControllers();
  const dfvc: DataFileValidationController = cm.DataFileValidationController;
  it('Should find Name as fullName column', () => {
    const testColumns = ['NationalCupCategory', 'OverallRank', 'Rank', 'Name', 'DOB'];
    const fieldSearchSet: ResultDataAliases = {
      FullName: ['Name']
    } as ResultDataAliases;

    const aliases: FileResultDataAliases = dfvc.matchFieldsToAvailableColumns(testColumns, fieldSearchSet);
    assert.notEqual(aliases.FullName, undefined);
  });
});

describe('findMatchingFieldNameFromAlternatives', () => {
  const cm: ControllerManager = new ControllerManager();
  cm.createControllers();
  const dfvc: DataFileValidationController = cm.DataFileValidationController;

  it('Should find matching primary name', () => {
    const testColumns = ['NationalCupCategory', 'OverallRank', 'Rank', 'Name', 'DOB'];
    const foundMatch: string|undefined = dfvc.findMatchingFieldNameFromAlternatives(testColumns, 'Name', ['FullName']);

    assert.equal(foundMatch, 'Name');
  });

  it('Should find matching secondary name', () => {
    const testColumns = ['NationalCupCategory', 'OverallRank', 'PIC', 'Name', 'DOB'];
    const foundMatch: string|undefined = dfvc.findMatchingFieldNameFromAlternatives(
      testColumns, 'Rank', ['FinishPosition', 'PIC', 'Result']);

    assert.equal(foundMatch, 'PIC');
  });

  it('Should find matching secondary name not matching case', () => {
    const testColumns = ['NationalCupCategory', 'OverallRank', 'PIC', 'FirstName', 'DOB'];
    const foundMatch: string|undefined = dfvc.findMatchingFieldNameFromAlternatives(
      testColumns, 'Firstname', ['GivenName']);

    assert.equal(foundMatch, 'FirstName');
  });
});
