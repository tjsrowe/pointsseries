'use strict';

import { License } from '../src/model';
import { LicenseDataController } from '../controller/LicenseDataController';
import assert from 'assert';

describe('LicenseDataControllerTest', () => {
  const ldc:LicenseDataController = new LicenseDataController();

  describe('licenseMatches', () => {
    const lic:License = {
      LicenseNumber: '1234',
      LicenseType: 'AC',
    };
    it('Should match same license', () => {
      assert.equal(ldc.licenseMatches(lic, lic), true);
    });

    it('Should reject with missing parameter', function() {
      assert.equal(ldc.licenseMatches(lic, undefined!), false);
      assert.equal(ldc.licenseMatches(undefined!, lic), false);
      assert.equal(ldc.licenseMatches(undefined!, undefined!), false);
    });

    it('Should reject if type does not match', function() {
      const lic2:License = {
        LicenseNumber: '1234',
        LicenseType: 'MTBA',
      };
      assert.equal(ldc.licenseMatches(lic, lic2), false);
      assert.equal(ldc.licenseMatches(lic2, lic), false);
    });

    it('Should reject if number does not match', function() {
      const lic2:License = {
        LicenseNumber: '1523',
        LicenseType: 'AC',
      };
      assert.equal(ldc.licenseMatches(lic, lic2), false);
      assert.equal(ldc.licenseMatches(lic2, lic), false);
    });
  });

  describe('hasMultipleOfSingleLicense', function() {
    it('Should report correctly on single licenses.', function() {
      const lic2:License = {
        LicenseNumber: '1523',
        LicenseType: 'AC',
      };
      assert.equal(ldc.hasMultipleOfSingleLicense(lic2), false);
    });

    it('Should report correctly on multiple licenses of the same type.', function() {
      const lic1:License = {
        LicenseNumber: '1523',
        LicenseType: 'AC',
      };
      const lic2:License = {
        LicenseNumber: '5678',
        LicenseType: 'AC',
      };

      assert.equal(ldc.hasMultipleOfSingleLicense([lic1, lic2]), true);
    });

    it('Should report correctly on single licenses of different types.', function() {
      const lic1:License = {
        LicenseNumber: '1523',
        LicenseType: 'AC',
      };
      const lic2:License = {
        LicenseNumber: '5678',
        LicenseType: 'MTBA',
      };
      assert.equal(ldc.hasMultipleOfSingleLicense([lic1, lic2]), false);
    });
  });
});
