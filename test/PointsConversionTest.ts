'use strict';

import { assert } from 'console';
import { convertPointsToMap } from '../src/pointsConversion';

describe('utils/pointsConversion', () => {
  const pointsData:any = {
    1: 200,
    2: 160,
    3: 130,
    DNF: 5,
    DNS: 1,
    DQ: 0,
    DSQ: 0,
  };

  describe('convertPointsToMap', () => {
    it('Should not have any NaN values in map', () => {
      const converted:Map<string|number, number>|undefined = convertPointsToMap(pointsData);
      assert(converted);
      assert(converted!.has('NaN') == false);
      assert(converted!.has(NaN) == false);
    });

    it('Should generate a Map with the above points', () => {
      const converted:Map<string|number, number>|undefined = convertPointsToMap(pointsData);
      assert(converted !== undefined);
      assert(converted!.size == 7);
    });

    it('Should convert map with string indexes to an output map', () => {
      const pointsMapWithStrings:Map<string|number, number> = new Map<string|number, number>();
      pointsMapWithStrings.set('1', 200);
      pointsMapWithStrings.set('2', 160);
      pointsMapWithStrings.set('3', 130);
      pointsMapWithStrings.set('DNF', 5);
      pointsMapWithStrings.set('DNS', 1);
      pointsMapWithStrings.set('DQ', 0);
      pointsMapWithStrings.set('DSQ', 0);

      const converted:Map<string|number, number>|undefined = convertPointsToMap(pointsMapWithStrings);
      assert(converted !== undefined);
      assert(converted!.size == 7);
      assert(converted?.get(2) == 160);
      assert(converted?.get('2') == 160);
    });

    it('Should convert map with number indexes to an output map', () => {
      const pointsMapWithNumbers:Map<string|number, number> = new Map<string|number, number>();
      pointsMapWithNumbers.set(1, 200);
      pointsMapWithNumbers.set(2, 160);
      pointsMapWithNumbers.set(3, 130);
      pointsMapWithNumbers.set('DNF', 5);
      pointsMapWithNumbers.set('DNS', 1);
      pointsMapWithNumbers.set('DQ', 0);
      pointsMapWithNumbers.set('DSQ', 0);

      const converted:Map<string|number, number>|undefined = convertPointsToMap(pointsMapWithNumbers);
      assert(converted !== undefined);
      assert(converted!.size == 7);
      assert(converted?.get(2) == 160);
      assert(converted?.get('2') == 160);
    });
  });
});
