'use strict';

import { Category, CategoryKey } from '../src/model';
import {
  CategoryController,
  compareByOrder,
  createIndexMapFromList,
} from '../controller/CategoryController';
import {
  createCategory,
  createCategoryHashId,
  createNameWithGender,
  removeGenderCategoryName
} from '../src/utils/category';

import { CategorySet } from '../src/utils/categorySet';
import { Config } from '../config/Config';
import { ConfigManager } from '../controller/ConfigManager';
import { Gender } from '../src/model/entrant';
import assert from 'assert';
import catconfig from './data/category.config.json';

describe('controller/CategoryController', function() {
  const sampleCats:Category[] = [
    {
      alternativeNames: [],
      gender: Gender.MEN,
      name: 'Elite',
    },
    {
      gender: Gender.WOMEN,
      name: 'Elite',
    },
    {
      alternativeNames: ['Masters 1', 'Masters 2', 'Masters 3', 'Masters 4'],
      gender: Gender.MEN,
      name: 'Masters',
    },
    {
      alternativeNames: [],
      gender: Gender.WOMEN,
      name: 'Masters',
    }
  ];
  const sampleCatsMap:Map<string, number> = createIndexMapFromList(sampleCats);

  describe('removeGenderCategoryName', function() {
    it('Should return a stripped category name', function() {
      const output = removeGenderCategoryName('Elite Men');
      assert.equal(output, 'Elite');
    });

    it('Should return a full unknown category name', function() {
      const output = removeGenderCategoryName('Elite Blerg');
      assert.equal(output, 'Elite Blerg');
    });

    it('Should not strip men from women', function() {
      const output = removeGenderCategoryName('Elite Women');
      assert.equal(output, 'Elite');
    });
  });

  describe('createCategory', function() {
    it('Should create Mens category from name', function() {
      const cat:Category = createCategory('Elite men');
      assert.equal(cat.gender, Gender.MEN);
    });

    it('Should use first category name', function() {
      const cat:Category = createCategory('Elite men');
      assert.equal(cat.name, 'Elite');
    });

    it('Should use first alternative category name', function() {
      const cat:Category = createCategory(['Elite men', 'Under 23 Men']);
      assert.equal(cat.name, 'Elite');
      assert.notEqual(cat.alternativeNames, undefined);
      assert.equal(cat.alternativeNames![0], 'Under 23');
    });

    it('Should ensure womens categories aren\'t truncated by the word \'men\'', function() {
      const cat:Category = createCategory(['Elite Women', 'Under 23 Women']);
      assert.equal(cat.name, 'Elite');
      assert.notEqual(cat.alternativeNames, undefined);
      assert.equal(cat.alternativeNames![0], 'Under 23');
    });

    it('Should ensure female categories aren\'t truncated by the word \'male\'', function() {
      const cat:Category = createCategory(['Elite Female', 'Under 23 Female']);
      assert.equal(cat.name, 'Elite');
      assert.notEqual(cat.alternativeNames, undefined);
      assert.equal(cat.alternativeNames![0], 'Under 23');
    });

    it('Should use multiple alternative category names', function() {
      const cat:Category = createCategory(
        [
          'Masters Women',
          'Masters 1 Women',
          'Masters 2 Women',
          'Masters 3 Women',
          'Masters 4 Women',
          'Masters 5 Women',
          'Masters 6 Women'
        ]
      );
      assert.equal(cat.name, 'Masters');
      assert.equal(cat.gender, Gender.WOMEN);
      assert.notEqual(cat.alternativeNames, undefined);
      assert.equal(cat.alternativeNames![3], 'Masters 4');
    });
  });

  describe('getCategoryList', function() {
    const cc:CategoryController = new CategoryController();

    const config:Config = ConfigManager.getDefaultConfig(catconfig);

    it('Should get the correct number of category entries for a series', function() {
      cc.config = config;
      const list:Category[] = cc.getCategoryList(config.series[0]);
      assert.equal(list.length, 8);
    });

    it('Should get the correct number of category entries for a global config', function() {
      const configWithNoSeriesCategories:Config = {
        ...config
      };
      configWithNoSeriesCategories.series[0].categories = undefined;
      cc.config = configWithNoSeriesCategories;
      const list:Category[] = cc.getCategoryList(configWithNoSeriesCategories.series[0]);
      assert.equal(list.length, 6);
    });

    it('Should get default categories when none defined in config', function() {
      const configWithNoConfigCategories:Config = {
        ...config
      };
      configWithNoConfigCategories.series[0].categories = undefined;
      configWithNoConfigCategories.categories = undefined;
      cc.config = configWithNoConfigCategories;
      const list:Category[] = cc.getCategoryList(configWithNoConfigCategories.series[0]);
      assert.equal(list.length, 32);
    });
  });

  describe('createNameWithGender', function() {
    it('Should create a concatenated string when provided a gender', function() {
      const key:string = createNameWithGender('Elite', Gender.WOMEN);
      assert.equal(key, 'elite_WOMEN');
    });

    it('Should create a concatenated string with no gender', function() {
      const key:string = createNameWithGender('Masters');
      assert.equal(key, 'masters');
    });
  });

  describe('createCategoryHashId', function() {
    it('Should create key based on primary name and gender', function() {
      const eliteCat:Category = {
        alternativeNames: ['Open', 'Under 23'],
        gender: Gender.MEN,
        name: 'Elite',
      };

      const key:CategoryKey = createCategoryHashId(eliteCat);
      assert.equal(key, 'elite_MEN');
    });
  });

  describe('createIndexMapFromList', function() {
    it('Should create a searchable hashmap', function() {
      const psn:number|undefined = sampleCatsMap.get('elite_WOMEN');
      assert.equal(psn, 1);
    });
    it('Should find a correct entry with an alternative name', function() {
      const psn:number|undefined = sampleCatsMap.get('masters 3_MEN');
      assert.equal(psn, 2);
    });
    it('Should not find non-existent category', function() {
      const psn:number|undefined = sampleCatsMap.get('masters 7_WOMEN');
      assert.equal(psn, undefined);
    });
  });

  describe('compareByOrder', function() {
    it('Should return equal values', function() {
      const result:number = compareByOrder(75, 75);
      assert.equal(result, 0);
    });
    it('Should report first value bigger when second undefined', function() {
      const result:number = compareByOrder(75, undefined);
      assert.equal(result, -75);
    });
    it('Should report second value bigger when first undefined', function() {
      const result:number = compareByOrder(undefined, 75);
      assert.equal(result, 75);
    });
    it('Should report equal when both undefined', function() {
      const result:number = compareByOrder(undefined, undefined);
      assert.equal(result, 0);
    });
  });

  describe('compareCategoryOrder', function() {
    it('Should report first category having smaller order', function() {
      const comparison:number = CategoryController.compareCategoryOrder(sampleCatsMap, sampleCats[1], sampleCats[2]);
      assert.equal(comparison, -1);
    });
  });

  describe('compareCategoryOrderNames', function() {
    it('Should report first category having smaller order', function() {
      const key1:CategoryKey = createCategoryHashId(sampleCats[1]);
      const key2:CategoryKey = createCategoryHashId(sampleCats[2]);
      const comparison:number = CategoryController.compareCategoryOrderNames(sampleCatsMap, key1, key2);
      assert.equal(comparison, -1);
    });

    it('Should report first category having larger order', function() {
      const key1:CategoryKey = createCategoryHashId(sampleCats[3]);
      const key2:CategoryKey = createCategoryHashId(sampleCats[2]);
      const comparison:number = CategoryController.compareCategoryOrderNames(sampleCatsMap, key1, key2);
      assert.equal(comparison, 1);
    });

    it('Should report non-existent category having smaller order', function() {
      const sampleCatCopy:Map<string, number> = new Map<string, number>(sampleCatsMap);
      const nonExistantCat:Category = {
        alternativeNames: [],
        gender: Gender.MIXED,
        name: 'Fabricated',
      };
      const key:CategoryKey = createCategoryHashId(nonExistantCat);
      sampleCatCopy.set(key, 4);
      const key1:CategoryKey = createCategoryHashId(sampleCats[3]);
      const key2:CategoryKey = createCategoryHashId(nonExistantCat);
      const comparison:number = CategoryController.compareCategoryOrderNames(sampleCatCopy, key1, key2);
      assert.equal(comparison, -1);
    });

    it('Should throw exception when comparing list with non-existent category', function() {
      const nonExistantCat:Category = {
        alternativeNames: [],
        gender: Gender.MIXED,
        name: 'Fabricated',
      };
      const key1:CategoryKey = createCategoryHashId(sampleCats[3]);
      const key2:CategoryKey = createCategoryHashId(nonExistantCat);
      assert.throws(() => {
        CategoryController.compareCategoryOrderNames(sampleCatsMap, key1, key2);
      });
    });

    it('Should compare a category using an altname', function() {
      const key1:CategoryKey = createCategoryHashId(sampleCats[3]);
      const comparison:number = CategoryController.compareCategoryOrderNames(sampleCatsMap, key1, 'masters 2_MEN');
      assert.equal(comparison, 1);
    });
  });

  describe('categoryMatches', function() {
    it('Should match category', function() {
      const set:CategorySet = new CategorySet();
      const categoryName1 = 'Elite Men';
      const categoryName2 = 'Elite Men';
      assert(CategoryController.categoryMatches(set, categoryName1, categoryName2));
    });

    it('Should not match same category for different genders', function() {
      const set:CategorySet = new CategorySet();
      const categoryName1 = 'Elite Men';
      const categoryName2 = 'Elite Women';
      assert(!CategoryController.categoryMatches(set, categoryName1, categoryName2));
      assert(!CategoryController.categoryMatches(set, categoryName2, categoryName1));
    });

    it('Should not match when only one category has gender', function() {
      const set:CategorySet = new CategorySet();
      const categoryName1 = 'Elite Men';
      const categoryName2 = 'Elite';
      assert(!CategoryController.categoryMatches(set, categoryName1, categoryName2));
      assert(!CategoryController.categoryMatches(set, categoryName2, categoryName1));
    });
  });
});
