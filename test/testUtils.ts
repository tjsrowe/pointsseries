'use strict';

import { CacheSet, createCacheSetFromZip } from '../controller/memcache/utils';
import { SeriesDataDetail, SeriesResult } from '../src/model/series';
import assert, { fail } from 'assert';
import fs, { PathLike } from 'fs';

import { ControllerManager } from '../controller/ControllerManager';
import { DateRange } from 'types';
import { EventDataDetail } from '../src/model/event';
import { SeriesDataController } from '../controller/SeriesDataController';
import { getDefaultDateRange } from '../src/dateUtils';
import path from 'path';

describe('controller/memcache/utils', function() {
  describe('createCacheSetFromZip', function() {
    const filePath:PathLike = path.join('test/data/export.zip');
    const exists:boolean = fs.existsSync(filePath);
    assert.equal(exists, true);
    const buf = fs.readFileSync(filePath);
    const cm:ControllerManager = new ControllerManager();
    cm.createControllers();
    const sdc:SeriesDataController = cm.SeriesDataController;

    it('Should parse a zip file and return a cache set with files', async function() {
      const cacheSet:CacheSet = await createCacheSetFromZip(buf);
      const allEvents:EventDataDetail[] = await cacheSet.events.getAll();
      assert.notEqual(allEvents, undefined);
      assert.notEqual(allEvents.length, 0);
    });

    it('Should read and parse a full series.', async function() {
      const cacheSet:CacheSet = await createCacheSetFromZip(buf);
      const dateRange: DateRange = getDefaultDateRange();
      cm.EventDataController.DataSource = cacheSet.events;
      cm.SeriesDataController.DataSource = cacheSet.series;

      cacheSet.series.get(3).then((detail:SeriesDataDetail) => {
        sdc.convertDataToSeriesResult(detail, dateRange)
          .then((convertedSeries:SeriesResult) => {
            console.log(convertedSeries);
          }).catch((err) => {
            fail(err);
          });
      }).catch((err) => fail(err));
    });
  });
});
