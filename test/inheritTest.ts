// 'use strict';

// import assert from 'assert'

// interface A extends B {
//   propA:string,
// }

// interface B {
//   propB:string,
// }

// describe('Inherited types test', function() {
//   it('Should not copy properties of type A to new type B', function() {
//     const myA:A = {
//       propA: 'foo',
//       propB: 'zork',
//     };

//     const myB = {
//       ...myA
//     };

//     assert.equal(myB.propA, undefined);
//   });
// });
