'use strict';

import { CategorySet } from '../src/utils/categorySet';
import assert from 'assert';
import { createCategorySet } from '../controller/CategoryController';

describe('CategorySet', () => {
  describe('contructor', () => {
    const categories:string[] = [
      'Elite Men',
      'Junior Men',
      'Junior Women'
    ];
    const cs:CategorySet = createCategorySet(categories);

    it('Should have the same number of categories in the set list', () => {
      assert(cs.categoryCount == categories.length);
    });

    it('Should have a start order for each category', () => {
      assert(cs.Map.get('elite men') != undefined);
      assert(cs.Map.get('junior men') != undefined);
      assert(cs.Map.get('junior women') != undefined);
    });

    it('Should have added these keys in lowercase order for each category', () => {
      assert(cs.Map.get('Elite Men') == undefined);
      assert(cs.Map.get('Junior Men') == undefined);
      assert(cs.Map.get('Junior Women') == undefined);
    });
  });
});
