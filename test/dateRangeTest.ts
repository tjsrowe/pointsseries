'use strict';

import { DateRange } from '../types';
import assert from 'assert';
import { isInDateRange } from '../src/isInDateRange';

describe('isInDateRange', function () {
  it('Should accept missing date range', function () {
    assert.strictEqual(isInDateRange('2021-07-20', undefined), true);
  });
  it('Should accept date range structures with no dates', function () {
    const dateRange:DateRange = {};
    assert.strictEqual(isInDateRange('2021-07-20', dateRange), true);
  });
  it('Should accept date range structures with empty structure', function () {
    assert.strictEqual(isInDateRange('2021-07-20', undefined), true);
  });
  it('Should accept date range structures with only start date', function () {
    const dateRange:DateRange = {
      fromDate: '2021-01-01'
    };
    assert.strictEqual(isInDateRange('2021-07-20', dateRange), true);
  });
  it('Should accept date range structures with only end date', function () {
    const dateRange:DateRange = {
      toDate: '2021-12-31'
    };
    assert.strictEqual(isInDateRange('2021-07-20', dateRange), true);
  });
  it('Should reject date range structures with only start date starting after specified date', function () {
    const dateRange:DateRange = {
      fromDate: '2021-08-15'
    };
    assert.strictEqual(isInDateRange('2021-07-20', dateRange), false);
  });
  it('Should reject date range structures with only end date starting before specified date', function () {
    const dateRange:DateRange = {
      toDate: '2021-06-30'
    };
    assert.strictEqual(isInDateRange('2021-07-20', dateRange), false);
  });
  it('Should accept date range between given dates', function () {
    const dateRange:DateRange = {
      fromDate: '2020-12-31',
      toDate: '2022-12-31',
    };
    assert.strictEqual(isInDateRange('2021-07-20', dateRange), true);
  });
});
