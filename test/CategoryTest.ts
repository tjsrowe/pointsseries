'use strict';

import { Category, CategoryKey } from '../src/model/category';
import { createCategory, createLookupKeyForString, removeGenderCategoryName } from '../src/utils/category';

import { Gender } from '../src/model/entrant';
import assert from 'assert';

describe('Category', () => {
  describe('createLookupKeyForString', () => {
    it('Should return the same name with corrected formatting when a lowercase formatted name is passed in', () => {
      const trimmedName = 'elite_men';
      const key: CategoryKey = createLookupKeyForString(trimmedName);
      assert.equal('elite_MEN', key);
    });
  });


  describe('createCategory', () => {
    it('Should create a category from a lowercase fomatted name', () => {
      const trimmedName = 'elite_men';
      const output:Category = createCategory(trimmedName);
      assert.equal('elite', output.name);
      assert.equal(Gender.MEN, output.gender);
    });
  });

  describe('removeGenderCategoryName', () => {
    it('Should remove underscores from category name with gender', () => {
      const trimmedName = 'elite_men';
      const output:string = removeGenderCategoryName(trimmedName);
      assert.equal('elite', output);
    });
  });
});
