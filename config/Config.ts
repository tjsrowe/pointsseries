'use strict';

import * as os from 'os';

import { ISO8601Date } from '../types';

export class Config {
  eventDataFileLocation!: string;
  eventCacheOutputLocation!: string;
  cacheLocation!: string;
  licenseTypes!: string[];
  licenseFields!: string[];
  dobFields!: string[];
  resultDataAliases!: ResultDataAliases;
  series!: ConfigSeries[];
  categories?: string[];
  maxCatId?: number;
  seriesCategoryLookupHash?: Map<string, number>;
  tempUploadPath: string = process.env.TEMP_UPLOAD_PATH || os.tmpdir();
  apiServer?: APIServer;
}

export type HttpsSettings = {
  port: 443;
  keyFile: string;
  certFile: string;
};

export class APIServer {
  httpPort = 80;
  https?: HttpsSettings;
  allowedOrigins!: string[];
}

export interface FileResultDataAliases {
  Result?: string;
  Firstname?: string;
  Surname?: string;
  Category?: string;
  IsIneligible?: string;
  FullName?: string;
}

export class ResultDataAliases {
  Result?: string[];
  Firstname?: string[];
  Surname?: string[];
  Category?: string[];
  IsIneligible?: string[];
  FullName?: string[];
}

export interface Filter {
  Format?: string[];
  FromDate?: ISO8601Date;
  ToDate?: ISO8601Date;
}

export class ConfigSeries {
  id!: number;
  fileId?: number;
  alias?: string;
  allowsDateOverride=true;
  description?: string;
  eventsList?: string;
  points?: string;
  pointsSystems?: any;
  excludedRiders?: string;
  filter?: Filter;
  maxCatId?: number;
  name?: string;
  categories?: string[];
  seriesCategoryLookupHash?: Map<string, number>;
  enabled?: boolean;
  configFile?: string;
  configFileLastModifiedDate?: Date;
}
