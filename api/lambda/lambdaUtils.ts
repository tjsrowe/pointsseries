'use strict';

import { Config, ConfigManager } from '../../controller/ConfigManager';

import { APIGatewayProxyEventQueryStringParameters } from 'aws-lambda/trigger/api-gateway-proxy';
import { DateRange } from '../../types';
import { isoDateToday } from '../../src/dateUtils';

export const lambdaDateRangeParams = (params: APIGatewayProxyEventQueryStringParameters | null): DateRange => {
  const outputDateRange: DateRange = {};
  if (params?.fromDate) {
    outputDateRange.fromDate = params.fromDate;
  }
  if (params?.toDate) {
    outputDateRange.toDate = params.toDate;
  } else {
    outputDateRange.toDate = isoDateToday();
  }
  return outputDateRange;
};

export const getAwsConfig = (): Config => {
  return ConfigManager.getDefaultConfig();
};
