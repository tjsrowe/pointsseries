'use strict';

import { Request, Response } from 'express';
import fs from 'fs';

class AssetRest {
  // eslint-disable-next-line @typescript-eslint/ban-types
  init(restCallAdder: Function) {
    restCallAdder('/assets/:asset', function (request: Request, response: Response) {
      const asset = request.params.asset;
      response.sendFile(asset, { root: process.cwd() + '/assets' });
    });
    restCallAdder('/js/:jsfile', function (request: Request, response: Response) {
      const js = request.params.jsfile;
      if (fs.existsSync(js)) {
        response.sendFile(js, { root: process.cwd() + '/js' });
      } else {
        response.status(404);
        response.end();
      }
    });
    restCallAdder('/changelog.html', function (request: Request, response: Response) {
      response.sendFile('changelog.html', { root: process.cwd() + '/html' });
    });
    restCallAdder('/changelog', function (request: Request, response: Response) {
      response.sendFile('changelog.html', { root: process.cwd() + '/html' });
    });
  }
}

export default AssetRest;
