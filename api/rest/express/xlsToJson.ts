'use strict';

import { Express, Request, Response } from 'express';

import { DateRange } from '../../../types';
import { ExcelSeriesController } from '../../../controller/ExcelSeriesController';
import { parseDateRangeParams } from '../../../src/parseDateRangeParams';

class XLSToJson {
  _excelSeriesController!: ExcelSeriesController;
  set ExcelSeriesController(esc: ExcelSeriesController) {
    this._excelSeriesController = esc;
  }

  init(rest: Express): void {
    const edc: ExcelSeriesController = this._excelSeriesController;
    rest.get('/xlsToJson/:id', function (request: Request, response: Response) {
      const fileId = request.params.id;
      const dateRange: DateRange = parseDateRangeParams(request.params, request.query);

      edc.getExcelFromSeries(response, fileId, dateRange);
    });

    rest.get('/series/:id.xlsx', function (request: Request, response: Response) {
      const fileId = request.params.id;
      const dateRange: DateRange = parseDateRangeParams(request.params, request.query);
      edc
        .getExcelFromSeries(response, fileId, dateRange, () => {
          response.writeHead(200, {
            'Content-Disposition': `attachment; filename=${fileId}.xlsx`,
            'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Transfer-Encoding': 'chunked',
          });
        })
        .then(() => {
          response.end();
        })
        .catch((err) => {
          response.status(500);
          console.warn(err);
        });
    });
  }
}

export default XLSToJson;
