'use strict';

import { DateRange, SeriesEvent } from '../../../types';
import { Express, Request, Response } from 'express';
import { SeriesDetail, SeriesEventFile, SeriesResult } from '../../../src/model';

import { Series } from '../../../src/utils/series';
import { SeriesDataController } from '../../../controller/SeriesDataController';
import dumpError from '../../../src/dumpError';
import { parseDateRangeParams } from '../../../src/parseDateRangeParams';

class SeriesDataRest {
  _seriesController!: SeriesDataController;
  set SeriesDataController(sdc: SeriesDataController) {
    this._seriesController = sdc;
  }

  init(rest: Express) {
    const sdc: SeriesDataController = this._seriesController;
    rest.get('/series/list', function (request: Request, response: Response) {
      let seriesList: SeriesDetail[] = [];
      seriesList = sdc.seriesList.filter((series) => {
        return series.enabled;
      });
      response.send(seriesList);
    });

    rest.get('/series/:id/info', function (request: Request, response: Response) {
      const seriesId = request.params.id;
      try {
        const info: SeriesDetail = sdc.getSeriesDetails(seriesId);
        response.send(info);
      } catch (error) {
        dumpError(error);
        response.status(404);
        response.send(error);
      }
    });

    rest.get('/series/:id', async function (request: Request, response: Response) {
      const seriesId: string = request.params.id;
      const dateRange: DateRange = parseDateRangeParams(request.params, request.query);
      try {
        sdc
          .getSeriesResultsById(seriesId, dateRange)
          .then((jsonBody: SeriesResult) => {
            response.send(jsonBody);
          })
          .catch((error) => {
            dumpError(error);
            response.status(404);
            response.send(error);
          });
      } catch (err) {
        response.status(500);
        console.error(err);
      }
    });

    rest.get('/series/:id/config', function(request: Request, response: Response) {
      const seriesId = request.params.id;

      let seriesList:SeriesDetail[] = [];
      seriesList = sdc.seriesList.filter((series) => {
        return !!series.enabled && series.id.toString() == seriesId || series.alias == seriesId;
      });

      response.send(seriesList[0]);
    });

    rest.get('/series/:id/events', function(request: Request, response: Response) {
      const seriesId: string = request.params.id;

      sdc.getSeriesEventsById(seriesId).then((events: SeriesEventFile[]) => {
        const elidedData: SeriesEvent[] = events.map((sef: SeriesEventFile) => {
          return {
            championshipStatus: sef.championshipStatus,
            date: sef.date,
            format: sef.format,
            id: sef.id,
            location: sef.location,
            name: sef.name,
            organiser: sef.organiser,
            pointsFactor: sef.pointsFactor,
            pointsSystem: sef.pointsSystem,
            state: sef.state,
            subtitle: sef.subtitle,
          };
        });
        response.send(elidedData);
      }).catch((err) => {
        response.status(500);
        console.error(err);
      });
    });
  }
}

export { SeriesDataRest, Series as SeriesData, SeriesDetail };

export default SeriesDataRest;
