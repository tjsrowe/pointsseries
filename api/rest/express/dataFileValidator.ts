'use strict';

import { Express, Request, Response } from 'express';

import { DataFileValidationController } from '../../../controller/DataFileValidationController';
import { ResultFileVerificationResult } from '../../../controller/EventDataFileController';
import { UploadedFile } from 'express-fileupload';

class DataFileValidatorRest {
  _dataFileValidationController!: DataFileValidationController;

  set DataFileValidationController(dfc: DataFileValidationController) {
    this._dataFileValidationController = dfc;
  }

  init(rest: Express) {
    const dfv: DataFileValidationController = this._dataFileValidationController;
    rest.post('/verifyDataFile', function (request: Request, response: Response) {
      let seriesId: number | undefined;
      try {
        seriesId = request.params.id ? parseInt(request.params.id) : undefined;
      } catch (error) {
        response.status(500);
        response.send(error);
      }

      if (!request.files) {
        response.send({
          message: 'No file uploaded',
          status: 400,
        });
      } else {
        const filesArray: UploadedFile | UploadedFile[] = request.files.resultFile;
        let fileToProcess: UploadedFile | undefined;
        if (Array.isArray(filesArray)) {
          if (filesArray.length > 1) {
            response.status(400);
            response.send({ message: 'Can only process one file per call.' });
          } else {
            fileToProcess = filesArray[0];
          }
        } else {
          fileToProcess = filesArray;
        }

        let errorMessage: string | undefined;
        if (fileToProcess) {
          try {
            dfv
              .verifyResultFile(fileToProcess, seriesId)
              .then((result: ResultFileVerificationResult) => {
                response.send(result);
              })
              .catch((error: Error) => {
                response.status(500);
                response.send({ message: error.message });
                response.end();
              });
          } catch (error: any) {
            errorMessage = error.message;
          }
        } else {
          errorMessage = 'No files uploaded.';
        }

        if (errorMessage) {
          response.status(500);
          response.send({ message: errorMessage });
          response.end();
        }
      }
    });
  }
}

export default DataFileValidatorRest;
