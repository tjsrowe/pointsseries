'use strict';

import { Config } from 'config/Config';
import { ControllerManager } from '../controller/ControllerManager';
import DataFileValidatorRest from './rest/express/dataFileValidator';
import EventDataRest from './rest/express/eventRest';
import { HTTP_SERVER_PORT } from '../env';
import SeriesDataRest from './rest/express/seriesData';
import XLSToJson from './rest/express/xlsToJson';
import bodyParser from 'body-parser';
import cors from 'cors';
import dumpError from '../src/dumpError';
import express from 'express';
import fileUpload from 'express-fileupload';
import fs from 'fs';
import https from 'https';
import morgan from 'morgan';

const rest = express();
rest.use(morgan('combined'));
rest.use(fileUpload({ createParentPath: true }));
rest.use(bodyParser.json());
rest.use(bodyParser.urlencoded({ extended: true }));
let xlsToJsonRest: XLSToJson;
let seriesRest: SeriesDataRest;
let eventRest: EventDataRest;
let dataValidatorRest: DataFileValidatorRest;
const controllerManager: ControllerManager = new ControllerManager();

function configureCors(config: Config): cors.CorsOptions {
  if (!config.apiServer) {
    throw new Error('Can\'t configure CORS alloable hosts without config.apiServer node.');
  }
  const allowedOrigins = config.apiServer.allowedOrigins;
  const options: cors.CorsOptions = {
    origin: allowedOrigins,
  };

  return options;
}

class ApiServer {
  init(lConfig: Config): Promise<void> {
    const pr: Promise<void> = new Promise((resolve, reject) => {
      // config = lConfig;
      let httpPort = HTTP_SERVER_PORT;
      if (lConfig.apiServer?.httpPort) {
        console.log(`HTTP port in config file takes precedence, listening on port ${lConfig.apiServer.httpPort}`);
        httpPort = lConfig.apiServer?.httpPort;
      }

      try {
        const rest = createRestServer(lConfig);
        rest.listen(httpPort, () =>
          console.log(`Listening for data model requests on HTTP port ${httpPort} - http://localhost:${httpPort}/`)
        );
        if (lConfig.apiServer?.https) {
          const sslKey = fs.readFileSync(lConfig.apiServer?.https?.keyFile);
          const sslCert = fs.readFileSync(lConfig.apiServer?.https?.certFile);
          const options = {
            cert: sslCert,
            key: sslKey,
          };
          const httpsServer = https.createServer(options, rest);
          const sslPort = lConfig.apiServer?.https.port;
          httpsServer.listen(sslPort, () =>
            console.log(`Listening for data model requests on HTTPS port ${sslPort} - https://localhost:${sslPort}/`)
          );
        }
        resolve();
      } catch (err: any) {
        reject(err);
        console.log(err.message);
      }
    });
    return pr;
  }
}

const apiServer: ApiServer = new ApiServer();
export default apiServer;

// exports.init = function(lConfig:Config, onComplete:Function) {
//   config = lConfig;
//   if (config.apiServer?.port) {
//     listenPort = config.apiServer?.port;
//   }

//   try {
//     const rest = createRestServer(config);
//     rest.listen(listenPort, () =>
//       console.log(`Listening for data model requests on port ${listenPort} - http://localhost:${listenPort}/`));
//     if (onComplete) {
//       onComplete();
//     }
//   } catch (err:any) {
//     console.log(err.message);
//   }
// };

// const restCallAdder = function(path:string, processor:any, method:string|undefined = 'GET') {
//   const corsOptions = configureCors(config);
//   //console.log("Test xls to json on http://localhost:" + listenPort + path);
//   if (method == 'POST') {
//     // cors()
//     rest.post(path, cors(corsOptions), processor);
//   } else {
//     rest.get(path, cors(corsOptions), processor);
//   }
// }

function createRestServer(config: Config) {
  if (rest.get('env') === 'production') {
    rest.set('trust proxy', 1); // trust first proxy
    // TODO sess.cookie.secure = true // serve secure cookies
  }

  rest.use(cors(configureCors(config)));
  // const corsOptions = configureCors(config);

  // rest.options('*', cors(corsOptions));
  // const sess:session.SessionOptions = {
  //   secret: 'cookie development server',
  //   cookie: {},
  //   genid: function() {
  //     return uuidv4() // use UUIDs for session IDs
  //   }
  // };

  // const s:session.Session = session.Session(sess);
  // rest.use();

  controllerManager.createControllers();
  controllerManager.setConfig(config);

  try {
    xlsToJsonRest = new XLSToJson();
    xlsToJsonRest.ExcelSeriesController = controllerManager.ExcelSeriesController;
    xlsToJsonRest.init(rest);

    seriesRest = new SeriesDataRest();
    seriesRest.SeriesDataController = controllerManager.SeriesDataController;
    seriesRest.init(rest);

    dataValidatorRest = new DataFileValidatorRest();
    dataValidatorRest.DataFileValidationController = controllerManager.DataFileValidationController;
    dataValidatorRest.init(rest);

    eventRest = new EventDataRest();
    eventRest.EventDataController = controllerManager.EventDataController;
    eventRest.init(rest);
  } catch (err) {
    dumpError(err);
  }

  return rest;
}

// const sess = {
//   secret: 'cookie development server',
//   cookie: {},
//   genid: function() {
//     return uuidv4() // use UUIDs for session IDs
//   }
// }

// let config:Config = {};
