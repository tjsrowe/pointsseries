'use strict';

import * as dotenv from 'dotenv';

import { ConfigManager } from './controller/ConfigManager';
import apiServer from './api/server';

dotenv.config();


// function loadSeriesConfigFromFile(seriesConfig: ConfigSeries) {
//   const configFile:string|undefined = seriesConfig.configFile;
//   if (seriesConfig.configFile == null) {
//     const errMsg = `Series config node ${JSON.stringify(seriesConfig)} did not specify a config file.`;
//     console.warn(errMsg);
//     throw Error(errMsg);
//   }
//   const configFileLastModified:Date = getFileModifiedDate(configFile!);
//   const id:number|undefined = seriesConfig.id;
//   const rawData = fs.readFileSync(configFile!, 'utf8'); //, function (err, data) {
//   const fixedData = replaceEnvValues(rawData);
//   let configNode:Config;
//   try {
//     configNode = JSON.parse(fixedData);
//   } catch (err) {
//     console.error('Error parsing startlist config at ' + configFile);
//     throw err;
//   }
//   // Retain only 'id' and configFile
//   for (const scmember in seriesConfig) delete seriesConfig[scmember];
//   seriesConfig.id = id;
//   seriesConfig.configFile = configFile;
//   seriesConfig.configFileLastModifiedDate = configFileLastModified;
// //  seriesConfig.get = function () {
// //   return getUpdatedConfig(seriesConfig);
// //  };
// //  for (const cnmember in configNode) seriesConfig[cnmember] = configNode[cnmember];
// }

// const getFileModifiedDate = (file: string):Date => {
//   const stats = fs.statSync(file);
//   const mtime:Date = stats.mtime;
//   return mtime;
// }

// function isConfigFileModified(seriesConfig: ConfigSeries):boolean {
//   if (!seriesConfig.configFile) {
//     return false;
//   }
//   const stats = fs.statSync(seriesConfig.configFile);
//   const mtime:Date = stats.mtime;
//   if (seriesConfig.configFileLastModifiedDate && mtime > seriesConfig.configFileLastModifiedDate) {
//     return true;
//   }
//   return false;
// }

// function getUpdatedConfig(seriesConfig: any) {
//   if (isConfigFileModified(seriesConfig)) {
//     console.log('Config file ' + seriesConfig.configFile + ' has been
//          modified since last use - re-loading from disk.');
//     loadSeriesConfigFromFile(seriesConfig);
//   }
//   return seriesConfig;
// }

// function loadSeriesConfigs(config: Config) {
//   const lists:ConfigSeries[] = config.series;
//   if (!lists) {
//     console.warn('No series are defined in config file.');
//   }
//   for (let i = 0; i < lists.length; i++) {
//     const series = lists[i];
//     if (series.configFile) {
//       loadSeriesConfigFromFile(series);
//     }
//   }
// }

let cfgMgr: ConfigManager;

ConfigManager.loadDefaultConfig()
  .then((mgr: ConfigManager) => {
    cfgMgr = mgr;
    apiServer.init(cfgMgr.Config);
  })
  .catch((err) => {
    console.log('Exception while reading config');
    console.log(err);
  });

// export type { SeriesDetail, ISO8601Date, Filter };
export * from './types';
