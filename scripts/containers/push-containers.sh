#!/bin/sh

. `dirname "$0"`/../ci/env.sh

if [ -z "$REGISTRY_HOST" ];
then
  echo 'REGISTRY_HOST must specify the location of a docker registry to store temp images.'
  exit 1
fi

BASEIMAGE_NAME=pointsseries-base:$BITBUCKET_COMMIT
IMAGE_NAME=pointsseries:$BITBUCKET_COMMIT
REGISTRY_BASE_COMMIT=$REGISTRY_HOST/pointsseries/$BASEIMAGE_NAME
REGISTRY_COMMIT=$REGISTRY_HOST/pointsseries/$IMAGE_NAME

docker tag $BASEIMAGE_NAME $REGISTRY_BASE_COMMIT
docker push $REGISTRY_BASE_COMMIT

docker tag $IMAGE_NAME $REGISTRY_COMMIT
docker push $REGISTRY_COMMIT
