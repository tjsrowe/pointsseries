#!/bin/sh

. `dirname "$0"`/../ci/env.sh

#require_env 'REGISTRY_HOST', 'REGISTRY_HOST must specify the location of a docker registry to store temp images.', 1
if [ -z "$REGISTRY_HOST" ];
then
 echo 'REGISTRY_HOST must specify the location of a docker registry to store temp images.'
 exit 1
fi

if [ -z "$BITBUCKET_COMMIT" ];
then
  echo 'BITBUCKT_COMMIT must be set - usually comes from CI process'
  exit 2
fi

BASEIMAGE_NAME=pointsseries-base:$BITBUCKET_COMMIT
IMAGE_NAME=pointsseries:$BITBUCKET_COMMIT
REGISTRY_BASE_COMMIT=$REGISTRY_HOST/pointsseries/$BASEIMAGE_NAME
REGISTRY_COMMIT=$REGISTRY_HOST/pointsseries/$IMAGE_NAME

DOCKER_BUILDKIT=1 docker build -f containers/Dockerfile-base -t $BASEIMAGE_NAME .
DOCKER_BUILDKIT=1 docker build --build-arg BASE_VERSION=$BITBUCKET_COMMIT -f containers/Dockerfile -t $IMAGE_NAME .

