'use strict';

import { DynamoDBClientConfig } from '../controller/dynamo/DDB';
import { EventDDB } from '../controller/dynamo/EventDDB';
import { EventDataDetail } from '../src/model/event';
import commandLineArgs from 'command-line-args';
import { dynamoConfigFromEnv } from '../src/dynamoUtils';
import { exit } from 'process';
import fs from 'fs';
import path from 'path';

const eddb: EventDDB = new EventDDB();
const dynamoConfig: DynamoDBClientConfig = dynamoConfigFromEnv();

const optionDefinitions: commandLineArgs.OptionDefinition[] = [
  { alias: 'r', name: 'aws-region', type: String },
  { alias: 'a', name: 'aws-key', type: String },
  { alias: 's', name: 'aws-secret', type: String },
  { alias: 'f', name: 'file', type: String },
];

const requirePropertyOrEnvironment = (
  options: commandLineArgs.CommandLineOptions,
  propertyName: string,
  environment?: string
) => {
  if (environment) {
    if (!options[propertyName]) {
      options[propertyName] = process.env[environment];
    }
    if (!options[propertyName]) {
      console.log(`${environment} or --${propertyName} must be specified`);
      exit(1);
    }
  } else {
    if (!options[propertyName]) {
      console.log(`--${propertyName} must be specified`);
      exit(1);
    }
  }
};

const options: commandLineArgs.CommandLineOptions = commandLineArgs(optionDefinitions);

requirePropertyOrEnvironment(options, 'aws-region', 'AWS_REGION');
requirePropertyOrEnvironment(options, 'file');

const importFile = options['file'];
const filePath = path.join('export/events', importFile);
const fileData = fs.readFileSync(filePath, 'utf-8');
const obj: EventDataDetail = JSON.parse(fileData);

eddb.connect(dynamoConfig);

if (obj.cacheFileName) {
  // generateEventId(54321, obj);
}

obj.id = obj.cacheFileName;
eddb.save(obj).then(() => {
  console.log(`Saved ${obj.cacheFileName}`);
});
