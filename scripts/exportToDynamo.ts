'use strict';

import { ConfigManager } from '../controller/ConfigManager';
import { ControllerManager } from '../controller/ControllerManager';
import { DynamoDBClientConfig } from '../controller/dynamo/DDB';
import { DynamoExporter } from '../src/exporters/DynamoExporter';
import { dynamoConfigFromEnv } from '../src/dynamoUtils';

const controllerManager: ControllerManager = new ControllerManager();

const clientParams: DynamoDBClientConfig = dynamoConfigFromEnv();

ConfigManager.loadDefaultConfig()
  .then((manager: ConfigManager) => {
    const dynamoExporter: DynamoExporter = new DynamoExporter(clientParams);

    manager.validateConfig();
    controllerManager.createControllers();
    controllerManager.setConfig(manager.Config);

    controllerManager.DataExportController.exportAllSeries(dynamoExporter);
    controllerManager.DataExportController.exportSeriesList(dynamoExporter);
    controllerManager.DataExportController.exportAllEvents(dynamoExporter); // 'export/events'
  })
  .catch((err) => {
    console.log(`Exception while reading config: ${err.message}`);
    console.log(err);
  });
