'use strict';

import * as dotenv from 'dotenv';

import { ConfigManager } from '../controller/ConfigManager';
import { ControllerManager } from '../controller/ControllerManager';
import { DynamoDBClientConfig } from '../controller/dynamo/DDB';
import { DynamoExporter } from '../src/exporters/DynamoExporter';
import { FileExporter } from '../src/exporters/FileExporter';
import { S3Exporter } from '../src/exporters/S3Exporter';
import { SeriesExporter } from '../controller/DataExportController';
import { ZipExporter } from '../src/exporters/ZipExporter';
import { currentTimeMs } from '../src/dateUtils';
import { dynamoConfigFromEnv } from '../src/dynamoUtils';
import { parse } from 'ts-command-line-args';
import path from 'path';

dotenv.config();

interface IExportArguments {
  awsProfile?: string,
  file?: string;
  output?: string;
  outputPath?: string;
  help?: boolean;
}

const args = parse<IExportArguments>(
  {
    awsProfile: { alias: 'a', multiple: false, optional: true, type: String },
    file: { alias: 'f', description: 'Output zip file or s3 bucket filename', optional: true, type: String },
    help: { alias: 'h', description: 'Prints this usage guide', optional: true, type: Boolean },
    output: { multiple: false, optional: true, type: String },
    outputPath: { alias: 'p', description: 'Output path or S3 bucket name',
      multiple: false, optional: true, type: String },
  },
  {
    helpArg: 'help',
  }
);

const outputUsage = () => {
  console.log('outputPath must be provided if output is not \'dynamo\'');
};

if (!args.output) {
  outputUsage();
}


// eslint-disable-next-line new-cap
// const creds = AWS.SharedIniFileCredentials({ profile: args.awsProfile });

const controllerManager: ControllerManager = new ControllerManager();

ConfigManager.loadDefaultConfig()
  .then((manager: ConfigManager) => {
    const exportDir = 'export/' + currentTimeMs().toString();
    let seriesExporter:SeriesExporter;

    switch (args.output) {
    case 'dynamo':
      const clientParams: DynamoDBClientConfig = dynamoConfigFromEnv();
      const dynamoExporter = new DynamoExporter(clientParams);
      seriesExporter = dynamoExporter;
      break;
    case 's3':
      // const s3TempPath:string = path.resolve(exportDir);
      const s3bucket = args.outputPath ? args.outputPath : 'pointsseries-dev-data';
      const s3key = args.file ? args.file : 'pointsseries-data.zip';
      console.log(`Exporting to S3 bucket ${s3bucket}:${s3key}`);
      const s3Exporter:S3Exporter = new S3Exporter(s3bucket, s3key);
      seriesExporter = s3Exporter;
      break;
    case 'zip':
      const zipTempFile:string = args.file ? args.file : 'export/' + currentTimeMs().toString() + '.zip';
      console.log(`Exporting to zip at ${zipTempFile}`);
      const zipExporter:ZipExporter = new ZipExporter(zipTempFile);
      seriesExporter = zipExporter;
      break;
    default:
      const fullPath:string = path.resolve(exportDir);
      console.log(`Exporting to disk at ${fullPath}`);
      const fileExporter:FileExporter = new FileExporter(exportDir);
      seriesExporter = fileExporter;
      break;
    }
    // const fileExport: FileExporter = new FileExporter(exportDir);
    manager.validateConfig();
    controllerManager.createControllers();
    controllerManager.setConfig(manager.Config);

    controllerManager.DataExportController.exportAllSeries(seriesExporter).then(() => {
      seriesExporter.commit();
    });
    // controllerManager.DataExportController.exportSeriesList(fileExport);
    // controllerManager.DataExportController.exportAllEvents(fileExport); // 'export/events'
  })
  .catch((err) => {
    console.log(`Exception while reading config: ${err.message}`);
    console.log(err);
  });
