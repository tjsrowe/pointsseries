'use strict';

import { DynamoDBClientConfig } from '../controller/dynamo/DDB';
import { EventDDB } from '../controller/dynamo/EventDDB';
import { dynamoConfigFromEnv } from '../src/dynamoUtils';

const eddb: EventDDB = new EventDDB();
const dynamoConfig: DynamoDBClientConfig = dynamoConfigFromEnv({});
eddb.connect(dynamoConfig);

eddb.getAll().then((items) => {
  console.log(`Got ${items.length} items from ${eddb.TableName}`);
  items.forEach((series) => {
    console.log(`Event: ${JSON.stringify(series)}`);
  });
});
