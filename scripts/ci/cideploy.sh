#!/bin/sh

docker build -t tmp . -f-<<EOF
FROM tjsrowe/bb-dind-aws:latest
COPY . build
WORKDIR build
EOF

docker run --privileged --rm --name ci --env-file=.env -it tmp &
echo Pausing 10 seconds to allow container to start
sleep 10

docker exec -it ci /bin/sh scripts/deployment/ecr-tag-commit.sh
docker exec -it ci /bin/sh scripts/deployment/registry-push-tag.sh
docker exec -it ci /bin/sh scripts/deployment/dev-deploy.sh
docker stop ci
