#!/bin/sh

. `dirname "$0"`/../ci/env.sh

if [ -z "$REGISTRY_HOST" ];
then
  echo 'REGISTRY_HOST must specify the location of a docker registry to store temp images.'
  exit 1
fi

if [ -z "$ECR_PUBLIC_URL" ];
then
  echo 'ECR_PUBLIC_URL must specify as public AWS ECR location to push images.'
  exit 2
fi

if [ -z "$CONTAINER_SUFFIX" ];
then
  echo 'CONTAINER_SUFFIX must specify a name to label containers and images with'
  exit 3
fi

COMMIT_IMAGE=pointsseries:$BITBUCKET_COMMIT
REGISTRY_COMMIT=$REGISTRY_HOST/pointsseries/$COMMIT_IMAGE
ECR_COMMIT=$ECR_PUBLIC_URL/$COMMIT_IMAGE
ECR_TAG=$ECR_PUBLIC_URL/pointsseries:$CONTAINER_SUFFIX
/usr/local/bin/aws ecr-public get-login-password | docker login --username AWS --password-stdin $ECR_PUBLIC_URL
docker pull $REGISTRY_COMMIT
docker tag $REGISTRY_COMMIT $ECR_COMMIT
docker push $ECR_COMMIT
docker tag $REGISTRY_COMMIT $ECR_TAG
docker push $ECR_TAG
