#!/bin/sh

set -e

. `dirname "$0"`/../ci/env.sh

if [ -z "$REGISTRY_HOST" ];
then
  echo 'REGISTRY_HOST must specify the location of a docker registry to store temp images.'
  exit 1
fi

if [ -z "$PORT_OFFSET" ];
then
  echo 'PORT_OFFSET must specify the base port to add to or we can''t bind to make the container accessible.'
  exit 2
fi

if [ -z "$CONTAINER_SUFFIX" ];
then
  echo 'CONTAINER_SUFFIX must specify a name to label containers and images with'
  exit 3
fi

BE_PORT=`expr $PORT_OFFSET + 4160`
BE_SSL_PORT=`expr $PORT_OFFSET + 4060`
COMMIT_IMAGE=pointsseries:$BITBUCKET_COMMIT
REGISTRY_COMMIT=$REGISTRY_HOST/pointsseries/$COMMIT_IMAGE
BE_CONTAINER=pointsseries-$CONTAINER_SUFFIX
LOCAL_IMAGE=pointsseries:$CONTAINER_SUFFIX
docker -H $DOCKER_HOST pull $REGISTRY_COMMIT
docker -H $DOCKER_HOST tag $REGISTRY_COMMIT $LOCAL_IMAGE
docker -H $DOCKER_HOST stop $BE_CONTAINER || true
docker -H $DOCKER_HOST rm $BE_CONTAINER || true
docker -H $DOCKER_HOST run --name $BE_CONTAINER -d -e POINTSSERIES_DATA_DIR=/var/pointssries/data -e config=/var/pointsseries/config/config.json -v $SSL_CERTIFICATE_DIR:/mnt/ssl -v /mnt/pointsseries-$CONTAINER_SUFFIX/data:/var/pointsseries/data -v /mnt/pointsseries-$CONTAINER_SUFFIX/config:/var/pointsseries/config -p $BE_PORT:80 -p $BE_SSL_PORT:443 -t $LOCAL_IMAGE

echo Finished with docker start of container $BE_CONTAINER on dev host.