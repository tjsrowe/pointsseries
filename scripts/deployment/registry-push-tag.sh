#!/bin/sh

. `dirname "$0"`/../ci/env.sh

if [ -z "$REGISTRY_HOST" ];
then
  echo 'REGISTRY_HOST must specify the location of a docker registry to store temp images.'
  exit 1
fi

if [ -z "$CONTAINER_SUFFIX" ];
then
  echo 'CONTAINER_SUFFIX must specify a name to label containers and images with'
  exit 3
fi

COMMIT_IMAGE=pointsseries:$BITBUCKET_COMMIT
REGISTRY_COMMIT=$REGISTRY_HOST/pointsseries/$COMMIT_IMAGE
REGISTRY_TAG=$REGISTRY_HOST/pointsseries/pointsseries:$CONTAINER_SUFFIX
docker pull $REGISTRY_COMMIT
docker tag $REGISTRY_COMMIT $REGISTRY_TAG
docker push $REGISTRY_TAG
