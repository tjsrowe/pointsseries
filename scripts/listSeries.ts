'use strict';

import { DynamoDBClientConfig } from '../controller/dynamo/DDB';
import { SeriesDDB } from '../controller/dynamo/SeriesDDB';
import { dynamoConfigFromEnv } from '../src/dynamoUtils';

const sddb: SeriesDDB = new SeriesDDB();
const dynamoConfig: DynamoDBClientConfig = dynamoConfigFromEnv({});
sddb.connect(dynamoConfig);

sddb.getAll().then((items) => {
  console.log(`Got ${items.length} items.`);
  items.forEach((series) => {
    console.log(`Series: ${JSON.stringify(series)}`);
  });
});
