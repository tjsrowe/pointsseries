[![npm version](https://badge.fury.io/js/@tjsrowe%2Fpointsseries.svg)](https://badge.fury.io/js/@tjsrowe%2Fpointsseries)

# pointsseries

This project is the points calculation library primarily used for the AusCycling National Cup, however may be useful to other sporting organisations for generating both rolling and fixed-period rankings.

The most important requirement of this system was to be able to process directly from Excel Spreadsheet data, so that quick, rapid corrections to results and licenses can be reflected without the database being the master.

## Publication

This package is available on npm at https://www.npmjs.com/package/@tjsrowe/pointsseries

It is mainly utilised by the pointsseries React GUI, a repy you can find at https://bitbucket.org/tjsrowe/pointsseries-ui

## Usage

`npm install --save @tjsrowe/pointsseries`

You can run this as an express server with REST api calls to test the front-end by using

`npm start`

### Dependencies

You need to install ts-node globally to compile the typescript in the project.

`npm install -g ts-node prettier`

## Building

Building is done within the Bitbucket CI pipeline. The containers built from the scripts/ directory build.

You can check that everything passes linting/testing using

`npm build`

# CI Configuration

## To get a token for the build to push container images to the public registry

`aws --profile <ci-user> ecr get-login-password --region ap-southeast-2`

You can chain this using:

`aws --profile <ci-user> ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin <endpoint>`

More on this coming later.