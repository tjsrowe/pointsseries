export const LOG_OUTPUT_NEW_LICENSE = process.env.LOG_OUTPUT_NEW_LICENSE || false;
export const LOG_OUTPUT_LICENSE_ERROR = process.env.LOG_OUTPUT_LICENSE_ERROR || false;

export const LICENSE_HASH_LENGTH = process.env.LICENSE_HASH_LENGTH ? parseInt(process.env.LICENSE_HASH_LENGTH) : 8;
export const DOB_HASH_LENGTH = process.env.DOB_HASH_LENGTH ? parseInt(process.env.DOB_HASH_LENGTH) : 4;

export const HTTP_SERVER_PORT = process.env.HTTP_SERVER_PORT ? parseInt(process.env.HTTP_SERVER_PORT) : 80;
export const HTTPS_SERVER_PORT = process.env.HTTPS_SERVER_PORT ? parseInt(process.env.HTTPS_SERVER_PORT) : 443;
