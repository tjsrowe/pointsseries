export const isRootPath = (filename: string): boolean => {
  return filename.startsWith('/') || filename.charAt(1) == ':' || filename.startsWith('\\');
};
