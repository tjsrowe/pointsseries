'use strict';

import * as UUID from 'uuid-1345';

export const generateUuidFromString = (str:string):Promise<string> => {
  const pr: Promise<string> = new Promise((resolve, reject) => {
    UUID.v5(
      {
        name: str,
        namespace: UUID.namespace.url,
      },
      function (err, result) {
        if (result) {
          resolve(result);
        } else {
          reject(err);
        }
      }
    );
  });
  return pr;
};
