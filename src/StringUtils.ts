'use strict';

export function removeSpaces(str: string): string {
  return str.replace(' ', '');
}

export default class StringUtils {
  static removeSpaces(str: string): string {
    return this.removeSpaces(str);
  }
}
