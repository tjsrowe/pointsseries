'use strict';

import { dateToIsoString } from './dateUtils';

const excelDateToISODate = (serial: number) => {
  const utc_days: number = Math.floor(serial - 25569);
  const utc_value: number = utc_days * 86400;
  const date_info: Date = new Date(utc_value * 1000);
  return dateToIsoString(date_info);
};

export default excelDateToISODate;
