'use strict';

import { Category, CategoryKey } from '../../src/model/category';
import { NamedPointsMap, PointsMap } from '../../controller/SeriesDataController';
import { SeriesDataDetail, SeriesDetail } from '../../src/model/series';
import { createLookupKeyForString, createNameWithGender } from './category';

import { CategoryController } from '../../controller/CategoryController';
import { CategorySet } from './categorySet';
import { Entrant } from '../../src/model/entrant';
import { SeriesEntrant } from '../../src/model/seriesentrant';
import { SeriesEventFile } from '../../src/model/event';
import assert from 'assert';
import { convertPointsToMap } from '../../src/pointsConversion';

export class Series {
  private events: SeriesEventFile[] = [];
  private categorySet: CategorySet;
  // TODO: Reference by string as key, no need to lookup index.
  private categoryMap: Map<number, SeriesEntrant[]> = new Map<number, SeriesEntrant[]>();
  excludedRiders?: any;
  private scoringSystems?: NamedPointsMap;
  private Columns?: string[];
  private seriesInfo?: SeriesDetail;

  private correctedCategories: Map<string, string> = new Map<string, string>();
  private categoryNameMap: Map<string, SeriesEntrant[]> = new Map<string, SeriesEntrant[]>();

  addEntrant(entrant: SeriesEntrant):void {
    const key:CategoryKey = entrant.Gender ?
      createNameWithGender(entrant.Category, entrant.Gender) :
      createLookupKeyForString(entrant.Category);
    const index:number|undefined = this.categorySet.getCategoryIndexByName(key);
    if (index == undefined) {
      throw new Error(`Category for entrant in ${entrant.Category} doesn't exist.`);
    }
    let entrantsInCategory: SeriesEntrant[] | undefined = this.categoryMap.get(index);
    // return this.categoryMap.get(index);
    // let entrantsInCategory: SeriesEntrant[] | undefined = this.categoryNameMap.get(entrant.Category);
    if (!entrantsInCategory) {
      entrantsInCategory = [];
      this.categoryNameMap.set(entrant.Category, entrantsInCategory);
    }

    entrantsInCategory.push(entrant);
  }

  addEvent(eventMetadata: SeriesEventFile) {
    this.events.push(eventMetadata);
  }

  get CategoryMap(): Map<string, number> {
    return this.categorySet.Map;
  }

  get EntrantCount(): number {
    let total = 0;
    this.categoryMap.forEach((list:SeriesEntrant[]) => {
      total += list.length;
    });
    return total;
  }

  get CategoryEntrants():SeriesEntrant[][] {
    const categories:SeriesEntrant[][] = [];
    this.categoryMap.forEach((entrants:SeriesEntrant[]) => {
      categories.push(entrants);
    });
    return categories;
  }

  // get Entrants(): SeriesEntrant[] {
  //   const entrants:SeriesEntrant[] = [];
  //   this.categoryMap.forEach((entrants:SeriesEntrant[]) => {
  //     entrants.forEach((e) => entrants.push(e));
  //   });
  //   return entrants;
  // }

  get Events(): SeriesEventFile[] {
    return this.events;
  }

  get ScoringSystems(): NamedPointsMap|undefined {
    return this.scoringSystems;
  }

  getScoringSystem(name?: string): Map<string | number, number> {
    if (name && this.scoringSystems?.has(name)) {
      return this.scoringSystems.get(name)!;
    } else if (this.scoringSystems?.has('default')) {
      return this.scoringSystems.get('default')!;
    }
    return new Map<string | number, number>();
  }

  setScoringSystems(maps: NamedPointsMap):void {
    this.scoringSystems = maps;
  }

  setScoringSystem(map: Object, name: string) {
    const converted: Map<string | number, number> | undefined = convertPointsToMap(map);
    assert(converted!.hasOwnProperty('NaN') == false);
    if (this.scoringSystems == undefined) {
      this.scoringSystems = new Map<string, PointsMap>();
    }
    if (converted !== undefined) {
      this.scoringSystems.set(name, converted);
    } else {
      throw new Error('Converting points map gave undefined table.');
    }
  }

  get HasScoringSystem(): boolean {
    return this.scoringSystems != undefined && this.scoringSystems.size > 0;
  }

  get HasSeriesDetail(): boolean {
    return this.seriesInfo != undefined;
  }

  public hasCategory(categoryName: string): unknown {
    return this.CategoryMap.has(categoryName.toLowerCase().trim());
  }

  public hasEvent(id: number): boolean {
    return this.events[id] != undefined;
  }

  get categoryCount(): number {
    return this.categorySet.categoryCount;
  }

  constructor(categorySet: CategorySet) {
    this.categorySet = categorySet;
  }

  public set SeriesDataDetail(sdd: SeriesDataDetail) {
    assert(sdd.scoringSystem != undefined);
    this.SeriesDetail = sdd;

    const converted: PointsMap | undefined = convertPointsToMap(sdd.scoringSystem);
    // assert converted != undefined;
    assert(converted!.hasOwnProperty('NaN') == false);
    this.setScoringSystem(converted!, 'default');
  }

  public set SeriesDetail(sd: SeriesDetail | SeriesDataDetail) {
    this.seriesInfo = sd;
  }

  public get SeriesDetail(): SeriesDetail {
    assert(this.seriesInfo, 'Can\'t get SeriesDetail when it\'s not set.');
    return this.seriesInfo;
  }

  private getCategoryIndex(category: Category) {
    return CategoryController.getCategoryMapOrder(this.categorySet.Map, category);
  }

  public getEntrantsInEntrantCategory(entry: Entrant): SeriesEntrant[] | undefined {
    const index: number | undefined = this.categorySet.getCategoryIndexByName(entry.Category);
    if (index != undefined) {
      if (!this.categoryMap.has(index)) {
        const seriesArray: SeriesEntrant[] = [];
        this.categoryMap.set(index, seriesArray);
        return seriesArray;
      }
      return this.categoryMap.get(index);
    } else {
      throw Error(`Category ${entry.Category} doesn't exist in series data.`);
    }
  }

  public getCategory(categoryName: string): Category | undefined {
    return this.categorySet.getCategoryByName(categoryName);
  }

  public categoryExists(categoryName: string): boolean {
    assert(categoryName != undefined);
    return this.categorySet.categoryExists(categoryName);
  }

  public createUnrecognisedCategory(categoryName: string): Category {
    assert(categoryName != undefined);
    return this.categorySet.createCategoryByName(categoryName, true);
  }

  setEvent(eventId: number, seriesEvent: SeriesEventFile) {
    this.events[eventId] = seriesEvent;
  }
}
