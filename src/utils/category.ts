'use strict';

import { Category, CategoryKey } from '../../src/model/category';

import { Gender } from '../../src/model/entrant';
import assert from 'assert';

export const removeGenderCategoryName = (categoryName: string): string => {
  const genderList: string[] = ['female', 'women', 'men', 'male', 'boys', 'girls', 'mixed'];
  const trimCategory: string = categoryName.trim();
  const matchCategory: string = trimCategory.toLowerCase();
  const matched: string | undefined = genderList.find((g) => matchCategory.endsWith(g));

  if (matched) {
    let nameExcludingGender:string = trimCategory.substring(0, trimCategory.length - matched.length).trim();
    while (nameExcludingGender.endsWith('_')) {
      nameExcludingGender = nameExcludingGender.substring(0, nameExcludingGender.length-1);
    }
    while (nameExcludingGender.startsWith('_')) {
      nameExcludingGender = nameExcludingGender.substring(1);
    }

    return nameExcludingGender;
  } else {
    return trimCategory;
  }
};

export const createCategory = (category: string | string[]): Category => {
  const firstCategory: string = typeof category === 'string' ? category : category[0];
  const lcFirstCategory: string = firstCategory.toLowerCase();
  let gender: Gender | undefined = undefined;
  if (lcFirstCategory.includes('women') || lcFirstCategory.includes('female') || lcFirstCategory.includes('girls')) {
    gender = Gender.WOMEN;
  } else if (lcFirstCategory.includes('men') || lcFirstCategory.includes('male') || lcFirstCategory.includes('boys')) {
    gender = Gender.MEN;
  } else if (lcFirstCategory.includes('Mixed')) {
    gender = Gender.MIXED;
  }
  const correctedCategoryName: string = removeGenderCategoryName(firstCategory);
  const output: Category = {
    gender: gender,
    name: correctedCategoryName
  };

  if (Array.isArray(category)) {
    for (let i = 1; i < category.length; i++) {
      const altName = removeGenderCategoryName(category[i]);
      if (output.alternativeNames == undefined) {
        output.alternativeNames = [];
      }
      output.alternativeNames.push(altName);
    }
  }

  return output;
};

export const createNameWithGender = (name: string, gender?: Gender): CategoryKey => {
  assert(name);

  let key: string = name.trim().toLocaleLowerCase();
  if (gender != undefined) {
    key = `${key}_${Gender[gender]}`;
  }
  return key;
};

export const createCategoryHashId = (cat: Category): CategoryKey => {
  assert(cat.name);
  return createNameWithGender(cat.name, cat.gender);
};


export const createLookupKeyForString = (categoryName: string): CategoryKey => {
  const cat: Category = createCategory(categoryName);
  const key: CategoryKey = createCategoryHashId(cat);
  return key;
};
