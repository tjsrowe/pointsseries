'use strict';

import { Category, CategoryKey } from '../../src/model/category';
import { createCategory, createCategoryHashId, createLookupKeyForString } from './category';

import { Gender } from '../../src/model/entrant';
import assert from 'assert';
import { createIndexMapFromList } from '../../controller/CategoryController';

export class CategorySet {
  private _categories: Category[];
  private _lookup: Map<string, number>;

  constructor(cats: Category[] = []) {
    this._categories = cats;
    this._lookup = createIndexMapFromList(cats);
  }

  public set Categories(cats: Category[]) {
    this._categories = cats;
    this._lookup = createIndexMapFromList(cats);
  }

  public get categoryCount(): number {
    return this._categories.length;
  }

  public get Map(): Map<string, number> {
    return this._lookup;
  }

  addAlias(names: string | string[], index: number): void {
    if (Array.isArray(names)) {
      names.forEach((catName) => {
        this._lookup.set(catName.toLowerCase().trim(), index);
      });
    } else {
      this._lookup.set(names.toLowerCase().trim(), index);
    }
  }

  addCategoryByName(categoryName: string, gender?: Gender): void {
    const category: Category = this.createCategoryByName(categoryName);
    if (category.gender == undefined) {
      category.gender = gender;
    } else if (gender != undefined && category.gender != gender) {
      throw Error(`Tried to create category but specied gender ${gender} \
did not match gender retrieved from string name ${categoryName}`);
    }
    const trimmedKey: string = categoryName.toLowerCase().trim();
    const newIndex: number = this._categories.push(category);
    this._lookup.set(trimmedKey, newIndex);
    const lookupHash: CategoryKey = createCategoryHashId(category);
    this._lookup.set(lookupHash, newIndex);
  }

  getCategoryIndexByName(categoryName: string): number | undefined {
    const trimmedName: string = categoryName.toLowerCase().trim();
    if (this._lookup.has(trimmedName)) {
      const index: number | undefined = this._lookup.get(trimmedName);
      if (!this._categories[index!] == undefined) {
        throw Error(`Category name map had entry for ${categoryName} but categoryMap had no index ${index}`);
      }
      return index;
    }
    const key: CategoryKey = createLookupKeyForString(trimmedName);
    const index: number | undefined = this._lookup.get(key);
    return index;
  }

  getCategoryByName(categoryName: string): Category | undefined {
    const trimmedName: string = categoryName.toLowerCase().trim();
    if (this._lookup.has(trimmedName)) {
      return this._categories[this._lookup.get(trimmedName)!];
    }

    const index: number | undefined = this.getCategoryIndexByName(categoryName);
    if (index != undefined) {
      this._lookup.set(trimmedName, index);
      return this._categories[index];
    }
    return undefined;
  }

  categoryExists(categoryName: string): boolean {
    if (!categoryName) {
      return false;
    }
    const trimmedName: string = categoryName.toLowerCase().trim();
    if (this._lookup.has(trimmedName)) {
      assert(this._categories[this._lookup.get(trimmedName)!] != undefined);
      return true;
    }
    const key: CategoryKey = createLookupKeyForString(categoryName);
    const foundCategory: number | undefined = this._lookup.get(key);
    if (foundCategory != undefined) {
      assert(this._categories[foundCategory!] != undefined);
      this._lookup.set(trimmedName, foundCategory);
      return true;
    } else {
      return false;
    }
  }

  createCategoryByName(categoryName: string, isUnrecognisedCategory = false): Category {
    assert(categoryName != undefined);
    const trimmedName: string = categoryName.trim();
    const cat: Category = createCategory(trimmedName);
    if (isUnrecognisedCategory) {
      cat.unrecognisedCategory = true;
    }

    const newIndex = this._categories.length;
    this._categories[newIndex] = cat;
    this._lookup.set(trimmedName, newIndex);
    const key: CategoryKey = createLookupKeyForString(trimmedName);
    this._lookup.set(key, newIndex);
    return cat;
  }
}
