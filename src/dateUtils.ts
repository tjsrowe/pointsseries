'use strict';
import { DateRange } from '../types';
import { isoDateStringToDate } from './isoDateStringToDate';

export const dateToIsoString = (inputDate: Date): string => {
  const year: string = inputDate.getFullYear().toString();
  const month: string = (inputDate.getMonth() + 1).toString().padStart(2, '0');
  const day: string = inputDate.getDate().toString().padStart(2, '0');
  const isoDate = `${year}-${month}-${day}`;
  return isoDate;
};

export const isoDateToday = (): string => {
  const now: Date = new Date();
  return dateToIsoString(now);
};

export const daysToMsec = (days: number): number => {
  return days * 86400000;
};

export const subtractDaysFromIsoDate = (startDate: string, days: number): string => {
  const parsedDate: Date | undefined = isoDateStringToDate(startDate);
  if (parsedDate) {
    const mstime: number = parsedDate.getTime();
    const subtractMsec = daysToMsec(days);
    const newMsTime: number = mstime - subtractMsec;

    const calculatedDate: Date = new Date(newMsTime);
    const outputIsoDate = dateToIsoString(calculatedDate);
    return outputIsoDate;
  }
  return startDate;
};

export const getDefaultDateRange = (): DateRange => {
  const to: string = isoDateToday();
  const from = subtractDaysFromIsoDate(to, 365);
  const dateRange: DateRange = {
    fromDate: from,
    toDate: to,
  };
  return dateRange;
};

export const getYearFromDateString = (isoDateString: string): number => {
  const parts: string[] = isoDateString.split('-');
  if (parts.length > 0) {
    return parseInt(parts[0]);
  }
  throw new Error('Provide date is not a valid ISO-8601 date string.');
};

export const currentTimeMs = (): number => {
  return new Date().getTime();
};

export const finishTimer = (startTime: number): number => {
  const finishTime: number = currentTimeMs();
  const time: number = finishTime - startTime;
  return time;
};

export const dateRangeToString = (dateRange: DateRange): string => {
  const from = dateRange.fromDate || '*';
  const to = dateRange.toDate || '*';
  const output = `${from}->${to}`;
  return output;
};
