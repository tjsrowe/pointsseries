'use strict';

import { AbstractEntrant } from './entrant';
import { EventResult } from './event';

export interface SeriesEntrant extends AbstractEntrant {
  results: EventResult[];
  Points?: number;
}
