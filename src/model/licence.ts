'use strict';

export type License = {
  LicenseType?: string;
  LicenseNumber?: string;
};
