'use strict';

import { SeriesEvent, SeriesEventFile } from './event';

import { Category } from './category';
import { Filter } from '../../config/Config';
import { SeriesEntrant } from './seriesentrant';

export interface SeriesDetail {
  id: number;
  name: string;
  alias?: string;
  displayname: string;
  link: string;
  enabled: boolean;
  description?: string;
  allowsDateOverride: boolean;
  filter?: Filter;
  categories: Category[];
}

export interface SeriesDataDetail extends SeriesDetail {
  eventIdList?: string[];
  scoringSystem: Object;
  events: SeriesEventFile[];
}

export interface SeriesResult extends SeriesDetail {
  // entrants: SeriesEntrant[];
  categoryEntrants: SeriesEntrant[][];
  events: SeriesEvent[];
  scoringSystem: Object;
}


