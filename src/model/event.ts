'use strict';

import { EventDataError } from '../../controller/EventDataController';
import { ISO8601Date } from '../../src/isoDateStringToDate';
import { Result } from './entrant';

export type ChampionshipEventDetails = {
  region: string;
  year: number;
  format: string;
};

export interface EventInfo {
  championshipStatus?: ChampionshipEventDetails;
  date?: ISO8601Date;
  format: string;
  id: string;
  isFinal?: boolean;
  location: string;
  name: string;
  organiser?: string;
  state: string;
  subtitle?: string;
}

export interface SeriesEvent extends EventInfo {
  pointsFactor?: number;
  pointsSystem?: string;
}

export interface SeriesEventFile extends SeriesEvent, EventDataFile {
  error?: any;
}

interface EventDataFile extends EventInfo {
  cacheFileName: string;
  dataFileTime?: Date;
  dataFileLoadTime?: Date;
  resultFileName: string;
  pointsFactor?: number;
  pointsSystem?: string;
}

export interface EventDataDetail extends EventDataFile {
  eventId?: number;
  eventRawData?: any;
  errorList?: EventDataError[];
  Columns?: string[];
}

export type EventResult = {
  Event: number;
  Result: number | string | Result;
  IsIneligible?: boolean;
  Points?: number;
};
