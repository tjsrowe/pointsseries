'use strict';

import { Gender } from './entrant';

export interface Category {
  unrecognisedCategory?: boolean;
  name: string;
  gender?: Gender;
  minimumAge?: number;
  maximumAge?: number;
  alternativeNames?: string[];
}

export type CategoryKey = string;
