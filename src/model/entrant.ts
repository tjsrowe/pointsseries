'use strict';

import { License } from './licence';

export type Result = number | string;
export type Entrant = AbstractEntrant;

export enum Sex {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  MIXED = 'MIXED'
}

export enum Gender {
  MEN = 'MEN',
  WOMEN = 'WOMEN',
  MIXED = 'MIXED'
}

export interface AbstractEntrant {
  FullName?: string;
  Surname?: string;
  Firstname?: string;
  Category: string;
  DOB_Hash?: string;
  DOB?: string;
  LicenseHash?: License;
  Gender?: Gender;
}

export interface EventEntrant extends AbstractEntrant {
  Result: Result;
  IsIneligible?: boolean;
}
