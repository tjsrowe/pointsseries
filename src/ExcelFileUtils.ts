'use strict';
import * as XLSX from 'xlsx';
import hasFiletypeInFile from './hasFiletypeInFile';

export class ExcelFileUtils {
  static isXlsFile(filePath: string): boolean {
    if (hasFiletypeInFile(filePath, 'xlsx') || hasFiletypeInFile(filePath, 'xls')) {
      return true;
    }
    return false;
  }

  static excelFilenameHasSheet(filePath: string): boolean {
    const sheetPosition: number = filePath.indexOf('!');
    if (sheetPosition >= 0) {
      return true;
    }
    return false;
  }

  static getExcelSheetFromPath(filePath: string): string | undefined {
    const sheetPosition: number = filePath.indexOf('!');
    if (sheetPosition > 0) {
      const sheetName: string = filePath.substring(sheetPosition + 1);
      return sheetName;
    }
    return undefined;
  }

  static getExcelFilenameForPath(filePath: string): string {
    if (ExcelFileUtils.excelFilenameHasSheet(filePath)) {
      const sheetPosition: number = filePath.indexOf('!');
      const xlsFileName: string = filePath.substring(0, sheetPosition);
      return xlsFileName;
    }
    return filePath;
  }

  static getSheetsFromFile(filePath: string): string[] {
    const workbook: XLSX.WorkBook = XLSX.readFile(filePath);
    return this.getSheetsFromWorkbook(workbook);
  }

  static getSheetsFromWorkbook(workbook: XLSX.WorkBook): string[] {
    const sheets: string[] = Object.keys(workbook.Sheets);
    return sheets;
  }

  static hasSheet(workbook: XLSX.WorkBook, name: string): boolean {
    const sheets: string[] = this.getSheetsFromWorkbook(workbook);
    return sheets.includes(name);
  }

  static createCacheKey(filePath: string, worksheet: string | undefined = undefined) {
    let cacheKey: string | undefined = undefined;

    if (ExcelFileUtils.excelFilenameHasSheet(filePath)) {
      worksheet = ExcelFileUtils.getExcelSheetFromPath(filePath);
    }
    const filename: string = ExcelFileUtils.getExcelFilenameForPath(filePath);
    cacheKey = filename + '!' + worksheet;
    return cacheKey;
  }
}
