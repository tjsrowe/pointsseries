'use strict';

// if (!String.prototype.endsWith) {
//   Object.defineProperty(String.prototype, 'endsWith', {
//     enumerable: false,
//     configurable: false,
//     writable: false,
//     value: function(searchString: string, position: number) {
//       position = position || this.length;
//       position = position - searchString.length;
//       const lastIndex: number = this.lastIndexOf(searchString);
//       return lastIndex !== -1 && lastIndex === position;
//     },
//   });
// }

export function strEndsWith(str: string, searchString: string, position?: number): boolean {
  if (str === undefined) {
    throw Error('null string won\'t end with anything.');
  } else if (typeof str !== 'string') {
    throw Error('Can only determine end of strings, got object: ' + JSON.stringify(str));
  }
  position = position || str.length;
  position = position - searchString.length;
  const lastIndex: number = str.lastIndexOf(searchString);
  return lastIndex !== -1 && lastIndex === position;
}
