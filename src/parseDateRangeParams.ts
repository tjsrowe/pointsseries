'use strict';
import { isoDateToday, subtractDaysFromIsoDate } from './dateUtils';
import { DateRange } from '../types';
import { ParamsDictionary } from 'express-serve-static-core';

export function parseDateRangeParams(params: ParamsDictionary, query: any): DateRange {
  const outputDateRange: DateRange = {};
  if (params.toDate) {
    outputDateRange.toDate = params.toDate;
  } else if (query && query.toDate) {
    outputDateRange.toDate = query.toDate;
  } else {
    outputDateRange.toDate = isoDateToday();
  }

  if (params.fromDate) {
    outputDateRange.fromDate = params.fromDate;
  } else if (query && query.fromDate) {
    outputDateRange.fromDate = query.fromDate;
  } else if (outputDateRange.toDate) {
    outputDateRange.fromDate = subtractDaysFromIsoDate(outputDateRange.toDate, 365);
  } else {
    const today = isoDateToday();
    outputDateRange.fromDate = subtractDaysFromIsoDate(today, 365);
  }
  return outputDateRange;
}
