'use strict';

import { DataValidator } from './dataValidator';

export default function dumpError(err: any) {
  if (typeof err === 'object') {
    try {
      err.forEach((value: any, key: any) => {
        if (DataValidator.isNumber(key)) {
          if (value.errorList) {
            dumpError(value.errorList);
          } else {
            dumpError(value.error);
          }
        }
      });
    } catch (err) {
      // ignore
    }

    if (err.message) {
      console.log('\nMessage: ' + err.message);
    }
    if (err.stack) {
      console.log('\nStacktrace:');
      console.log('====================');
      console.log(err.stack);
    }
  } else if (Array.isArray(err)) {
    err.forEach((element) => {
      console.warn(JSON.stringify(element.error));
    });
  } else {
    console.trace(`dumpError :: argument is not an object: ${err}`);
  }
}
