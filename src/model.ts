'use strict';

import { AbstractEntrant, EventEntrant } from './model/entrant';
import { Category, CategoryKey } from './model/category';
import { EventDataDetail, EventInfo, SeriesEventFile } from './model/event';
import { SeriesDataDetail, SeriesDetail, SeriesResult } from './model/series';

import { License } from './model/licence';
import { SeriesEntrant } from './model/seriesentrant';

export type { AbstractEntrant, Category, CategoryKey, EventDataDetail,
  EventEntrant, EventInfo, License, SeriesEntrant,
  SeriesEventFile, SeriesDataDetail, SeriesDetail, SeriesResult };

