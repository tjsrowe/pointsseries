'use strict';

export class DataValidator {
  public static isInteger = function (str: string) {
    // Derived from
    // https://stackoverflow.com/questions/175739/built-in-way-in-javascript-to-check-if-a-string-is-a-valid-number
    if (typeof str != 'string') return false; // we only process strings!
    if (!isNaN(parseInt(str))) return false; // ...and ensure strings of whitespace fail

    // return !isNaN(str)
    // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
  };

  public static isInt(value: any): boolean {
    return !isNaN(value) && parseInt(value) == value && !isNaN(parseInt(value, 10));
  }

  public static isNumeric(str: string) {
    // From https://stackoverflow.com/questions/175739/built-in-way-in-javascript-to-check-if-a-string-is-a-valid-number
    if (typeof str != 'string') return false; // we only process strings!
    const numValue: number = +str;
    return (
      !isNaN(numValue) && // use type coercion to parse the _entirety_
      // of the string (`parseFloat` alone does not do this)...
      !isNaN(parseFloat(str))
    ); // ...and ensure strings of whitespace fail
  }

  public static isNumber(value: any): boolean {
    return typeof value === 'number' && isFinite(value);
  }
}
