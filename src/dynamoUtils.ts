'use strict';

import * as dotenv from 'dotenv';

import { DynamoDBClientConfig } from '../controller/dynamo/DDB';

dotenv.config();

const AWS_REGION:string|undefined = process.env.AWS_REGION || 'ap-southeast-2';

type DynamoEnvProps = {
  region?: string,
  accessKeyId?: string,
  secretAccessKey?: string
};

export const dynamoConfigFromEnv = ( {
  accessKeyId = process.env.AWS_ACCESS_KEY_ID,
  region = AWS_REGION,
  secretAccessKey = process.env.AWS_SECRET_KEY
} : DynamoEnvProps = { }
): DynamoDBClientConfig => {
  if (!accessKeyId) {
    throw new Error('Required parameter AWS_ACCESS_KEY_ID not configured');
  } else if (!secretAccessKey) {
    throw new Error('Required parameter AWS_SECRET_KEY not configured');
  } else if (!region) {
    throw new Error('Required parameter AWS_REGION not configured');
  }
  const clientParams: DynamoDBClientConfig = {
    credentials: {
      accessKeyId,
      secretAccessKey,
    },
    region,
  };

  return clientParams;
};
