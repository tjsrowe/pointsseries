'use strict';

export const hasFiletypeInFile = (filePath: string, extension: string | undefined): boolean => {
  if (!filePath && !extension) {
    throw Error(`You must specify both a file path and extension to check 
    (got filePath=undefined and ext=undefined)`);
  }
  if (!extension) {
    throw Error(`You must specify both a file path and extension to check (got ${filePath} and ext=undefined)`);
  }
  if (!filePath) {
    throw Error(
      `You must specify both a file path and extension to check (got filePath=undefined and ext=${extension})`
    );
  }

  const extensionPosition: number = filePath.indexOf('.' + extension);
  if (extensionPosition > 0) {
    return true;
  }
  return false;
};

export default hasFiletypeInFile;
