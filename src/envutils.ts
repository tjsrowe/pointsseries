export const replaceEnvValues = (data: any): any => {
  let failed = false;
  let failureValue: string | undefined;
  // Stolen from
  // https://stackoverflow.com/questions/21363912/how-to-resolve-a-path-that-includes-an-environment-variable-in-nodejs
  if (data != undefined) {
    let replaced = data.replace(/%([^%]+)%/g, (_: any, n: string) => {
      if (process.env[n] == undefined) {
        console.error(`Tried to replace ${n} but was not set in environment.`);
        failed = true;
        failureValue = n;
        return '';
      } else {
        return process.env[n]!.replaceAll('\\', '\\\\');
      }
    });
    if (failed) {
      throw new Error(`Got an error while replacing environment variable ${failureValue} from config file.`);
    }
    replaced = replaced.replace(/\$([A-Z_]+[A-Z0-9_]*)/gi, (_: any, a: string) =>
      process.env[a]!.replaceAll('\\', '\\\\')
    );
    return replaced;
  } else {
    return data;
  }
};
