'use strict';

import { strEndsWith } from './endsWith';

export class JsonUtils {
  static removeSpacesFromKeys(jsonObject: any): any {
    if (jsonObject.constructor == objectConstructor) {
      for (const key in jsonObject) {
        const fixedValue = JsonUtils.removeSpacesFromKeys(jsonObject[key]);
        if (key.indexOf(' ') >= 0) {
          const replacedKey = key.replaceAll(' ', '');
          jsonObject[replacedKey] = fixedValue;
          delete jsonObject[key];
        }
      }
    } else if (jsonObject.constructor == arrayConstructor) {
      for (const key2 in jsonObject) {
        jsonObject[key2] = JsonUtils.removeSpacesFromKeys(jsonObject[key2]);
      }
    }
    return jsonObject;
  }

  static getColumnsUsed(jsonObject: any): string[] {
    const columns: string[] = [];
    if (jsonObject.constructor == objectConstructor) {
      for (const key in jsonObject) {
        if (!columns.includes(key)) {
          columns.push(key);
        }
      }
    } else if (jsonObject.constructor == arrayConstructor) {
      for (const key2 in jsonObject) {
        const intColumns: string[] = JsonUtils.getColumnsUsed(jsonObject[key2]);
        for (let i = 0; i < intColumns.length; i++) {
          if (!columns.includes(intColumns[i])) {
            columns.push(intColumns[i]);
          }
        }
      }
    }
    return columns;
  }

  static isJsonFile(filePath: string): boolean {
    return strEndsWith(filePath, '.json');
  }
}

const objectConstructor = {}.constructor;
const arrayConstructor = [].constructor;
