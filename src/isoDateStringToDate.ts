export type ISO8601Date = string;

export function isoDateStringToDate(str: ISO8601Date): Date | undefined {
  if (typeof str === 'string') {
    const dateParts: string[] = str.split('-');
    const dateObj: Date = new Date();
    dateObj.setFullYear(parseInt(dateParts[0]));
    dateObj.setMonth(parseInt(dateParts[1]) - 1);
    dateObj.setDate(parseInt(dateParts[2]));
    return dateObj;
  }
  return undefined;
}
