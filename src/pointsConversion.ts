'use strict';

import { PointsMap } from '../controller/SeriesDataController';

export function convertPointsToMap(scoringSystem: Object): PointsMap | undefined {
  const output:Map<string|number, number> = new Map<string|number, number>();

  if (scoringSystem instanceof Map) {
    const ssAsMap:Map<string|number, number> = scoringSystem;

    for (const [key, value] of ssAsMap) {
      injectKeyValueToMap(key.toString(), value, output);
    }
  } else {
    for (const [key, value] of Object.entries(scoringSystem)) {
      injectKeyValueToMap(key, value, output);
    }
  }
  return output;
}

function injectKeyValueToMap(key: string, value: any, output: Map<string | number, number>) {
  const points:number = parseInt(value.toString());
  let correctedKey:string|number = key;
  try {
    const i:number = parseInt(key);
    if (!Number.isNaN(i)) {
      correctedKey = i;
    }
  } catch (err) {
    correctedKey = key;
  }
  output.set(correctedKey, points);
}

