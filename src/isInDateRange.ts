'use strict';

import { ISO8601Date, isoDateStringToDate } from './isoDateStringToDate';
import { DateRange } from '../types';

export function isInDateRange(dateStr: ISO8601Date, dateRange?: DateRange): boolean {
  if (!dateRange) {
    // No range means from and to eternity :D
    return true;
  }
  if (!dateRange.fromDate && !dateRange.toDate) {
    return true;
  }
  const targetDate: Date | undefined = isoDateStringToDate(dateStr);
  if (!targetDate) {
    return false;
  }
  if (dateRange.fromDate) {
    const fromDate: Date | undefined = isoDateStringToDate(dateRange.fromDate);
    if (fromDate && targetDate.getTime() < fromDate.getTime()) {
      return false;
    }
  }
  if (dateRange.toDate) {
    const toDate: Date | undefined = isoDateStringToDate(dateRange.toDate);
    if (toDate && targetDate.getTime() > toDate.getTime()) {
      return false;
    }
  }
  return true;
}
