'use strict';

import excelDateToISODate from './excelDateToIsoDate';

const GetDateAsISO = (dateStr: string) => {
  if (Number.isInteger(dateStr)) {
    return excelDateToISODate(parseInt(dateStr));
  } else if (dateStr) {
    try {
      if (dateStr.indexOf('/') >= 2) {
        // means we have d/m/y or m/d/y
        const parts: string[] = dateStr.split('/');
        let day;
        let month;
        let year;

        if (parts.length == 3) {
          day = parts[0];
          month = parts[1];
          if (parseInt(parts[2]) < 31) {
            year = 2000 + parseInt(parts[2]);
          } else if (parseInt(parts[2]) < 100) {
            year = 1900 + parseInt(parts[2]);
          } else {
            year = parseInt(parts[2]);
          }
        } else if (parts.length == 2) {
          // We kinda have to assume it's the current year with only the month and day being specified.
          year = new Date().getFullYear();
          if (parseInt(parts[0]) > 12) {
            // Stupid American date formats.
            month = parts[0];
            day = parts[1];
          } else {
            month = parts[1];
            day = parts[0];
          }
        }
        const retVal = year + '-' + month + '-' + day;
        return retVal;
      }
    } catch (err) {
      console.trace(`Can't parse date ${dateStr}`);
      throw err;
    }
  }
  return dateStr;
};

export default GetDateAsISO;
