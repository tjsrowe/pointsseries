'use strict';

import AWS from 'aws-sdk';
import { ZipExporter } from './ZipExporter';
import fs from 'fs';
import os from 'os';
import path from 'path';
import { rejects } from 'assert';

export class S3Exporter extends ZipExporter {
  private _s3bucket!: string;
  private _s3key!:string;

  constructor(s3bucket:string, s3key:string, outputPath?: string, eventPath?: string) {
    super(path.join(os.tmpdir(), 'tmpFile.zip'), undefined, outputPath, eventPath);
    this._s3bucket = s3bucket;
    this._s3key = s3key;
  }

  commit():Promise<void> {
    const pr:Promise<void> = new Promise((resolve, reject) => {
      super.commit().then(() => {
        this.uploadTmpFileToS3().then((location) => {
          console.log(`Uploaded to ${location}`);
          resolve();
        }).catch((err) => {
          reject(err);
        });
      }).catch((err) => {
        reject(err);
      });
    });
    return pr;
  }

  uploadTmpFileToS3():Promise<string> {
    const s3:AWS.S3 = new AWS.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    });

    const params:AWS.S3.PutObjectRequest = {
      Body: fs.createReadStream(path.join(os.tmpdir(), 'tmpFile.zip')),
      Bucket: this._s3bucket,
      Key: this._s3key,
    };
    const pr:Promise<string> = new Promise((resolve) => {
      s3.upload(params, function(err:any, data:any) {
        if (err) {
          console.error(err);
          rejects(err);
        } else {
          resolve(data.Location);
        }
      });
    });
    return pr;
  }
}
