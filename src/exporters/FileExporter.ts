'use strict';

import { EventDataDetail, SeriesDetail } from '../../src/model';
import { EventExporter, SeriesExporter } from '../../controller/DataExportController';

import fs from 'fs';
import path from 'path';

export class FileExporter implements SeriesExporter, EventExporter {
  private _eventPath!: string;
  private _seriesPath!: string;
  private _outputPath!:string;

  public get outputPath():string {
    return this._outputPath;
  }

  constructor(outputPath?: string, eventPath?: string) {
    if (outputPath) {
      this._outputPath = outputPath;
      this._seriesPath = path.join(outputPath, 'series');
    } else {
      this._outputPath = 'export';
      this._seriesPath = path.join('export', 'series');
    }

    if (eventPath) {
      this._eventPath = eventPath;
    } else if (outputPath) {
      this._eventPath = path.join(outputPath, 'event');
    } else {
      this._eventPath = path.join('export', 'event');
    }
  }

  saveSeries(series: SeriesDetail): Promise<number> {
    const filename = series.id.toString() + '.json';
    const outputFileName = path.join(this._seriesPath, filename);
    const pr: Promise<number> = new Promise((resolve, reject) => {
      fs.writeFile(outputFileName, JSON.stringify(series, undefined, 2), { encoding: 'utf-8' }, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve(series.id);
        }
      });
    });
    return pr;
  }

  saveEvent(eventData: EventDataDetail): Promise<string> {
    const outputFileName = path.join(this._eventPath, eventData.cacheFileName);
    const pr: Promise<string> = new Promise((resolve, reject) => {
      fs.writeFile(outputFileName, JSON.stringify(eventData, undefined, 2), { encoding: 'utf-8' }, function (err) {
        if (err) {
          reject(err);
        } else if (eventData.id) {
          resolve(eventData.id);
        } else {
          reject(new Error(`No id in returned eventData: ${eventData}`));
        }
      });
    });
    return pr;
  }

  checkConnection(): Promise<void> {
    const seriesPath: string = path.resolve(this._seriesPath);
    if (!fs.existsSync(seriesPath)) {
      fs.mkdirSync(seriesPath, { recursive: true });
    }

    const eventPath: string = path.resolve(this._eventPath);
    if (!fs.existsSync(eventPath)) {
      fs.mkdirSync(eventPath, { recursive: true });
    }
    return Promise.resolve();
  }

  saveSeriesList(list: SeriesDetail[]): Promise<void> {
    const listFile: string = path.resolve(this._seriesPath, 'list.json');
    fs.writeFileSync(listFile, JSON.stringify(list), 'utf8');
    return Promise.resolve();
  }

  commit():Promise<void> {
    return Promise.resolve();
  }
}
