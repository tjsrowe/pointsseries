'use strict';

import { EventDataDetail, SeriesDetail } from '../../src/model';
import { EventExporter, SeriesExporter } from '../../controller/DataExportController';

import { DynamoDBClientConfig } from '../../controller/dynamo/DDB';
import { EventDDB } from '../../controller/dynamo/EventDDB';
import { SeriesDDB } from '../../controller/dynamo/SeriesDDB';

export class DynamoExporter implements SeriesExporter, EventExporter {
  private _series: SeriesDDB;
  private _event: EventDDB;
  private _connectionDetails: DynamoDBClientConfig | undefined;

  constructor(connectionDetails: DynamoDBClientConfig) {
    this._series = new SeriesDDB();
    this._event = new EventDDB();
    this._connectionDetails = connectionDetails;
  }

  saveSeries(series: SeriesDetail): Promise<number> {
    return this._series.save(series);
  }

  saveEvent(eventData: EventDataDetail): Promise<string> {
    return this._event.save(eventData);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  saveSeriesList(list: SeriesDetail[]): Promise<void> {
    // return this._series.saveList()
    return Promise.resolve();
  }

  checkConnection() {
    if (this._connectionDetails) {
      this._series.connect(this._connectionDetails);
      this._event.connect(this._connectionDetails);
    }
    this._connectionDetails = undefined;
  }

  commit():Promise<void> {
    return Promise.resolve();
  }
}
