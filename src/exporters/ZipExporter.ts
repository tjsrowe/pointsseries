'use strict';

import { EventDataDetail, SeriesDataDetail, SeriesDetail } from '../../src/model';
import { EventExporter, SeriesExporter } from '../../controller/DataExportController';

import archiver from 'archiver';
import fs from 'fs';
import path from 'path';

export class ZipExporter implements SeriesExporter, EventExporter {
  private _zipFile?: string;
  private _archive!:archiver.Archiver;
  private _seriesDir:string;
  private _eventDir:string;
  private _outputStream?:fs.WriteStream;

  public get OutputStream():fs.WriteStream|undefined {
    return this._outputStream;
  }

  constructor(zipFile?:string, os?:fs.WriteStream, seriesPath?: string, eventPath?: string) {
    this._seriesDir = seriesPath || 'series';
    this._eventDir = eventPath || 'events';
    this._zipFile = zipFile;

    if (!os) {
      if (!zipFile) {
        throw new Error('Must specify an output filename if no output stream is provided');
      }
      os = fs.createWriteStream(zipFile);
    }
    this._outputStream = os;

    this._archive = archiver('zip', {
      zlib: { level: 9 } // Sets the compression level.
    });
    this._archive.pipe(os);
  }
  saveSeriesList(list: SeriesDataDetail[]): Promise<void> {
    const listFile: string = path.resolve(this._seriesDir, 'list.json');
    try {
      this._archive.append(JSON.stringify(list), { name: listFile });
      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  checkConnection(): void {
    return;
  }

  saveEvent(eventData: EventDataDetail): Promise<string> {
    const outputFileName = path.join(this._eventDir, eventData.cacheFileName);
    const pr: Promise<string> = new Promise((resolve, reject) => {
      try {
        this._archive.append(JSON.stringify(eventData, undefined, 2), { name: outputFileName });
        resolve(eventData.id);
      } catch (err) {
        reject(err);
      }
    });
    return pr;
  }

  saveSeries(series: SeriesDetail): Promise<number> {
    const filename = series.id.toString() + '.json';
    const outputFileName = path.join(this._seriesDir, filename);
    const pr: Promise<number> = new Promise((resolve, reject) => {
      try {
        this._archive.append(JSON.stringify(series, undefined, 2), { name: outputFileName });
        resolve(series.id);
      } catch (err) {
        reject(err);
      }
    });
    return pr;
  }

  commit():Promise<void> {
    const pr:Promise<void> = new Promise((resolve, reject) => {
      try {
        console.log(`Closing output zip archive ${this._zipFile}`);
        this._archive.finalize().then(() => {
          resolve();
        }).catch((err) => {
          reject(err);
        });
      } catch (err) {
        reject(err);
      }
    });
    return pr;
  }
}
