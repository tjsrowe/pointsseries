'use strict';

import { AbstractDataController } from './AbstractDataController';
import { License } from '../src/model';

export class LicenseDataController extends AbstractDataController {
  hasMultipleOfSingleLicense(licenseHashes: any) {
    const counts: any = {};
    if (!Array.isArray(licenseHashes)) {
      return false;
    }
    for (let i = 0; i < licenseHashes.length; i++) {
      const cl = licenseHashes[i];
      if (!counts[cl.LicenseType]) {
        counts[cl.LicenseType] = 1;
      } else {
        return true;
      }
    }
    return false;
  }

  licenseMatches(lic1: License, lic2: License): boolean {
    if (!lic1 || !lic2) {
      return false;
    }
    if (lic1.LicenseType == lic2.LicenseType && lic1.LicenseNumber == lic2.LicenseNumber) {
      return true;
    }
    return false;
  }

  hasLicenseHashMatching(licenseToCheck: License|License[], licenseToFind: any): boolean {
    if (Array.isArray(licenseToCheck)) {
      for (let i = 0; i < licenseToCheck.length; i++) {
        const currentLicenseToCheck = licenseToCheck[i];
        if (this.licenseMatches(currentLicenseToCheck, licenseToFind)) {
          return true;
        }
      }
    } else {
      if (this.licenseMatches(licenseToCheck, licenseToFind)) {
        return true;
      }
    }
    return false;
  }
}
