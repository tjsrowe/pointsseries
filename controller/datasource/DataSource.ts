'use strict';

export interface DataSource<T, idtype> {
  get(id:idtype):Promise<T>;
  getAll():Promise<T[]>;
  save(item:T):Promise<idtype>;
}

export default DataSource;
