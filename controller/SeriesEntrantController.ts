'use strict';

import { AbstractDataController } from './AbstractDataController';
import { CategoryController, } from './CategoryController';
import { SeriesEntrant } from '../src/model';

export class SeriesEntrantController extends AbstractDataController {
  private _categoryController!:CategoryController;

  set CategoryController(cc:CategoryController) {
    this._categoryController = cc;
  }

  // compareEntrants(map:Map<string, number>, a:SeriesEntrant, b:SeriesEntrant):number {
  //   const keyA:CategoryKey = createNameWithGender(a.Category, a.Gender);
  //   const keyB:CategoryKey = createNameWithGender(b.Category, b.Gender);
  //   const comparison:number = CategoryController.compareCategoryOrderNames(map, keyA, keyB);
  //   if (comparison == 0) {
  //     return (b.Points ? b.Points : 0) - (a.Points ? a.Points : 0);
  //   } else {
  //     return comparison;
  //   }
  // }

  compareEntrantsOnPoints(a:SeriesEntrant, b:SeriesEntrant):number {
    return (b.Points ? b.Points : 0) - (a.Points ? a.Points : 0);
  }

  // sortSeriesEntrants(series:Series):SeriesEntrant[] {
  //   return this.sortSeriesEntrantsFromMap(series.CategoryMap, series.Entrants);
  // }

  // private sortSeriesEntrantsFromMap(map:Map<string, number>, list:SeriesEntrant[]):SeriesEntrant[] {
  //   return list.sort((a:SeriesEntrant, b:SeriesEntrant) => this.compareEntrants(map, a, b));
  // }

  sortCategoryEntrants(list:SeriesEntrant[]):SeriesEntrant[] {
    return list.sort((a:SeriesEntrant, b:SeriesEntrant) => this.compareEntrantsOnPoints(a, b));
  }
}
