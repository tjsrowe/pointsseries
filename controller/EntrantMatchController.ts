'use strict';

import { AbstractEntrant } from '../src/model';
import { CategoryController } from './CategoryController';
import { CategorySet } from '../src/utils/categorySet';
import { EntrantDataController } from './EntrantDataController';
import { LOG_OUTPUT_NEW_LICENSE } from '../env';
import { LicenseDataController } from './LicenseDataController';
import assert from 'assert';
import dumpError from '../src/dumpError';

const ENABLE_DOB_CHECK = false;

export class EntrantMatchController {
  config: any;
  _licenseDataController!: LicenseDataController;
  _entrantDataController!: EntrantDataController;
  _categoryController!: CategoryController;

  public set LicenseDataController(ldc: LicenseDataController) {
    this._licenseDataController = ldc;
  }

  public set EntrantDataController(edc: EntrantDataController) {
    this._entrantDataController = edc;
  }

  public set CategoryController(cc: CategoryController) {
    this._categoryController = cc;
  }

  fullNameMatches(entrant1: AbstractEntrant, entrant2: AbstractEntrant) {
    if (EntrantDataController.hasFullNameField(entrant1) && EntrantDataController.hasFullNameField(entrant2)) {
      if (this.hasMatchingField(entrant1, entrant2, 'Name', true)) {
        return true;
      }
    }
    return false;
  }

  createFullNameFromPartials(entrantWithPartials: AbstractEntrant) {
    let fullStringFromPartials = '';
    if (entrantWithPartials.Firstname && !Array.isArray(entrantWithPartials.Firstname)) {
      const firstname: string = entrantWithPartials.Firstname.trim();
      if (entrantWithPartials.Surname) {
        fullStringFromPartials = firstname + ' ' + entrantWithPartials.Surname.trim().toUpperCase();
      } else {
        fullStringFromPartials = firstname;
      }
    } else if (entrantWithPartials.Surname && !Array.isArray(entrantWithPartials.Surname)) {
      fullStringFromPartials = entrantWithPartials.Surname.trim().toUpperCase();
    } else {
      return null;
    }
    if (fullStringFromPartials != '') {
      return fullStringFromPartials;
    }
    return null;
  }

  cleanupFullName(name: string): string {
    name = name.trim().replaceAll('  ', ' ');
    return name;
  }

  comparePartialToFullName(entrantWithPartials: AbstractEntrant, entrantWithFull: AbstractEntrant): boolean {
    if (!entrantWithFull.FullName) {
      return false;
    }
    let fullStringFromPartials = this.createFullNameFromPartials(entrantWithPartials);

    if (!fullStringFromPartials) {
      return false;
    }
    fullStringFromPartials = fullStringFromPartials.toUpperCase();
    const cleanNameFullName: string = this.cleanupFullName(entrantWithFull.FullName);
    if (Array.isArray(entrantWithFull.FullName)) {
      return this.includesInsensitive([cleanNameFullName], fullStringFromPartials);
    } else if (fullStringFromPartials == cleanNameFullName.toUpperCase()) {
      return true;
    }
    return false;
  }

  matchNames(entrant1: AbstractEntrant, entrant2: AbstractEntrant): boolean {
    if (!EntrantDataController.hasPartNameFields(entrant1) || !EntrantDataController.hasPartNameFields(entrant2)) {
      if (this.fullNameMatches(entrant1, entrant2)) {
        return true;
      } else {
        if (EntrantDataController.hasPartNameFields(entrant1) && !EntrantDataController.hasPartNameFields(entrant2)) {
          if (this.comparePartialToFullName(entrant1, entrant2)) {
            return true;
          }
        } else if (
          EntrantDataController.hasPartNameFields(entrant2) &&
          !EntrantDataController.hasPartNameFields(entrant1)
        ) {
          if (this.comparePartialToFullName(entrant2, entrant1)) {
            return true;
          }
        } else {
          return false;
        }
      }
    }

    const firstnameMatches = this.hasMatchingFirstname(entrant1, entrant2);
    const surnameMatches = this.hasMatchingSurname(entrant1, entrant2);
    if (firstnameMatches && surnameMatches) {
      return true;
    }

    return false;
  }

  includesInsensitive(array: any[], searchValue: string): boolean {
    const searchValueUpper = searchValue.toUpperCase();
    for (let i = 0; i < array.length; i++) {
      const arrUpper = array[i].toUpperCase();
      if (arrUpper == searchValueUpper) {
        return true;
      }
    }
    return false;
  }

  hasMatchingField(existingEntry: any, lookupEntry: any, fieldName: string, caseInsensitive: boolean): boolean {
    let searchValue = lookupEntry[fieldName];
    if (caseInsensitive && typeof searchValue == 'string') {
      searchValue = searchValue.toUpperCase().trim();
    }
    if (Array.isArray(existingEntry[fieldName])) {
      if (!caseInsensitive) {
        return existingEntry[fieldName].includes(searchValue);
      } else if (searchValue) {
        return this.includesInsensitive(existingEntry[fieldName], searchValue);
      }
    } else {
      let compareValue = existingEntry[fieldName];
      if (compareValue != undefined && caseInsensitive) {
        compareValue = compareValue.toUpperCase().trim();
      }
      if (searchValue == compareValue) {
        return true;
      }
    }
    return false;
  }

  hasMatchingFirstname(existingEntry: AbstractEntrant, lookupEntry: AbstractEntrant): boolean {
    return this.hasMatchingField(existingEntry, lookupEntry, 'Firstname', true);
  }

  hasMatchingSurname(existingEntry: AbstractEntrant, lookupEntry: AbstractEntrant): boolean {
    return this.hasMatchingField(existingEntry, lookupEntry, 'Surname', true);
  }

  matchEntrantPerson(e1:AbstractEntrant, e2:AbstractEntrant, eventName?:string):boolean {
    const rdn1: string = this._entrantDataController.getRiderDisplayName(e1);
    const rdn2: string = this._entrantDataController.getRiderDisplayName(e2);
    const licensesMatch:boolean = this.matchEntrantLicenses(e1, e2);
    if (licensesMatch && this.hasMatchingSurname(e1, e2)) {
      if (LOG_OUTPUT_NEW_LICENSE && eventName &&
        this._licenseDataController.hasMultipleOfSingleLicense(e1.LicenseHash)) {
        const e1lhstr: string = JSON.stringify(e1.LicenseHash);
        console.log(`Matched rider ${rdn2} in '${eventName}' who had more than one license ${e1lhstr}`);
      }

      return true;
    } else if (licensesMatch && eventName) {
      console.warn(`License was thought to match for ${rdn2} + in event '${eventName}'
         with ${rdn1} already in series, but surname does not match.`);
    }
    if (this.matchNames(e1, e2)) {
      if (e1.LicenseHash && e2.LicenseHash && !licensesMatch) {
        return false;
      } else if (!ENABLE_DOB_CHECK) {
        if (!this.matchDobIfAvailable(e1, e2) && eventName) {
          console.warn(`${rdn2} matched on name but had different DOB in ${eventName}`);
        }
        return true;
      } else if (this.matchDobIfAvailable(e1, e2)) {
        return true;
      }
      if (eventName) {
        console.warn(`${rdn2} matched on name but had different DOB in ${eventName}`);
      }
    }
    return false;
  }

  matchEntrant(categoryList:CategorySet, e1: AbstractEntrant, e2: AbstractEntrant): boolean {
    assert(this._categoryController != undefined,
      'categoryController not set in EntrantMatchController when calling matchEntrant');
    assert(this._entrantDataController != undefined,
      'entrantDataController not set in EntrantMatchController when calling matchEntrant');
    assert(this._licenseDataController != undefined,
      'licenseDataController not set in EntrantMatchController when calling matchEntrant');
    try {
      if (CategoryController.categoryMatches(categoryList, e1.Category, e2.Category)) {
        return this.matchEntrantPerson(e1, e2);
      }
    } catch (err) {
      dumpError(err);
      console.warn('Error occurred matching entrants: ' + err);
      return false;
    }
    return false;
  }

  matchEntrantLicenses(entrant1: AbstractEntrant, entrant2: AbstractEntrant): boolean {
    if (entrant1.LicenseHash && entrant2.LicenseHash &&
      this._licenseDataController.hasLicenseHashMatching(entrant1.LicenseHash, entrant2.LicenseHash)) {
      if (Array.isArray(entrant1.LicenseHash)) {
        // check each license
        for (let i = 0; i < entrant1.LicenseHash.length; i++) {
          const currentLicenseToCheck = entrant1.LicenseHash[i];
          if (this._licenseDataController.hasLicenseHashMatching(entrant2.LicenseHash, currentLicenseToCheck)) {
            return true;
          }
        }
      } else {
        // only one license to check
        if (this._licenseDataController.hasLicenseHashMatching(entrant2.LicenseHash, entrant1.LicenseHash)) {
          return true;
        }
      }
    }
    return false;
  }

  // TODO: DOB fields on Entrant objects?
  matchDobIfAvailable(entrant1: AbstractEntrant, entrant2: AbstractEntrant): boolean {
    if (this._entrantDataController.hasDob(entrant1) && this._entrantDataController.hasDob(entrant2)) {
      return entrant1.DOB == entrant2.DOB;
    } else if (this._entrantDataController.hasDobHash(entrant1) && this._entrantDataController.hasDobHash(entrant2)) {
      return entrant1.DOB_Hash == entrant2.DOB_Hash;
    } else {
      return true;
    }
  }
}
