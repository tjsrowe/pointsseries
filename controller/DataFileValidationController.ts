'use strict';

import {
  ErrorLevel,
  ResultFileVerificationResult,
  ValidationErrorType,
  ValidationInfo,
  findFirstFieldFromVariants,
  processLicenseField,
} from './EventDataFileController';
import { FileResultDataAliases, ResultDataAliases } from 'config/Config';

import { AbstractDataController } from './AbstractDataController';
import { DataFileController } from './DataFileController';
import { EntrantDataController } from './EntrantDataController';
import { License } from '../src/model';
import { UploadedFile } from 'express-fileupload';

export class DataFileValidationController extends AbstractDataController {
  private dataFileController!: DataFileController;
  private entrantDataController!: EntrantDataController;

  public set DataFileController(dfc: DataFileController) {
    this.dataFileController = dfc;
  }

  public set EntrantDataController(edc: EntrantDataController) {
    this.entrantDataController = edc;
  }

  verifyResultFile(file: UploadedFile, seriesId: number | undefined): Promise<ResultFileVerificationResult> {
    const dfc: DataFileController = this.dataFileController!;
    const pr: Promise<ResultFileVerificationResult> = new Promise<ResultFileVerificationResult>((resolve, reject) => {
      try {
        dfc.saveTempFromUpload(file).then((tempFileName) => {
          const prData: Promise<JSON[]> = dfc.dataFileAsJson(tempFileName);
          prData.then((data: JSON[]) => {
            try {
              this.validateEventData(data, seriesId)
                .then((result: ResultFileVerificationResult) => {
                  resolve(result);
                })
                .catch((err: any) => {
                  reject(err);
                });
            } catch (error) {
              reject(error);
            }
          });
        });
      } catch (error) {
        reject(error);
      }
    });
    return pr;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  validateEventData(data: JSON[], seriesId: number | undefined): Promise<ResultFileVerificationResult> {
    const pr: Promise<ResultFileVerificationResult> = new Promise((resolve) => {
      const result: ResultFileVerificationResult = this.validateDataColumns(data);
      resolve(result);
    });
    return pr;
  }

  validateLicenses(data: JSON[]): ValidationInfo[] {
    const validationInfo: ValidationInfo[] = [];
    const licenseFields: string[] = this.configManager.getLicenseFields();
    const licenseTypes: string[] = this.configManager.getLicenseTypes();

    data.forEach(async (row: any, rowNumber: number) => {
      const licenseFieldToUse = findFirstFieldFromVariants(row, licenseFields);
      const license: License = processLicenseField(row[licenseFieldToUse], licenseTypes);
      if (!license) {
        const name: string = this.entrantDataController.getRiderDisplayName(row);
        const vi: ValidationInfo = {
          errorLevel: ErrorLevel.WARNING,
          errorType: ValidationErrorType.MISSING_DATA,
          lineNumber: rowNumber,
          message: `No license data for ${name}`,
        };
        validationInfo.push(vi);
      }
    });
    return validationInfo;
  }

  validateRequiredColumns(data: JSON[]): ValidationInfo[] {
    const validationIssues: ValidationInfo[] = [];
    const viNameCol: ValidationInfo | undefined = this.hasNameColumn(data);
    if (viNameCol) {
      validationIssues.push(viNameCol);
    }
    const viResultCol:ValidationInfo|undefined = this.hasResultColumn(data);
    if (viResultCol) {
      validationIssues.push(viResultCol);
    }
    const viCategroyCol:ValidationInfo|undefined = this.hasCategoryColumn(data);
    if (viCategroyCol) {
      validationIssues.push(viCategroyCol);
    }
    return validationIssues;
  }

  findMatchingFieldNameFromAlternatives(
    availableColumns: string[],
    primaryFieldName: string,
    alternativeFieldNames: string[]
  ): string | undefined {
    let match: string | undefined;

    availableColumns.forEach((column: string) => {
      if (match === undefined && column.toLowerCase() == primaryFieldName.toLowerCase()) {
        match = column;
      } else if (match === undefined) {
        alternativeFieldNames.forEach((alt: string) => {
          if (match === undefined && column.toLowerCase() == alt.toLowerCase()) {
            match = column;
          }
        });
      }
    });
    return match;
  }

  matchFieldsToAvailableColumns(columns: string[], fieldSearchSet: ResultDataAliases): FileResultDataAliases {
    const aliases: FileResultDataAliases = {};
    const aliasesAsAny: any = aliases;

    for (const [keyField, keyAlternatives] of Object.entries(fieldSearchSet)) {
      const alt: string | undefined = this.findMatchingFieldNameFromAlternatives(columns, keyField, keyAlternatives);
      aliasesAsAny[keyField] = alt;
    }

    return aliases;
  }

  hasResultColumn(data: JSON[] | any): ValidationInfo | undefined {
    const fieldSearchSet: ResultDataAliases = this.configManager.getResultDataAliases();
    const aliases: FileResultDataAliases = this.matchFieldsToAvailableColumns(data.Columns, fieldSearchSet);
    if ((aliases.Result)) {
      return undefined;
    } else {
      const vi: ValidationInfo = {
        errorLevel: ErrorLevel.ERROR,
        errorType: ValidationErrorType.MISSING_FIELD,
        lineNumber: 0,
        message: 'No result usable field found in file',
      };
      console.debug(data.Columns);
      return vi;
    }
  }

  hasCategoryColumn(data: JSON[] | any): ValidationInfo | undefined {
    const fieldSearchSet: ResultDataAliases = this.configManager.getResultDataAliases();
    const aliases: FileResultDataAliases = this.matchFieldsToAvailableColumns(data.Columns, fieldSearchSet);
    if ((aliases.Category)) {
      return undefined;
    } else {
      const vi: ValidationInfo = {
        errorLevel: ErrorLevel.ERROR,
        errorType: ValidationErrorType.MISSING_FIELD,
        lineNumber: 0,
        message: 'No category usable field found in file',
      };
      console.debug(data.Columns);
      return vi;
    }
  }

  hasNameColumn(data: JSON[] | any): ValidationInfo | undefined {
    const fieldSearchSet: ResultDataAliases = this.configManager.getResultDataAliases();

    const aliases: FileResultDataAliases = this.matchFieldsToAvailableColumns(data.Columns, fieldSearchSet);
    if ((aliases.Firstname && aliases.Surname) || aliases.FullName) {
      // ok
      return undefined;
    } else {
      if (aliases.Firstname && !aliases.Surname) {
        const vi: ValidationInfo = {
          errorLevel: ErrorLevel.ERROR,
          errorType: ValidationErrorType.MISSING_FIELD,
          lineNumber: 0,
          message: 'Got Firstname field but no Surname field in file',
        };
        return vi;
      } else if (aliases.Surname && !aliases.Firstname) {
        const vi: ValidationInfo = {
          errorLevel: ErrorLevel.ERROR,
          errorType: ValidationErrorType.MISSING_FIELD,
          lineNumber: 0,
          message: 'Got Surname field but no Firstname field in file',
        };
        console.debug(data.Columns);
        return vi;
      }
      const vi: ValidationInfo = {
        errorLevel: ErrorLevel.ERROR,
        errorType: ValidationErrorType.MISSING_FIELD,
        lineNumber: 0,
        message: 'No name usable field found in file - had neither valid Firstname nor Surname, ' +
          'wanted FullName or alias',
      };
      return vi;
    }
  }

  validateDataColumns(data: JSON[]): ResultFileVerificationResult {
    const result: ResultFileVerificationResult = {
      issues: [],
      valid: false,
    };
    const requiredColumns: ValidationInfo[] = this.validateRequiredColumns(data);
    if (requiredColumns.length == 0) {
      const licenseValidationIssues: ValidationInfo[] = this.validateLicenses(data);
      licenseValidationIssues.forEach((lvi) => result.issues.push(lvi));
    } else {
      requiredColumns.forEach((lvi) => result.issues.push(lvi));
    }
    if (result.issues.length == 0) {
      result.valid = true;
    }

    return result;
  }
}
