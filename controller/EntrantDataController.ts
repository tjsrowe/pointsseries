'use strict';

import { AbstractEntrant } from '../src/model/entrant';
import { EntrantMatchController } from './EntrantMatchController';
import { LOG_OUTPUT_NEW_LICENSE } from '../env';
import { LicenseDataController } from './LicenseDataController';
import { assert } from 'console';

const LOG_OUTPUT_LICENSE_MERGED = false;
const LOG_OUTPUT_ADD_NAME_VARIANT = false;
const LOG_OUTPUT_FIELDS_MERGED = false;
const LOG_OUTPUT_MERGE_DOB = false;
const LOG_OUTPUT_FIELD_TO_ARRAY = false;

export class EntrantDataController {
  config: any;
  private _licenseDataController!: LicenseDataController;
  private _entrantMatchController!: EntrantMatchController;

  public set EntrantMatchController(emc: EntrantMatchController) {
    this._entrantMatchController = emc;
  }

  public set LicenseDataController(ldc: LicenseDataController) {
    this._licenseDataController = ldc;
  }

  getFirstFieldValue(element: any, index: string): any {
    if (Array.isArray(element[index])) {
      return element[index][0];
    } else {
      return element[index];
    }
  }

  requiresLicenseCheck(entrant: any): boolean {
    if (Array.isArray(entrant.LicenseHash)) {
      return true;
    } else if (entrant.LicenseHash) {
      return true;
    }
    return false;
  }

  getRiderFirstname(entrant: any): string {
    return this.getFirstFieldValue(entrant, 'Firstname');
  }

  getRiderSurname(entrant: any): string {
    const surname: string = this.getFirstFieldValue(entrant, 'Surname');
    if (!surname) {
      throw new Error('Rider has no surname');
    }
    return surname.toUpperCase();
  }

  getRiderName(entrant: any): string {
    return this.getFirstFieldValue(entrant, 'Name');
  }

  mergeLicenseInto(existingEntry: any, newEntry: any, eventName?: string): number {
    const existingName: string = this.getRiderDisplayName(existingEntry);
    if (
      this.requiresLicenseCheck(newEntry) &&
      !this._licenseDataController.hasLicenseHashMatching(existingEntry.LicenseHash, newEntry.LicenseHash)
    ) {
      const hasDupeBefore = this._licenseDataController.hasMultipleOfSingleLicense(existingEntry.LicenseHash);
      this.addFieldVariant(existingEntry, newEntry, 'LicenseHash');
      if (LOG_OUTPUT_LICENSE_MERGED) {
        console.debug(`Merged license for ${this.getRiderDisplayName(newEntry)} into ${existingName}`);
      }
      const hasDupeAfter = this._licenseDataController.hasMultipleOfSingleLicense(existingEntry.LicenseHash);
      if (LOG_OUTPUT_NEW_LICENSE && !hasDupeBefore && hasDupeAfter) {
        const existingDisplayName = this.getRiderDisplayName(existingEntry);
        if (eventName) {
          console.warn(`A second license of the same type (${newEntry.LicenseHash.LicenseType}) was added to \
              ${existingDisplayName} when processing event ${eventName}`);
        }
      }
      return 1;
    }
    return 0;
  }

  hasDobHash(entrant: AbstractEntrant): boolean {
    if (entrant.DOB_Hash) {
      return true;
    }
    return false;
  }

  // TODO: Ensure data straight out of sheet recognised DOB
  hasDob(entrant: any): boolean {
    if (entrant.DOB) {
      return true;
    }
    return false;
  }

  getRiderDisplayName(entrant: AbstractEntrant): string {
    if (EntrantDataController.hasFullNameField(entrant) && !EntrantDataController.hasPartNameFields(entrant)) {
      return this.getRiderName(entrant);
    }
    const firstname = this.getRiderFirstname(entrant);
    const surname = this.getRiderSurname(entrant);
    if (firstname && surname) {
      return firstname + ' ' + surname.toUpperCase();
    } else {
      return this.getRiderName(entrant);
    }
  }

  static hasFullNameField(entrant: AbstractEntrant): boolean {
    if (entrant.FullName) {
      return true;
    }
    return false;
  }

  static hasPartNameFields(entrant: AbstractEntrant): boolean {
    if (entrant.Firstname && entrant.Surname) {
      return true;
    }
    return false;
  }

  static hasNameFields(entrant: AbstractEntrant): boolean {
    return EntrantDataController.hasFullNameField(entrant) || EntrantDataController.hasPartNameFields(entrant);
  }

  mergeNameInto(existingEntry: AbstractEntrant, newEntry: AbstractEntrant): number {
    assert(this._entrantMatchController, 'EntrantMatchController must be configured on EntrantDataController');

    const displayName: string = this.getRiderDisplayName(newEntry);
    let fieldsMerged = 0;
    if (
      EntrantDataController.hasFullNameField(newEntry) &&
      !this._entrantMatchController.fullNameMatches(existingEntry, newEntry)
    ) {
      this.addFieldVariant(existingEntry, newEntry, 'Name');
      fieldsMerged++;
      if (LOG_OUTPUT_ADD_NAME_VARIANT) {
        const newDisplayName: string = this.getRiderDisplayName(newEntry);
        console.debug(`Add full name variant for ${newDisplayName} to existing rider ${displayName}`);
      }
    }
    if (EntrantDataController.hasPartNameFields(newEntry)) {
      if (!this._entrantMatchController.hasMatchingFirstname(existingEntry, newEntry)) {
        this.addFieldVariant(existingEntry, newEntry, 'Firstname');
        fieldsMerged++;
        if (LOG_OUTPUT_ADD_NAME_VARIANT) {
          console.debug(`Added firstname variant ${newEntry.Firstname} to existing rider ${displayName}`);
        }
      }
      if (!this._entrantMatchController.hasMatchingSurname(existingEntry, newEntry)) {
        this.addFieldVariant(existingEntry, newEntry, 'Surname');
        fieldsMerged++;
        if (LOG_OUTPUT_ADD_NAME_VARIANT) {
          console.debug(`Added surname variant ${newEntry.Surname} to existing rider ${displayName}`);
        }
      }
    }
    return fieldsMerged;
  }

  mergeEntrantIntoExisting(existingEntry: any, newEntry: any, eventName?: string): void {
    let fieldsMerged = 0;
    fieldsMerged += this.mergeLicenseInto(existingEntry, newEntry, eventName);
    fieldsMerged += this.mergeNameInto(existingEntry, newEntry);
    fieldsMerged += this.mergeDobInto(existingEntry, newEntry);
    if (fieldsMerged > 0) {
      const displayName: string = this.getRiderDisplayName(existingEntry);
      if (LOG_OUTPUT_FIELDS_MERGED) {
        console.debug(`Merged ${fieldsMerged} additional field values in to ${displayName}`);
      }
    }
  }

  mergeDobInto(existingEntry: any, newEntry: any): number {
    if (!this.hasDob(existingEntry)) {
      if (this.hasDob(newEntry)) {
        if (LOG_OUTPUT_MERGE_DOB) {
          console.debug(`Merging DOB for ${this.getRiderDisplayName(newEntry)} into \
             ${this.getRiderDisplayName(existingEntry)}`);
        }
        this.addFieldVariant(existingEntry, newEntry, 'DOB');
        return 1;
      } else if (this.hasDobHash(newEntry)) {
        if (LOG_OUTPUT_MERGE_DOB) {
          console.debug(`Merging DOB_hash for ${this.getRiderDisplayName(newEntry)} \
               into ${this.getRiderDisplayName(existingEntry)}`);
        }
        this.addFieldVariant(existingEntry, newEntry, 'DOB_Hash');
        return 1;
      }
    }
    return 0;
  }

  addFieldVariant(existingEntry: any, newEntry: any, field: string): void {
    if (!Array.isArray(existingEntry[field])) {
      const existingValue = existingEntry[field];
      existingEntry[field] = [];
      if (existingValue) {
        existingEntry[field].push(existingValue);
        if (LOG_OUTPUT_FIELD_TO_ARRAY) {
          console.debug(`Converted single ${field} for entrant \
              ${this.getRiderDisplayName(newEntry)} field to array.`);
        }
      }
    }
    existingEntry[field].push(newEntry[field]);
  }
}
