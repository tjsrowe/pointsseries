/* eslint-disable operator-linebreak */
'use strict';

import * as ExcelJS from 'exceljs';

import { AbstractEntrant, SeriesEntrant, SeriesResult } from '../src/model';
import { DateRange, SeriesEvent } from '../types';
import { Workbook, Worksheet } from 'exceljs';

import { AbstractDataController } from './AbstractDataController';
import { Gender } from '../src/model/entrant';
import { SeriesDataController } from './SeriesDataController';
import Stream from 'stream';
import { titleCase } from 'title-case';

export class ExcelSeriesController extends AbstractDataController {
  private _seriesDataController!: SeriesDataController;

  public set SeriesDataController(sdc: SeriesDataController) {
    this._seriesDataController = sdc;
  }

  seriesToExcelStream(
    seriesData: SeriesResult,
    filename: string,
    outputStream: Stream,
    beforeWriteStream?: Function
  ): Promise<Workbook> {
    const prWorkbook: Promise<Workbook> = new Promise<Workbook>((resolve, reject) => {
      const options: ExcelJS.stream.xlsx.WorkbookStreamWriterOptions = {
        filename: filename,
        stream: outputStream,
        useSharedStrings: true,
        useStyles: false,
        zip: undefined!,
      };

      const json: SeriesResult = seriesData;
      if (beforeWriteStream) {
        beforeWriteStream();
      }
      const wb: Promise<Workbook> = this.createWorkbookAndStream(options, json);
      wb.then((workbook) => {
        resolve(workbook);
      }).catch((err) => {
        console.trace('Error: ' + err);
        reject(err);
      });
    });
    return prWorkbook;
  }

  getExcelFromSeries(
    outputStream: Stream,
    seriesId: string,
    dateRange: DateRange,
    beforeWriteSream?: Function
  ): Promise<Workbook> {
    const prWorkbook: Promise<Workbook> = new Promise<Workbook>((resolve, reject) => {
      this._seriesDataController
        .getSeriesResultsById(seriesId, dateRange)
        .then((data: SeriesResult) => {
          const filename = `${seriesId}.xlsx`;
          resolve(this.seriesToExcelStream(data, filename, outputStream, beforeWriteSream));
        })
        .catch((err) => reject(err));
    });
    return prWorkbook;
  }

  private createWorkbookAndStream(
    options: ExcelJS.stream.xlsx.WorkbookStreamWriterOptions,
    seriesJson: SeriesResult
  ): Promise<ExcelJS.stream.xlsx.WorkbookWriter> {
    const wbPromise: Promise<ExcelJS.stream.xlsx.WorkbookWriter> = new Promise((resolve, reject) => {
      try {
        const workbook: ExcelJS.stream.xlsx.WorkbookWriter = new ExcelJS.stream.xlsx.WorkbookWriter(options);
        this.makePointsSheet(workbook, seriesJson);
        this.makeStatsSheet(workbook, seriesJson);
        // workbook.addWorksheet(sheet);
        // workbook.addWorksheet(statssheet);
        workbook.commit().then(() => {
          resolve(workbook);
        });
      } catch (err) {
        reject(err);
      }
    });
    return wbPromise;
  }

  isWomensCategory(categoryName: string): boolean {
    return categoryName.indexOf('Female') != -1 ||
      categoryName.indexOf('Women') != -1;
  }

  makeStatsSheet(workbook: any, seriesResult: SeriesResult) {
    const statSheet = workbook.addWorksheet('Stats', {
      views: [{ state: 'frozen', xSplit: 1, ySplit: 1 }],
    });

    const headerRow = ['Category', 'Participants'];
    statSheet.addRow(headerRow);

    const categories: Map<string, number> = new Map<string, number>();

    let maleCount = 0;
    let femaleCount = 0;
    let totalEntrants = 0;

    seriesResult.categoryEntrants.forEach((entrants:SeriesEntrant[]) => {
      const currentCategoryName: AbstractEntrant['Category'] = entrants[0].Category;
      categories.set(currentCategoryName, entrants.length);

      const femaleEntrants: SeriesEntrant[] = entrants.filter((e: SeriesEntrant) => e.Gender === Gender.WOMEN);
      femaleCount = femaleCount + femaleEntrants.length;
      maleCount = maleCount + (entrants.length - femaleEntrants.length);

      totalEntrants = totalEntrants + entrants.length;
    });

    categories.forEach((entrantCount: number, categoryName: string) => {
      const dataRow = [categoryName, entrantCount];
      statSheet.addRow(dataRow);
    });

    statSheet.addRow(['All Mens categories', maleCount]);
    statSheet.addRow(['All Womens categories', femaleCount]);
    statSheet.addRow(['All entrants', totalEntrants]);
    return statSheet;
  }

  // seriesToXlsStreamCallback(options:any, seriesJson:JSON, onComplete:Function, onError:Function) {
  //     console.log(JSON.stringify(seriesJson));

  //     try {
  //         var workbook = new ExcelJS.stream.xlsx.WorkbookWriter(options);
  //         var sheet = this.makePointsSheet(workbook, seriesJson);
  //         workbook.addWorksheet(sheet);
  //         workbook.commit();

  //         onComplete(workbook);
  //     } catch (err) {
  //         onError(err);
  //     }
  // };

  makeEventCell(rowData: any, targetEventId: number, result: any[]) {
    let written = false;
    for (let idx = 0; idx < result.length; idx++) {
      const currentResult = result[idx];
      if (currentResult && currentResult.Event == targetEventId) {
        rowData.push(currentResult.Points);
        rowData.push(currentResult.Result);
        written = true;
        break;
      }
    }
    if (!written) {
      rowData.push('');
      rowData.push('');
    }
  }

  buildEventScoresRow(rowData: string[], events: SeriesEvent[], results: any) {
    for (let eventIndex = 0; eventIndex < events.length; eventIndex++) {
      this.makeEventCell(rowData, eventIndex, results);
    }
  }

  addHeaderRow(seriesSheet: Worksheet, events: SeriesEvent[]): void {
    const headerRow: string[] = [
      'LicenseNumber',
      'Category',
      'Psn',
      'Points',
      'Firstname',
      'Surname',
      'DOBHash',
      'Psn',
    ];

    for (let i = 0; i < events.length; i++) {
      const event: SeriesEvent = events[i];
      headerRow.push(event.name);
      headerRow.push('Evt' + (i + 1) + 'Psn');
    }
    seriesSheet.addRow(headerRow);
  }

  addCategoryEntrantsToSheet(seriesSheet: Worksheet, entrants: SeriesEntrant[], events: SeriesEvent[]): void {
    let categoryPosition = 1;
    const categoryName: string = entrants[0].Gender
      ? titleCase(`${entrants[0].Category} ${entrants[0].Gender}`.toLowerCase())
      : entrants[0].Category;

    for (let j = 0; j < entrants.length; j++) {
      const entrant: SeriesEntrant = entrants[j];
      const rowData: string[] = [
        '',
        categoryName,
        categoryPosition.toString(),
        entrant.Points ? entrant.Points.toString() : '0',
        entrant.Firstname ? entrant.Firstname : '',
        entrant.Surname ? entrant.Surname : '',
        '',
        categoryPosition.toString(),
      ];
      this.buildEventScoresRow(rowData, events, entrant.results);
      seriesSheet.addRow(rowData);
      categoryPosition++;
    }
  }

  makePointsSheet(workbook: Workbook, seriesResult: SeriesResult): ExcelJS.Worksheet {
    const seriesSheet: Worksheet = workbook.addWorksheet('Series', {
      views: [{ state: 'frozen', xSplit: 1, ySplit: 1 }],
    });

    this.addHeaderRow(seriesSheet, seriesResult.events);

    for (let c = 0; c < seriesResult.categoryEntrants.length;c++) {
      const categoryEntrants: SeriesEntrant[] = seriesResult.categoryEntrants[c];
      this.addCategoryEntrantsToSheet(seriesSheet, categoryEntrants, seriesResult.events);
    }

    return seriesSheet;
  }
}
