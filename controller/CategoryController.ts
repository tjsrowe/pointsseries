'use strict';

import { Category, CategoryKey } from '../src/model/category';
import { Config, ConfigSeries } from '../config/Config';
import { createCategory, createCategoryHashId, createNameWithGender } from '../src/utils/category';

import { AbstractDataController } from './AbstractDataController';
import { CategorySet } from '../src/utils/categorySet';
import assert from 'assert';
import defaultCategoryList from './data/defaultCategoryList.json';

export const createCategorySet = (list: (string | string[])[]): CategorySet => {
  const categoryList: Category[] = createCategorylist(list);
  const cs: CategorySet = new CategorySet(categoryList);
  list.forEach((names, index) => {
    cs.addAlias(names, index);
  });
  return cs;
};

export const createCategorylist = (list: (string | string[])[]): Category[] => {
  const output: Category[] = [];
  list.forEach((category: string | string[]) => {
    if (category != '') {
      const cat: Category = createCategory(category);
      output.push(cat);
    }
  });
  return output;
};

export class CategoryController extends AbstractDataController {
  createCategorySet(categories: Category[]): CategorySet {
    const cs: CategorySet = new CategorySet();

    cs.Categories = categories;
    return cs;
  }

  static categoryMatches(categoryList: CategorySet, catName1: string, catName2: string): boolean {
    if (catName1 == catName2) {
      return true;
    }
    const map: Map<string, number> = categoryList.Map;

    if (!map.has(catName1) || !map.has(catName2)) {
      return false;
    }
    return CategoryController.compareCategoryOrderNames(categoryList.Map, catName1, catName2) == 0;
  }

  static compareCategoryOrderNames(categoryMap: Map<string, number>, key1: string, key2: string): number {
    const order1: number | undefined = categoryMap.get(key1);
    assert(order1 != undefined, `Category ${key1} must have a known order to sort`);
    const order2: number | undefined = categoryMap.get(key2);

    assert(order2 != undefined, `Category ${key2} must have a known order to sort`);
    return compareByOrder(order1, order2);
  }

  static compareCategoryOrder(categoryMap: Map<string, number>, cat1: Category, cat2: Category): number {
    const order1: number | undefined = CategoryController.getCategoryMapOrder(categoryMap, cat1);
    const order2: number | undefined = CategoryController.getCategoryMapOrder(categoryMap, cat2);
    return compareByOrder(order1, order2);
  }

  static getCategoryMapOrder(categoryMap: Map<string, number>, cat: Category): number | undefined {
    // Return the first value we can find.
    const id: CategoryKey = createCategoryHashId(cat);
    const order: number | undefined = categoryMap.get(id);

    if (order != undefined) {
      return order;
    }

    if (cat.alternativeNames == undefined) {
      return undefined;
    }
    const firstExisting: string | undefined = cat.alternativeNames.find((name) => categoryMap.has(name) == true);

    if (firstExisting == undefined) {
      return undefined;
    }
    return categoryMap.get(firstExisting);
  }

  hasCustomCategoryList(struct: Config | ConfigSeries): boolean {
    if (struct && struct.categories) {
      return true;
    }
    return false;
  }

  getCustomCategoryList(struct: Config | ConfigSeries): string[] | undefined {
    if (struct && struct.categories) {
      return struct.categories;
    }
    return undefined;
  }

  getCategoryList(configSeries: ConfigSeries): Category[] {
    let categoryList: string[] | string[][] | undefined = undefined;

    if (this.hasCustomCategoryList(configSeries)) {
      categoryList = this.getCustomCategoryList(configSeries);
    } else if (this.hasCustomCategoryList(this.config)) {
      categoryList = this.getCustomCategoryList(this.config);
    } else {
      categoryList = defaultCategoryList;
    }
    if (categoryList == undefined) {
      return [];
    }
    const outputList: Category[] = createCategorylist(categoryList);

    return outputList;
  }
}

export function createIndexMapFromList(list: Category[]): Map<string, number> {
  const map: Map<string, number> = new Map();
  list.forEach((cat: Category, index: number) => {
    const key: string = createCategoryHashId(cat);
    map.set(key, index);
    if (cat.alternativeNames) {
      cat.alternativeNames.forEach((altName) => {
        const altKey: string = createNameWithGender(altName, cat.gender);

        map.set(altKey, index);
      });
    }
  });
  return map;
}

export const compareByOrder = (n1: number | undefined, n2: number | undefined): number => {
  if (n1 == undefined) {
    if (n2 == undefined) {
      return 0;
    }
    return n2;
  } else if (n2 == undefined) {
    return -n1;
  } else {
    return n1 - n2;
  }
};
