'use strict';

import * as dotenv from 'dotenv';

import { DDB } from './DDB';
import { EventDataDetail } from '../../src/model';

dotenv.config();

const EVENT_TABLE_NAME = process.env.EVENT_TABLE_NAME || 'pointsseries_dev_event';

export class EventDDB extends DDB<EventDataDetail, string> {
  constructor(tableName = EVENT_TABLE_NAME) {
    super(tableName);
  }

  getItemId(item: EventDataDetail): string {
    return item.id!;
  }
}
