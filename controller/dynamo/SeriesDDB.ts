'use strict';

import * as dotenv from 'dotenv';

import { DDB } from './DDB';
import { SeriesDetail } from '../../src/model';

dotenv.config();

const SERIES_TABLE_NAME = process.env.SERIES_TABLE_NAME || 'pointsseries_dev_series';

export class SeriesDDB extends DDB<SeriesDetail, number> {
  constructor(tableName = SERIES_TABLE_NAME) {
    super(tableName);
  }

  getItemId(item: SeriesDetail): number {
    return item.id;
  }
}
