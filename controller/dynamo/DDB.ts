'use strict';

import {
  DynamoDB,
  DynamoDBClientConfig,
  GetItemCommandInput,
  GetItemCommandOutput,
  PutItemCommandInput,
  QueryCommandInput,
  QueryCommandOutput,
} from '@aws-sdk/client-dynamodb';
import { marshall, marshallOptions, unmarshall } from '@aws-sdk/util-dynamodb';

import DataSource from '../datasource/DataSource';
import throttledQueue from 'throttled-queue';

export type { DynamoDBClientConfig };

export abstract class DDB<T, idtype> implements DataSource<T, idtype> {
  private _tableName: string;
  private _ddb!: DynamoDB;
  private _readThrottle: <Return = unknown>(fn: () => Return | Promise<Return>) => Promise<Return>;
  private _writeThrottle: <Return = unknown>(fn: () => Return | Promise<Return>) => Promise<Return>;
  private _writeNumber = 1;

  public get TableName() {
    return this._tableName;
  }

  protected constructor(tableName: string) {
    this._tableName = tableName;
    this._readThrottle = throttledQueue(15, 5000);
    this._writeThrottle = throttledQueue(5, 5000);
  }

  connect(credentials: DynamoDBClientConfig) {
    this._ddb = new DynamoDB(credentials);
  }

  abstract getItemId(item: T): idtype;
  private getCommandSize(command: any): number {
    if (!command) {
      console.debug('Asked to get length of undefined command result');
      return 0;
    }
    const numKb = (JSON.stringify(command).length + 1000) / 1000;
    return numKb;
  }

  get(id: idtype): Promise<T> {
    const params: GetItemCommandInput = {
      Key: marshall({
        id: id,
      }),
      TableName: this._tableName,
    };
    const pr: Promise<T> = new Promise((resolve, reject) => {
      this._readThrottle(() => {
        this._ddb
          .getItem(params)
          .then((value: GetItemCommandOutput) => {
            const itemAny: any = unmarshall(value.Item || {});
            const item: T = {
              ...itemAny,
            };
            const commandSize: number = Math.floor(this.getCommandSize(value));
            for (let i = 1; i < commandSize; i++) {
              this._readThrottle(() => ({}));
            }
            resolve(item);
          })
          .catch((err) => {
            reject(err);
          });
      });
    });
    return pr;
  }

  getAll():Promise<T[]> {
    const results:Promise<T[]> = new Promise((resolve, reject) => {
      const qci:QueryCommandInput = {
        TableName: this._tableName,
      };
      const items:T[] = [];
      let commandOutputSize = 0;
      try {
        console.log(`Scanning ${this._tableName}`);
        this._ddb.scan(qci).then((output:QueryCommandOutput) => {
          commandOutputSize = Math.floor(this.getCommandSize(output));
          console.debug(`Got command output of size ${commandOutputSize}`);
          output.Items?.forEach((item) => {
            if (item) {
              const itemAny:any = unmarshall(item || {});
              items.push({
                ...itemAny
              });
            }
          });
          for (let i = 1; i < commandOutputSize; i++) {
            this._readThrottle(() => ({}));
          }

          resolve(items);
        });
      } catch (err) {
        console.debug(`Got exception while scanning table ${this._tableName}`);
        reject(err);
      }
    });
    return results;
  }

  save(item: T): Promise<idtype> {
    const options: marshallOptions = {
      removeUndefinedValues: true,
    };
    const params: PutItemCommandInput = {
      Item: marshall(item, options),
      TableName: this._tableName,
    };

    const commandSize: number = Math.floor(this.getCommandSize(params));

    const itemId = this.getItemId(item);
    const pr: Promise<idtype> = new Promise((resolve, reject) => {
      const writeNumber: number = this._writeNumber++;
      // console.log(`Queued ${commandSize}KB table ${this._tableName} write ${writeNumber} on item ${itemId}`);
      this._writeThrottle(() => {
        this._ddb
          .putItem(params)
          .then(() => {
            console.debug(`Finished write ${writeNumber} on table ${this._tableName} for item ${itemId}`);
            resolve(this.getItemId(item));
          })
          .catch((err) => {
            reject(err);
          });
      });
      for (let i = 1; i < commandSize; i++) {
        this._writeThrottle(() => ({}));
      }
    });
    return pr;
  }
}
