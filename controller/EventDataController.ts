'use strict';

import { CacheableDataFileLoad, DataFileController } from './DataFileController';
import { EventDataDetail, EventInfo, SeriesEventFile } from '../src/model/event';
import { EventDataFileController, ResultFileVerificationResult } from './EventDataFileController';

import { AbstractDataController } from './AbstractDataController';
import { DataFileValidationController } from './DataFileValidationController';
import DataSource from './datasource/DataSource';
import { DateRange } from '../types';
import { EventEntrant } from '../src/model/entrant';
import excelDateToISODate from '../src/excelDateToIsoDate';
import { getYearFromDateString } from '../src/dateUtils';
import { isInDateRange } from '../src/isInDateRange';
import { isRootPath } from '../src/isRootPath';
import path from 'path';

const LOG_EVENT_CACHE_HITS = false;

export interface EventDataError {
  message: string;
  lineNumber?: number;
}

export class EventDataController extends AbstractDataController {
  private _dataFileController!: DataFileController;
  private _dataFileValidationController!: DataFileValidationController;
  private _eventDataFileController!: EventDataFileController;
  private _eventDataSource!:DataSource<EventDataDetail, string>;

  public get DataFileController(): DataFileController {
    return this._dataFileController;
  }

  public set DataFileController(value: DataFileController) {
    this._dataFileController = value;
  }

  public get DataFileValidationController(): DataFileValidationController {
    return this._dataFileValidationController;
  }

  public set DataFileValidationController(value: DataFileValidationController) {
    this._dataFileValidationController = value;
  }

  public set EventDataFileController(edc: EventDataFileController) {
    this._eventDataFileController = edc;
  }

  public set DataSource(ds:DataSource<EventDataDetail, string>) {
    this._eventDataSource = ds;
  }

  static getYearFromEventDate(event: EventDataDetail): number | undefined {
    if (!event.date) {
      return undefined;
    }

    const eventYear: number = getYearFromDateString(event.date);
    return eventYear;
  }

  hasLicenseColumn(eventDataSet: any): boolean {
    if (!eventDataSet.Columns) {
      throw Error(
        'eventDataSet had no Columns set - this should not ' + 'occur and these should be set prior to getting here.'
      );
    }
    const licenseFields: string[] = this.configManager.getLicenseFields();
    for (let i = 0; i < licenseFields.length; i++) {
      if (includesInsensitive(eventDataSet.Columns, licenseFields[i])) {
        return true;
      }
    }
    return false;
  }

  parseEventResult(event: EventDataDetail): EventDataDetail[] {
    if (event == undefined) {
      throw Error('Event passed in to parseEventResult was undefined');
    }
    if (!event.eventRawData.Columns) {
      throw Error(`eventDataSet had no Columns set on ${event.name} this \
should not occur and these should be set prior to getting here.`);
    }
    const resultRows: any[] = [];
    const errors:EventDataError[] = [];
    const eventArr: any = event.eventRawData;

    const rvir: ResultFileVerificationResult = this._dataFileValidationController.validateDataColumns(eventArr);
    if (!rvir.valid) {
      const sourceDataFile: string = event.isFinal ? event.cacheFileName : event.resultFileName;
      const messages: string[] = rvir.issues.map((vi) =>
        vi.lineNumber !== 0 ? `${vi.lineNumber}: ${vi.message}` : vi.message);
      const message: string = messages.join(', ') + ' (' + sourceDataFile + ')';
      throw new Error(message);
    }

    for (let i = 0; i < eventArr.length; i++) {
      const eventDataRow = eventArr[i];
      if (isErrorData(eventDataRow)) {
        const err:EventDataError = {
          lineNumber: i,
          message: eventDataRow.message,
        };
        errors.push(err);
      } else if (errors.length == 0) {
        try {
          const hasLicense: boolean = this.hasLicenseColumn(eventArr);
          const resultRow: EventEntrant | undefined = this._eventDataFileController.parseRiderDataFileRow(
            eventDataRow,
            event,
            !hasLicense
          );
          if (resultRow != undefined) {
            resultRows.push(resultRow);
          }
        } catch (error:any) {
          const err:EventDataError = {
            lineNumber: i,
            message: error.message,
          };

          console.warn(`Got errors while parsing data for ${event.name}: ${error}`);
          errors.push(err);
        }
      }
    }
    if (errors.length == 0) {
      return resultRows;
    } else {
      const errStr: string = errors.join();
      console.warn(`Got errors while parsing data for ${event.name}: ${errStr}`);
      throw new Error(errStr);
    }
  }

  loadEventLogLine(data: CacheableDataFileLoad): string {
    const line: string =
      'Loaded from ' + data.isMemoryCache ? 'in-memory cache' : data.isDiskCache ? 'disk cache' : 'data file';
    return line;
  }

  async requestEvent(seriesEvent: SeriesEventFile, baseDirectory: string): Promise<EventDataDetail> {
    const dfc: DataFileController = this._dataFileController;
    const pr: Promise<EventDataDetail> = new Promise<EventDataDetail>(async (resolve, reject) => {
      const dataFile: string = isRootPath(seriesEvent.resultFileName) ?
        seriesEvent.resultFileName :
        path.join(baseDirectory, seriesEvent.resultFileName);
      const skipCache = !seriesEvent.isFinal;

      try {
        const cacheDataLoaded: CacheableDataFileLoad = await dfc.loadCachedDataFile(dataFile, skipCache);
        const event: EventDataDetail = {
          dataFileLoadTime: cacheDataLoaded.dataLoadTime,
          dataFileTime: cacheDataLoaded.dataModificationTime,
          eventRawData: cacheDataLoaded.dataFileJsonData,
          ...seriesEvent,
        };
        if (!cacheDataLoaded.dataFileJsonData.Columns) {
          throw Error('Loaded data on ' + dataFile + ' has no Columns set');
        }
        // event.eventRawData = cacheDataLoaded.dataFileJsonData;
        if (LOG_EVENT_CACHE_HITS) {
          let isCacheSkipped = '';
          if (skipCache) {
            isCacheSkipped = ' - cache skipped because result is not final.';
          }
          console.log(`${dataFile}: ${this.loadEventLogLine(cacheDataLoaded)}${isCacheSkipped}`);
        }
        resolve(event);
      } catch (err) {
        reject(err);
      }
    });
    return pr;
  }

  requestEvents(seriesEventIds:string[]):Promise<Map<string, EventDataDetail>> {
    if (!seriesEventIds) {
      throw Error('array of event IDs must be specified in requestEvents');
    }
    if (!this._eventDataSource) {
      throw Error('EventDataSource is not set in requestEvents');
    }
    const resultMapPromise:Promise<Map<string, EventDataDetail>> = new Promise((resolve, reject) => {
      const promiseList:Promise<void>[] = [];
      const results:Map<string, EventDataDetail> = new Map<string, EventDataDetail>();

      const assignedEvents:EventDataDetail[] = [];
      seriesEventIds.forEach((id:string) => {
        const pr:Promise<void> = this.retrieveEventAndSetEventId(id, results, assignedEvents);
        promiseList.push(pr);
      });

      Promise.all(promiseList).then(() => {
        resolve(results);
      }).catch((err) => {
        reject(err);
      });
    });

    return resultMapPromise;
  }

  generateOrGetEventId(eventIds:EventDataDetail[], edd:EventDataDetail):number {
    if (edd.eventId != undefined) {
      const found:EventDataDetail|undefined = eventIds.find((e) => e.eventId == edd.eventId);
      if (found) {
        return edd.eventId;
      }
    }
    let i = 0;
    while (eventIds[i] != undefined) {
      i++;
    }
    return i;
  }

  retrieveEventAndSetEventId(id: string, results: Map<string, EventDataDetail>,
    eventIds:EventDataDetail[]): Promise<void> {
    return this._eventDataSource.get(id).then((edd:EventDataDetail) => {
      const eventId:number = this.generateOrGetEventId(eventIds, edd);
      results.set(id, edd);
      eventIds[eventId] = edd;
      edd.eventId = eventId;
      return Promise.resolve();
    });
  }

  parseAllEventResults(eventObject: EventDataDetail[]): EventDataDetail[] {
    const errors = [];
    for (let i = 0; i < eventObject.length; i++) {
      const currentObject: EventDataDetail = eventObject[i];
      if (!currentObject.errorList || currentObject.errorList?.length == 0) {
        try {
          const eventResult: EventDataDetail[] = this.parseEventResult(currentObject);
          if (eventResult.length === 0) {
            throw new Error(`Event result for ${currentObject.resultFileName} was empty.`);
          }
          currentObject.eventRawData = eventResult;
        } catch (error:any) {
          currentObject.errorList = error;
          errors.push(currentObject);
        }
      } else {
        errors.push(currentObject);
      }
    }
    if (errors.length == 0) {
      return eventObject;
    } else {
      throw errors;
    }
  }

  isEventInDateRange(event: EventDataDetail, dateRange: DateRange): boolean {
    if (!event.date) {
      return false;
    }
    return isInDateRange(event.date, dateRange);
  }

  getEventDate(event: EventInfo): string | undefined {
    if (!event.date) {
      return undefined;
    }
    let eventDate: string = event.date;
    if (typeof eventDate === 'number') {
      eventDate = excelDateToISODate(eventDate);
    }
    return eventDate;
  }
}

function isErrorData(eventData: any): boolean {
  if (eventData.error) {
    return true;
  }
  return false;
}

function includesInsensitive(array: string[], searchValue: string): boolean {
  if (!array) {
    console.error('Passed in null array to be searched for ' + searchValue);
  }
  const searchValueUpper = searchValue.toUpperCase();
  for (let i = 0; i < array.length; i++) {
    const arrUpper = array[i].toUpperCase();
    if (arrUpper == searchValueUpper) {
      return true;
    }
  }
  return false;
}
