'use strict';

import { ExcelDataFileController, ExcelFileOptions } from './ExcelDataFileController';
import { currentTimeMs, finishTimer } from '../src/dateUtils';

import { AbstractDataController } from './AbstractDataController';
import { CSVDataFileController } from './CSVDataFileController';
import { DataFileCacheController } from './DataFileCacheController';
import { ExcelFileUtils } from '../src/ExcelFileUtils';
import { JsonDataFileController } from './JsonDataFileController';
import { JsonUtils } from '../src/jsonUtils';
import { UploadedFile } from 'express-fileupload';
import fs from 'fs';
import path from 'path';
import sha256 from 'crypto-js/sha256';

export type CacheableDataFileLoad = {
  dataFileJsonData: any;
  dataLoadTime: Date;
  dataModificationTime: Date;
  isMemoryCache: boolean;
  isDiskCache: boolean;
  isDiskLoad: boolean;
  retrieveTime: number | undefined;
};

export class DataFileController extends AbstractDataController {
  private _csvDataFileController?: CSVDataFileController | undefined;
  private _dataFileCacheController?: DataFileCacheController | undefined;
  private _excelDataFileController?: ExcelDataFileController | undefined;
  private _jsonDataFileController?: JsonDataFileController | undefined;
  public get dataFileCacheController(): DataFileCacheController | undefined {
    return this._dataFileCacheController;
  }
  public set dataFileCacheController(value: DataFileCacheController | undefined) {
    this._dataFileCacheController = value;
  }
  public get excelDataFileController(): ExcelDataFileController | undefined {
    return this._excelDataFileController;
  }
  public set excelDataFileController(value: ExcelDataFileController | undefined) {
    this._excelDataFileController = value;
  }
  public get csvDataFileController(): CSVDataFileController | undefined {
    return this._csvDataFileController;
  }
  public set csvDataFileController(value: CSVDataFileController | undefined) {
    this._csvDataFileController = value;
  }
  public get jsonDataFileController(): JsonDataFileController | undefined {
    return this._jsonDataFileController;
  }
  public set jsonDataFileController(value: JsonDataFileController | undefined) {
    this._jsonDataFileController = value;
  }
  constructor() {
    super();
  }

  processXlsFile(filePath: string, options?: undefined | ExcelFileOptions): Promise<any> {
    const prXlsFile: Promise<any> = new Promise<any>((resolve, reject) => {
      if (options && options.Worksheet) {
        this.excelDataFileController!.xlsxAsJson(filePath, options)
          .then((data) => {
            resolve(data);
          })
          .catch((error) => {
            reject(error);
          });
      } else {
        let path = undefined;

        if (ExcelFileUtils.excelFilenameHasSheet(filePath)) {
          const xlsxPath = ExcelFileUtils.getExcelFilenameForPath(filePath);
          const sheet = ExcelFileUtils.getExcelSheetFromPath(filePath);
          if (!options) {
            options = {
              Worksheet: sheet,
            };
          } else if (options && !options.Worksheet) {
            options.Worksheet = sheet;
          }
          path = xlsxPath;
        } else {
          path = filePath;
        }
        const excelPromise = this.excelDataFileController!.xlsxAsJson(path, options);
        excelPromise
          .then(
            (jsonData: any) => {
              resolve(jsonData);
            },
            (err: Error) => {
              reject(err);
            }
          )
          .catch((err: Error) => {
            reject(err);
          });
      }
    });
    return prXlsFile;
  }

  dataFileAsJson(filePath: string, options?: undefined | ExcelFileOptions): Promise<JSON[]> {
    const prJson: Promise<JSON[]> = new Promise<JSON[]>((resolve, reject) => {
      if (ExcelFileUtils.isXlsFile(filePath)) {
        this.processXlsFile(filePath, options)
          .then((data) => resolve(data))
          .catch((error) => {
            reject(error);
          });
      } else {
        this.csvDataFileController!.csvAsJson(filePath, resolve, reject);
      }
    });
    return prJson;
  }

  // dataFileAsJsonSync(filePath:string, onSuccess:Function,
  // onFailure:Function, options?:undefined|ExcelFileOptions):void {
  //   if (ExcelFileUtils.isXlsFile(filePath)) {
  //     this.processXlsFile(filePath, options).then((data) => { onSuccess(data) }).catch((error) => {
  //       onFailure(error);
  //     });
  //   } else {
  //         this.csvDataFileController!.csvAsJsonSync(filePath, onSuccess, onFailure);
  //   }
  // }

  getExistingFile(baseDirectory: string, targetFile: string): string {
    let actualFile: string | undefined;
    let sheet = null;
    if (ExcelFileUtils.isXlsFile(targetFile)) {
      actualFile = ExcelFileUtils.getExcelFilenameForPath(targetFile);
      sheet = ExcelFileUtils.getExcelSheetFromPath(targetFile);
      const exists = fs.existsSync(actualFile);
      if (exists && sheet) {
        return targetFile + '! ' + sheet;
      } else if (exists) {
        return targetFile;
      }
    } else if (fs.existsSync(targetFile)) {
      return targetFile;
    }

    if (!baseDirectory.endsWith('/')) {
      actualFile = baseDirectory + '/';
    } else {
      actualFile = baseDirectory;
    }
    let returnValue;
    if (ExcelFileUtils.isXlsFile(targetFile)) {
      if (!sheet) {
        sheet = ExcelFileUtils.getExcelSheetFromPath(targetFile);
      }
      actualFile = actualFile + ExcelFileUtils.getExcelFilenameForPath(targetFile);
      if (sheet) {
        returnValue = actualFile + '!' + sheet;
      } else {
        returnValue = actualFile;
      }
    } else {
      actualFile = actualFile + targetFile;
      returnValue = actualFile;
    }
    if (fs.existsSync(actualFile)) {
      return returnValue;
    }
    return targetFile;
  }

  fileExistsOnFs(resultFilePath: string): boolean {
    let filename = undefined;
    if (ExcelFileUtils.isXlsFile(resultFilePath)) {
      filename = ExcelFileUtils.getExcelFilenameForPath(resultFilePath);
    } else {
      filename = resultFilePath;
    }

    return fs.existsSync(filename);
  }

  loadCachedDataFile(
    resultFilePath: string,
    skipCache: boolean,
    options?: ExcelFileOptions
  ): Promise<CacheableDataFileLoad> {
    const prData: Promise<CacheableDataFileLoad> = new Promise<CacheableDataFileLoad>((resolve, reject) => {
      // let cachedData:any|undefined = undefined;

      const loadDetail: CacheableDataFileLoad = {
        dataFileJsonData: undefined,
        dataLoadTime: new Date(),
        dataModificationTime: new Date(),
        isDiskCache: false,
        isDiskLoad: true,
        isMemoryCache: false,
        retrieveTime: undefined,
      };

      const startTime = currentTimeMs();
      if (!skipCache) {
        const fileExists: boolean = this.fileExistsOnFs(resultFilePath);
        loadDetail.dataFileJsonData = this.dataFileCacheController!.retrieveFromCache(resultFilePath);
        if (!fileExists && loadDetail.dataFileJsonData) {
          loadDetail.isMemoryCache = false;
          this.dataFileCacheController!.removeFromCache(resultFilePath);
        } else if (loadDetail.dataFileJsonData) {
          loadDetail.isMemoryCache = true;
          loadDetail.isDiskLoad = false;
          if (!loadDetail.dataFileJsonData.Columns) {
            loadDetail.isMemoryCache = false;
            loadDetail.isDiskCache = false;
            loadDetail.isDiskLoad = true;
          } else {
            const isUpdated: boolean = this.dataFileCacheController!.isNewerThanCache(resultFilePath);
            if (isUpdated) {
              loadDetail.isDiskLoad = true;
              loadDetail.isDiskCache = false;
            } else {
              loadDetail.isDiskLoad = false;
            }
          }
        }
      }

      if (loadDetail.isDiskLoad) {
        if (!this.dataFileCacheController) {
          throw new Error('DataFileCacheController does not exist on DataFileController so cannot load cached data.');
        }
        const dfcc: DataFileCacheController = this.dataFileCacheController!;
        this.loadFile(resultFilePath, options)
          .then((data: any) => {
            loadDetail.dataLoadTime = new Date();
            loadDetail.dataModificationTime = this.dataFileCacheController!.safeFileTime(resultFilePath);
            loadDetail.dataFileJsonData = data;
            data.Columns = JsonUtils.getColumnsUsed(data);
            if (!skipCache) {
              dfcc.cacheFile(resultFilePath, data);
            }
            loadDetail.retrieveTime = finishTimer(startTime);
            resolve(loadDetail);
          })
          .catch((err: Error) => {
            console.trace(err);
            reject(
              new Error(`Failed loading file ${resultFilePath} while loading \
directly from file: ${err.message}.  ${JSON.stringify(err)}`)
            );
          });
      } else if (loadDetail.dataFileJsonData) {
        loadDetail.retrieveTime = finishTimer(startTime);
        resolve(loadDetail);
      } else {
        reject(new Error(`No cache hit and no file for ${resultFilePath}`));
      }
    });
    return prData;
  }

  loadFile(filePath: string, options?: ExcelFileOptions): Promise<any> {
    if (!filePath) {
      Promise.reject(new Error('Empty file path passed to load file.'));
    }
    const pr: Promise<any> = new Promise((resolve, reject) => {
      if (ExcelFileUtils.isXlsFile(filePath)) {
        return this.excelDataFileController!.xlsxAsJson(filePath, options)
          .then((jsonData) => {
            resolve(jsonData);
          })
          .catch((error) => {
            reject(error);
          });
      } else if (this._csvDataFileController && this.csvDataFileController!.isCsvFile(filePath)) {
        this.csvDataFileController!.csvAsJson(filePath, resolve, reject);
      } else if (JsonUtils.isJsonFile(filePath)) {
        this.jsonDataFileController!.loadJsonFile(filePath)
          .then((data) => resolve(data))
          .catch((err) => reject(err));
      } else {
        // how the hell do we deal with an unknown file type?
        reject(
          new Error(`Unknown file type for file ${filePath} - check \
           the file extension, must be .xlsx, .xls, .json or .csv`)
        );
      }
    });
    return pr;
  }

  fileOrCacheFileExists(filePath: string): boolean {
    if (!filePath) {
      return false;
    }
    if (fs.existsSync(filePath)) {
      return true;
    }
    if (this._dataFileCacheController && this._dataFileCacheController!.isCached(filePath)) {
      return true;
    }
    return false;
  }

  getFilenameHashForPath(filePath: string): string {
    const cachePath: string = this.dataFileCacheController!.getCachePathForFile(filePath);
    const pathSha: string = sha256(cachePath).toString();
    return pathSha;
  }

  getPathFromFilename(filename: string): string {
    const regex = /\\/g;
    const workingPath: string = filename.replaceAll(regex, '/');
    const path: string = workingPath.substring(0, workingPath.lastIndexOf('/') + 1);
    return path;
  }

  getFileDirectory(fileObject: string): string {
    let path: string;
    if (ExcelFileUtils.isXlsFile(fileObject)) {
      path = ExcelFileUtils.getExcelFilenameForPath(fileObject);
      path = this.getPathFromFilename(path);
    } else {
      path = this.getPathFromFilename(fileObject);
    }
    return path;
  }

  saveTempFromUpload(file: UploadedFile): Promise<string> {
    const tempUploadFilePath: string = path.join(this.configManager.getTempUploadFilePath(), file.md5 + '.xlsx');

    const prUploadPath: Promise<string> = new Promise((resolve, reject) => {
      file.mv(tempUploadFilePath, function (err: any) {
        if (err) {
          reject(err);
          console.log(`Failed moving temp file to ${tempUploadFilePath}: ` + err);
        }
        resolve(tempUploadFilePath);
      });
    });
    return prUploadPath;
  }
}
