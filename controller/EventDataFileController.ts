import { AbstractEntrant, EventDataDetail, EventEntrant, License, SeriesEventFile } from '../src/model';
import { DOB_HASH_LENGTH, LICENSE_HASH_LENGTH, LOG_OUTPUT_LICENSE_ERROR } from '../env';

import { AbstractDataController } from './AbstractDataController';
import { ChampionshipEventDetails } from '../src/model/event';
import { DataValidator } from '../src/dataValidator';
import { EntrantDataController } from './EntrantDataController';
import { ResultDataAliases } from '../config/Config';
import excelDateToISODate from '../src/excelDateToIsoDate';
import { generateUuidFromString } from '../src/uuid';
import getDateAsISO from '../src/getDateAsIso';
import { getYearFromDateString } from '../src/dateUtils';
import sha256 from 'crypto-js/sha256';

export enum ErrorLevel {
  ERROR,
  WARNING,
  INFO,
}


export enum ValidationErrorType {
  MISSING_FIELD,
  MISSING_DATA
}

export type ValidationInfo = {
  errorLevel: ErrorLevel;
  lineNumber: number;
  message: string;
  errorType: ValidationErrorType;
};

export type ResultFileVerificationResult = {
  issues: ValidationInfo[];
  valid: boolean;
};

type SeriesResultFileList = {
  Id?:string,
  EventName:string,
  ResultFile:string,
  CacheFile:string,
}

export class EventDataFileController extends AbstractDataController {
  private convertSheetRowToEvent(json: any): Promise<SeriesEventFile> {
    let eventDate: string = json.EventDate;
    if (typeof eventDate === 'number') {
      eventDate = excelDateToISODate(eventDate);
    }

    const prEventDetail:Promise<SeriesEventFile> = new Promise((resolve, reject) => {
      generateEventId(json).then((id) => {
        const eventDetail: SeriesEventFile = {
          cacheFileName: json.CacheFile,
          championshipStatus: this.getEventChampionshipStatus(json),
          date: eventDate,
          format: json.Format,
          id: id,
          isFinal: json.IsFinal,
          location: json.Location,
          name: json.EventName,
          organiser: json.Organiser,
          pointsFactor: json.PointsFactor,
          pointsSystem: json.PointsSystem,
          resultFileName: json.ResultFile,
          state: json.State,
          subtitle: json.Subtitle,
        };
        resolve(eventDetail);
      }).catch((err) => {
        reject(new Error(`Failed loding eventId ${json.EventName}: ${err}`));
      });
    });
    return prEventDetail;
  }

  public convertEventJsonToEventMeta(eventsJson: JSON[] | any[]): Promise<SeriesEventFile[]> {
    const promises:Promise<SeriesEventFile>[] = [];
    eventsJson.forEach((json: JSON | any) => {
      promises.push(this.convertSheetRowToEvent(json));
    });
    return Promise.all(promises);
  }

  getEventChampionshipStatus(eventJson: any): ChampionshipEventDetails | undefined {
    const region: string = eventJson.ChampionshipRegion;
    if (region == undefined) {
      return undefined;
    }
    const format: string = eventJson.Format;
    let eventDate: string = eventJson.EventDate;
    if (typeof eventDate === 'number') {
      eventDate = excelDateToISODate(eventDate);
    }
    const year: number = getYearFromDateString(eventDate);
    if (year == undefined || year < 1900 || year > 2050) {
      return undefined;
    }
    const status: ChampionshipEventDetails = {
      format: format,
      region: region,
      year: year,
    };
    return status;
  }

  parseRiderDataFileRow(row: any, event: EventDataDetail, ignoreLicenseCheck: boolean): EventEntrant | undefined {
    const entrant: EventEntrant | any = {};
    const fieldSearchSet: ResultDataAliases = this.configManager.getResultDataAliases();
    matchRowFields(entrant, row, fieldSearchSet);
    // By here we must have a valid name, category and result.

    if (!EntrantDataController.hasNameFields(entrant) || !entrant.Category || !entrant.Result) {
      return undefined;
    }

    if (!ignoreLicenseCheck) {
      try {
        const licenseFields: string[] = this.configManager.getLicenseFields();
        const licenseTypes: string[] = this.configManager.getLicenseTypes();
        getLicenseFromFileRow(entrant, row, licenseFields, licenseTypes);
      } catch (err) {
        let errMsg = err;
        if (event.name) {
          errMsg = errMsg + ' in event ' + event.name;
        }
        if (LOG_OUTPUT_LICENSE_ERROR) {
          console.log(errMsg);
        }
      }
    }

    this.getDobFromFileRow(entrant, row);
    this.setIneligibleIfSet(entrant, row);
    return entrant;
  }

  setIneligibleIfSet(entrant: EventEntrant, row: any): void {
    if (row.IsIneligible) {
      const fieldStr: string = row.IsIneligible.toString().toLowerCase();
      if (fieldStr == 'y' || fieldStr == 'true' || fieldStr == 'yes') {
        entrant.IsIneligible = true;
      }
    }
  }

  getDobFromFileRow(entrant: EventEntrant, row: any): void {
    const dobFields = this.configManager.getDobFields();

    const dobValue = findValueFromVariants(row, dobFields);
    if (dobValue) {
      entrant.DOB_Hash = dobToHash(dobValue);
    }
  }
}

export function findCaseInsensitiveObjectProperty(target: any, searchKey: string): any {
  const searchKeyLowerCase = searchKey.toLowerCase();
  let result: string|null = null;
  for (const [keyField, keyValue] of Object.entries(target)) {
    const lowerKeyField = keyField.toLowerCase();

    if (lowerKeyField === searchKeyLowerCase) {
      result = (keyValue as unknown as string).toString();
      break;
    }
  }
  return result;
}

export function findFirstFieldFromVariants(target: any, fieldNameOptions: any): any {
  for (let i = 0; i < fieldNameOptions.length; i++) {
    const checkField = fieldNameOptions[i];
    if (target[checkField]) {
      return checkField;
    }
  }
  return null;
}

function findValueFromVariants(target: any, fieldNameOptions: string[]): any {
  for (let i = 0; i < fieldNameOptions.length; i++) {
    const checkField = fieldNameOptions[i];
    const checkFieldValue: any = findCaseInsensitiveObjectProperty(target, checkField);
    if (checkFieldValue !== null && checkFieldValue !== undefined) {
      return checkFieldValue;
    }
  }
  return null;
}

function dobToHash(dob: any): string {
  const isoDate: string = getDateAsISO(dob);
  const shaDate: string = sha256(isoDate).toString();
  const subSha: string = shaDate.substring(0, DOB_HASH_LENGTH);
  return subSha;
}

function matchRowFields(entrant: any, row: any, fieldSearchSet: ResultDataAliases): void {
  for (const [keyField, keyAlternatives] of Object.entries(fieldSearchSet)) {
    const basicnameFieldValue = findCaseInsensitiveObjectProperty(row, keyField);
    if (basicnameFieldValue) {
      entrant[keyField] = basicnameFieldValue;
    } else {
      if (Array.isArray(keyAlternatives)) {
        const matchedValue = findValueFromVariants(row, keyAlternatives);
        if (matchedValue) {
          entrant[keyField] = matchedValue;
        }
      } else {
        const alt: any = keyAlternatives;
        const checkFieldValue = findCaseInsensitiveObjectProperty(row, alt);
        if (checkFieldValue) {
          entrant[keyField] = checkFieldValue;
        }
      }
    }
  }
}

export function getLicenseFromFileRow(
  entrant: AbstractEntrant,
  row: any,
  licenseFields: string[],
  licenseTypes: string[]
): void {
  const licenseFieldToUse = findFirstFieldFromVariants(row, licenseFields);

  if (licenseFieldToUse) {
    const license: License = processLicenseField(row[licenseFieldToUse], licenseTypes);
    entrant.LicenseHash = license;
  } else if (!entrant.LicenseHash) {
    const err: any = Error('No license data for ' + entrant.Firstname + ' ' + entrant.Surname);
    err.MissingField = 'LicenseHash';
    throw err;
  }
}

export function processLicenseField(field: any, licenseTypes: string[]): License {
  let license: License = {};
  // var licenseTypes = this._configManager.getLicenseTypes();

  if (field && (DataValidator.isInteger(field) || DataValidator.isInt(field) || DataValidator.isNumber(field))) {
    const shaIntLicense: string = sha256(field.toString()).toString();
    const subIntSha: string = shaIntLicense.substring(0, LICENSE_HASH_LENGTH);
    license = {
      LicenseNumber: subIntSha,
      LicenseType: licenseTypes[0],
    };
  } else if (field) {
    const numberPortion: string = field.replace(/^\D+/g, ''); // replace all leading non-digits with nothing
    const shaStrLicense: string = sha256(numberPortion).toString();
    const subStrSha: string = shaStrLicense.substring(0, LICENSE_HASH_LENGTH);
    license.LicenseNumber = subStrSha;

    for (let i = 0; i < licenseTypes.length; i++) {
      const currentLicenseType = licenseTypes[i];
      const firstLetter = currentLicenseType.substring(0, 1);
      if (field.startsWith(firstLetter)) {
        license.LicenseType = currentLicenseType;
      }
    }
    if (!license.LicenseType) {
      license.LicenseType = licenseTypes[0];
    }
  }
  return license;
}

export function generateEventId(event: SeriesResultFileList): Promise<string> {
  if (event.Id) {
    return Promise.resolve(event.Id);
  } else if (event.CacheFile) {
    return Promise.resolve(event.CacheFile);
  } else if (!event.ResultFile) {
    return Promise.reject(
      new Error(`Couldn't generate eventId for ${event.EventName} because resultFileName was undefined`));
  } else {
    return generateUuidFromString(event.ResultFile);
  }
}
