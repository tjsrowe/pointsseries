'use strict';

import { ConfigManager, seriesIdentifier } from './ConfigManager';
import { EventDataDetail, SeriesDataDetail } from '../src/model';
import { SeriesDataController, SeriesLoadResult } from './SeriesDataController';

import { ALL_DATES } from '../types';
import { AbstractDataController } from './AbstractDataController';
import { ConfigSeries } from '../config/Config';
import { ExcelSeriesController } from './ExcelSeriesController';

interface Exporter {
  checkConnection(): void;
  commit():Promise<void>;
}

export interface EventExporter extends Exporter {
  saveEvent(event: EventDataDetail): Promise<string>;
}

export interface SeriesExporter extends EventExporter {
  saveSeries(series: SeriesDataDetail): Promise<number>;
  saveSeriesList(list: SeriesDataDetail[]): Promise<void>;
}

class DataExportController extends AbstractDataController {
  _seriesDataController!: SeriesDataController;
  _excelSeriesController!: ExcelSeriesController;

  set SeriesDataController(sdc: SeriesDataController) {
    this._seriesDataController = sdc;
  }

  set ExcelSeriesController(xsc: ExcelSeriesController) {
    this._excelSeriesController = xsc;
  }

  exportAllEvents(exporter: SeriesExporter): Promise<void> {
    const manager: ConfigManager = this.configManager;
    exporter.checkConnection();
    const prlist: Promise<any>[] = [];
    const eventIdList:Set<string> = new Set<string>();

    manager.Config.series.forEach((series: ConfigSeries) => {
      prlist.push(this.exportSeriesWithEvents(series, exporter, eventIdList));
    });

    const retpr: Promise<void> = new Promise((resolve, reject) => {
      Promise.all(prlist)
        .then(() => resolve)
        .catch((err) => reject(err));
    });
    return retpr;
  }

  exportAllSeries(exporter: SeriesExporter): Promise<void> {
    const manager: ConfigManager = this.configManager;
    exporter.checkConnection();

    const prlist: Promise<any>[] = [];
    const eventIdList:Set<string> = new Set<string>();

    manager.Config.series.forEach((series: ConfigSeries) => {
      if (series.enabled == undefined || (series.enabled == true && (series.id || series.alias))) {
        prlist.push(this.exportSeriesWithEvents(series, exporter, eventIdList));
      }
    });
    const pr:Promise<void> = new Promise((resolve, reject) => {
      Promise.all(prlist)
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
    return pr;
  }

  exportEventsFromSeries(series: ConfigSeries, exporter: EventExporter, eventIdList:Set<string>): Promise<string[]> {
    const pr: Promise<string[]> = new Promise((resolve, reject) => {
      const prSeriesData: Promise<SeriesLoadResult> = this._seriesDataController.retrieveSeries(series, ALL_DATES);
      prSeriesData.then((seriesLoad: SeriesLoadResult) => {
        console.log(`Exporting ${seriesLoad.numberLoaded} events from series '${seriesIdentifier(series)}'`);
        // const prlist: Promise<string>[] = [];

        const saves:Promise<string>[] = [];
        seriesLoad.eventsData.forEach((eventData: EventDataDetail) => {
          if (eventData.cacheFileName) {
            eventData.id = eventData.cacheFileName;
            if (!eventIdList.has(eventData.id)) {
              eventIdList.add(eventData.id);
              saves.push(exporter.saveEvent(eventData));
            }
          } else {
            const msg = `Event loaded in ${seriesLoad.series?.SeriesDetail?.displayname} from \
  ${seriesLoad.dataSource} has no cacheFileName: ${eventData.name}`;
            console.warn(msg);
            reject(new Error(msg));
          }
        });
        Promise.all(saves)
          .then((ids: string[]) => {
            resolve(ids);
          })
          .catch((err) => reject(err));
      });
    });

    return pr;
  }

  exportSeriesDefinition(series: ConfigSeries, ids: string[], exporter: SeriesExporter): Promise<number> {
    return new Promise((resolve, reject) => {
      this._seriesDataController.getSeriesDataDetailFromConfig(series).then((seriesDetail: SeriesDataDetail) => {
        const seriesDataDetail:SeriesDataDetail = {
          ...seriesDetail,
        };
        resolve(exporter.saveSeries(seriesDataDetail));
      }).catch((err) => reject(err));
    });
  }

  exportSeriesList(exporter: SeriesExporter): Promise<void> {
    return this._seriesDataController.getSeriesDataList().then((detail:SeriesDataDetail[]) => {
      exporter.checkConnection();
      return exporter.saveSeriesList(detail);
    });
  }

  exportSeriesWithEvents(series: ConfigSeries, exporter: SeriesExporter, eventIdList:Set<string>): Promise<void> {
    const returnpr: Promise<void> = new Promise((resolve, reject) => {
      this.exportEventsFromSeries(series, exporter, eventIdList).then((ids: string[]) => {
        this.exportSeriesDefinition(series, ids, exporter)
          .then(() => resolve())
          .catch((err) => reject(err));
      });
    });

    return returnpr;
  }
}

export { DataExportController };

// this.exportEventsFromSeries(series, exporter).then((ids:string[]) => {

// });

// this._seriesDataController.getSeriesResults(series).then((data:SeriesData) => {

//   // if (series.id) {
//   //   const idFile = path.resolve(exportPath, `${series.id}.json`);
//   //   fs.writeFileSync(idFile, JSON.stringify(data), 'utf8');

//   //   const idExcelFile = path.resolve(exportPath, `${series.id}.xlsx`);
//   //   const outputExcelSteam:fs.WriteStream = fs.createWriteStream(idExcelFile, 'utf8');
//   //   prlist.push(this._excelSeriesController.seriesToExcelStream(data, idExcelFile, outputExcelSteam));
//   // }
//   // if (series.alias) {
//   //   const aliasFile = path.resolve(exportPath, `${series.alias}.json`);
//   //   fs.writeFileSync(aliasFile, JSON.stringify(data), 'utf8');

//   //   const aliasExcelFile = path.resolve(exportPath, `${series.alias}.xlsx`);
//   //   const outputExcelSteam:fs.WriteStream = fs.createWriteStream(aliasExcelFile, 'utf8');
//   //   prlist.push(this._excelSeriesController.seriesToExcelStream(data, aliasExcelFile, outputExcelSteam));
//   // }
// }).catch((err) => {
//   console.warn(`Failed while loading series ${seriesIdentifier(series)}: ${err}`);
// });
// }
// exportAllEvents(SeriesExportertoPath?:string):void {
