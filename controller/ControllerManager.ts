'use strict';

import { Config, ConfigManager } from './ConfigManager';

import { CSVDataFileController } from './CSVDataFileController';
import { CategoryController } from './CategoryController';
import { DataExportController } from './DataExportController';
import { DataFileCacheController } from './DataFileCacheController';
import { DataFileController } from './DataFileController';
import { DataFileValidationController } from './DataFileValidationController';
import { EntrantDataController } from './EntrantDataController';
import { EntrantMatchController } from './EntrantMatchController';
import { EventDataController } from './EventDataController';
import { EventDataFileController } from './EventDataFileController';
import { EventFilterController } from './EventFilterController';
import { ExcelDataFileController } from './ExcelDataFileController';
import { ExcelSeriesController } from './ExcelSeriesController';
import { JsonDataFileController } from './JsonDataFileController';
import { LicenseDataController } from './LicenseDataController';
import { SeriesDataController } from './SeriesDataController';
import { SeriesEntrantController } from './SeriesEntrantController';

export class ControllerManager {
  get SeriesDataController(): SeriesDataController {
    return this.seriesDataController;
  }
  get ExcelSeriesController(): ExcelSeriesController {
    return this.excelSeriesController;
  }
  get DataFileValidationController(): DataFileValidationController {
    return this.dataFileValidationController;
  }
  get DataExportController(): DataExportController {
    return this.dataExportController;
  }
  get EventDataController(): EventDataController {
    return this.eventDataController;
  }
  private categoryController: CategoryController = new CategoryController();
  private excelDataFileController: ExcelDataFileController = new ExcelDataFileController();
  private dataFileController: DataFileController = new DataFileController();
  private dataFileCacheController: DataFileCacheController = new DataFileCacheController();
  private csvDataFileController: CSVDataFileController = new CSVDataFileController();
  private entrantDataController: EntrantDataController = new EntrantDataController();
  private entrantMatchController: EntrantMatchController = new EntrantMatchController();
  private licenseDataController: LicenseDataController = new LicenseDataController();
  private eventDataController: EventDataController = new EventDataController();
  private eventDataFileController: EventDataFileController = new EventDataFileController();
  private seriesDataController: SeriesDataController = new SeriesDataController();
  private excelSeriesController: ExcelSeriesController = new ExcelSeriesController();
  private jsonDataCacheController: JsonDataFileController = new JsonDataFileController();
  private dataFileValidationController: DataFileValidationController = new DataFileValidationController();
  private dataExportController: DataExportController = new DataExportController();
  private eventFilterController: EventFilterController = new EventFilterController();
  private seriesEntrantController: SeriesEntrantController = new SeriesEntrantController();

  private configManager!: ConfigManager;

  public setConfig(config: Config) {
    this.configManager = new ConfigManager(config);
    this.configManager.validateConfig(config);

    this.categoryController.config = config;
    this.csvDataFileController.config = config;
    this.dataExportController.config = config;
    this.dataFileCacheController.config = config;
    this.dataFileController.config = config;
    this.eventDataController.config = config;
    this.excelSeriesController.config = config;
    this.seriesDataController.config = config;

    this.dataFileController.configManager = this.configManager;
    this.dataExportController.configManager = this.configManager;
    this.dataFileValidationController.configManager = this.configManager;
    this.eventDataController.configManager = this.configManager;
    this.eventDataFileController.configManager = this.configManager;
    this.eventFilterController.configManager = this.configManager;
    this.seriesDataController.configManager = this.configManager;
  }

  public createControllers(): void {
    this.dataFileController.dataFileCacheController = this.dataFileCacheController;
    this.dataFileController.excelDataFileController = this.excelDataFileController;
    this.dataFileController.csvDataFileController = this.csvDataFileController;
    this.dataFileController.jsonDataFileController = this.jsonDataCacheController;

    this.seriesDataController.DataFileController = this.dataFileController;
    this.seriesDataController.EventDataController = this.eventDataController;
    this.seriesDataController.CategoryController = this.categoryController;
    this.seriesDataController.EntrantDataController = this.entrantDataController;
    this.seriesDataController.EntrantMatchController = this.entrantMatchController;
    this.seriesDataController.EventDataFileController = this.eventDataFileController;
    this.seriesDataController.EventFilterController = this.eventFilterController;
    this.seriesDataController.SeriesEntrantController = this.seriesEntrantController;
    this.entrantDataController.EntrantMatchController = this.entrantMatchController;
    this.entrantDataController.LicenseDataController = this.licenseDataController;
    this.entrantMatchController.EntrantDataController = this.entrantDataController;
    this.entrantMatchController.CategoryController = this.categoryController;
    this.entrantMatchController.LicenseDataController = this.licenseDataController;
    this.eventDataController.DataFileController = this.dataFileController;
    this.eventDataController.DataFileValidationController = this.dataFileValidationController;
    this.eventDataController.EventDataFileController = this.eventDataFileController;
    this.eventFilterController.EventDataController = this.eventDataController;
    this.excelSeriesController.SeriesDataController = this.seriesDataController;

    this.dataFileValidationController.DataFileController = this.dataFileController;
    this.dataFileValidationController.EntrantDataController = this.entrantDataController;

    this.dataExportController.SeriesDataController = this.SeriesDataController;
    this.dataExportController.ExcelSeriesController = this.ExcelSeriesController;
  }
}

export type { DataExportController,
  DataFileValidationController,
  EventDataController,
  ExcelSeriesController,
  SeriesDataController
};
