'use strict';
import fs, { Stats } from 'fs';
import { AbstractDataController } from './AbstractDataController';
import { ExcelFileUtils } from '../src/ExcelFileUtils';
import { JsonUtils } from '../src/jsonUtils';
import os from 'os';
import path from 'path';
import { strEndsWith } from '../src/endsWith';

type CachedData = {
  modified: number;
  data: any;
};

export class DataFileCacheController extends AbstractDataController {
  fileCache: Map<string, CachedData> = new Map<string, CachedData>();
  constructor() {
    super();
  }

  sanitiseFilePath(filePath: string): string {
    const replaceRegex = /[/.\\:!]/g;
    let safePath: string | undefined;
    if (!filePath) {
      throw new Error('Trying to sanitize null filePath');
    } else if (strEndsWith(filePath, '.json')) {
      safePath = filePath.substring(0, filePath.length - 5).replace(replaceRegex, '_') + '.json';
    } else {
      safePath = filePath.replace(replaceRegex, '_');
    }
    if (!safePath.endsWith('.json')) {
      safePath = safePath + '.json';
    }

    return safePath;
  }

  getCachePathForFile(filePath: string): string {
    const safePath: string = this.sanitiseFilePath(filePath);
    let fullPath: string;
    if (this.config && this.config.cacheLocation) {
      if (strEndsWith(this.config.cacheLocation, '/') || strEndsWith(this.config.cacheLocation, '\\')) {
        fullPath = this.config.cacheLocation + safePath;
      } else {
        fullPath = this.config.cacheLocation + path.sep + safePath;
      }
    } else {
      const tmpDir: string = os.tmpdir();
      fullPath = tmpDir + path.sep + safePath;
    }
    return fullPath;
  }

  getCacheFilePath(): string {
    if (this.config.eventCacheOutputLocation) {
      if (strEndsWith(this.config.eventCacheOutputLocation, '/')) {
        return this.config.eventCacheOutputLocation;
      } else {
        return this.config.eventCacheOutputLocation + '/';
      }
    }
    return './';
  }

  cacheFile(filePath: string, jsonData: any, modTime?: number): void {
    const cacheFilePath = this.getCachePathForFile(filePath);
    fs.writeFileSync(cacheFilePath, JSON.stringify(jsonData), {
      encoding: 'utf8',
    });
    const cacheKey = this.getCacheKeyFromPath(filePath);
    const exists: boolean = fs.existsSync(cacheFilePath);
    if (!exists && !modTime) {
      modTime = new Date().getTime();
    } else if (exists) {
      const stats = fs.statSync(cacheFilePath);
      if (stats && stats.mtime) {
        modTime = stats.mtime.getTime();
      }
    }

    if (!modTime) {
      modTime = new Date().getTime();
    }
    this.fileCache.set(cacheKey, {
      data: jsonData,
      modified: modTime,
    });
  }

  writeCacheFile(filePath: string, eventData: any): void {
    const cacheFilePath: string = this.getCacheFilePath();
    const outputFile: string = cacheFilePath + filePath;
    const eventDataStr: string = JSON.stringify(eventData);
    fs.writeFileSync(outputFile, eventDataStr);
  }

  getFilenameOnly(filePath: string): string {
    if (ExcelFileUtils.isXlsFile(filePath)) {
      const filename = ExcelFileUtils.getExcelFilenameForPath(filePath);
      return filename;
    } else {
      return filePath;
    }
  }

  removeFromCache(resultFilePath: string, options = {}): void {
    const cacheKey = this.getCacheKeyFromPath(resultFilePath, options);
    this.fileCache.delete(cacheKey);
  }

  safeFileTime(resultFilePath: string):Date {
    const safeFilePath: string = this.getFilenameOnly(resultFilePath);
    if (!fs.existsSync(safeFilePath)) {
      throw new Error(`File ${safeFilePath} does not exist to get file time from.`);
    }
    const ft: Date = this.getFileTime(safeFilePath);
    return ft;
  }

  isNewerThanCache(resultFilePath: string, options = {}): boolean {
    const ft: Date = this.safeFileTime(resultFilePath);

    if (this.isCachedInMemory(resultFilePath)) {
      const cacheKey: string = this.getCacheKeyFromPath(resultFilePath, options);
      const key: CachedData | undefined = this.fileCache.get(cacheKey);

      const modTime: number = key ? key.modified : new Date().getTime();
      const ftms: number = ft.getTime();
      if (!modTime || ftms > modTime) {
        return true;
      }
      return false;
    } else {
      const cacheTime: Date = this.getDiskCacheTime(resultFilePath);
      return !cacheTime || ft > cacheTime;
    }
  }

  cacheTextThenContinue(key: string, textJsonData: any, modTime: number, onSuccess: Function): Promise<any> {
    const pr = new Promise((resolve, reject) => {
      const jsonData = JSON.parse(textJsonData);
      try {
        jsonData.Columns = JsonUtils.getColumnsUsed(jsonData);
        this.fileCache.set(key, {
          data: jsonData,
          modified: modTime,
        });
        resolve(jsonData);
      } catch (err) {
        reject(err);
      }
    });
    if (onSuccess) {
      pr.then((jsonData: any) => {
        onSuccess(jsonData);
      });
    }
    return pr;
  }

  getCacheKeyFromPath(path: string, options?: any): string {
    if (ExcelFileUtils.isXlsFile(path)) {
      let sheet = undefined;
      if (options && options.Worksheet) {
        sheet = options.Worksheet;
      }
      return ExcelFileUtils.createCacheKey(path, sheet);
    } else {
      return path;
    }
  }

  isCachedInMemory(path: string, options?: any): boolean {
    const cacheKey = this.getCacheKeyFromPath(path, options);
    if (this.fileCache.has(cacheKey)) {
      return true;
    }
    return false;
  }

  isCachedOnDisk(path: string): boolean {
    const cachePath = this.getCachePathForFile(path);
    return fs.existsSync(cachePath);
  }

  isCached(path: string): boolean {
    return this.isCachedInMemory(path) || this.isCachedOnDisk(path);
  }

  retrieveFromCache(resultFilePath: string, options = {}): any {
    const cacheKey = this.getCacheKeyFromPath(resultFilePath, options);
    if (this.isCachedInMemory(resultFilePath)) {
      return this.fileCache.get(cacheKey)?.data;
    } else if (this.isCachedOnDisk(resultFilePath)) {
      const diskCachePath: string = this.getCachePathForFile(resultFilePath);
      const inputFile: Buffer = fs.readFileSync(diskCachePath);
      return JSON.parse(inputFile.toString());
    }
  }

  getDiskCacheTime(path: string): Date {
    const cachePath: string = this.getCachePathForFile(path);
    return this.getFileTime(cachePath);
  }

  getFileTime(filePath: string): Date {
    const stat: Stats = fs.statSync(filePath);
    return stat.mtime;
  }
}
