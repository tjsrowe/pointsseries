'use strict';

import DataSource from '../../controller/datasource/DataSource';
import { PathLike } from 'fs';
import fs from 'fs';
import path from 'path';

export abstract class FSDS<T, idtype> implements DataSource<T, idtype> {
  private _extension = '.json';
  private _path: PathLike;

  constructor(dataDir:PathLike, extension:string) {
    this._extension = extension;
    this._path = dataDir;
  }

  getBaseDir():string {
    return this._path.toString();
  }

  getFilePath(id:idtype):string {
    return path.join(this.getBaseDir(), this.idToString(id), this._extension);
  }

  get(id:idtype):Promise<T> {
    const fullFilePath:string = this.getFilePath(id);
    return new Promise<T>((resolve, reject) => {
      fs.readFile(fullFilePath, (err, data) => {
        if (err) {
          reject(err);
        } else {
          const parsed:any = data.toJSON();
          const output:T = {
            ...parsed
          };
          resolve(output);
        }
      });
    });
  }

  convertEachFile(filenames:string[]):Promise<T[]> {
    const fileReadPromises:Promise<T>[] = [];
    filenames.forEach((filename) => {
      const fpr:Promise<T> = new Promise((resolveFile, rejectFile) => {
        fs.readFile(filename, (ferr, fdata) => {
          if (ferr) {
            rejectFile(ferr);
          } else {
            const parsed:any = fdata.toJSON();
            const output:T = {
              ...parsed
            };
            resolveFile(output);
          }
        });
      });
      fileReadPromises.push(fpr);
    });
    return Promise.all(fileReadPromises);
  }

  getAll():Promise<T[]> {
    return new Promise<T[]>((resolve, reject) => {
      fs.readdir(this._path, (err, files:string[]) => {
        if (err) {
          reject(err);
        } else {
          const includedFiles:string[] = files.filter((filename) => {
            return path.extname(filename) == this._extension;
          });
          return this.convertEachFile(includedFiles);
        }
      });
    });
  }
  abstract save(item:T):Promise<idtype>;
  abstract idToString(id:idtype):string;
}
