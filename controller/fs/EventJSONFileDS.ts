'use strict';

import { EventDataDetail } from '../../src/model';
import JSONFileDS from './JSONFileDS';
import { PathLike } from 'fs';

export default class EventJSONFileDS extends JSONFileDS<EventDataDetail, string> {
  constructor(fileDir:PathLike) {
    super(fileDir);
  }
  save(event: EventDataDetail): Promise<string> {
    throw new Error(`Method not implemented to save ${event.id}`);
  }
  idToString(id: string): string {
    return id;
  }
}
