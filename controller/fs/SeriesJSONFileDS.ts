'use strict';

import JSONFileDS from './JSONFileDS';
import { PathLike } from 'fs';
import { Series } from '../../src/utils/series';

export default class SeriesJSONFileDS extends JSONFileDS<Series, number> {
  constructor(fileDir:PathLike) {
    super(fileDir);
  }
  save(item: Series): Promise<number> {
    throw new Error(`Method not implemented to save ${item.SeriesDetail.displayname}`);
  }
  idToString(id: number): string {
    return id.toString();
  }
}
