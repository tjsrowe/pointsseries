'use strict';

import { FSDS } from './FSDS';
import { PathLike } from 'fs';

export default abstract class JSONFileDS<T, idtype> extends FSDS<T, idtype> {
  constructor(fileDir:PathLike) {
    super(fileDir, '.json');
  }
}
