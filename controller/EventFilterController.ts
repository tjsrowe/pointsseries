'use strict';

import { ConfigSeries, Filter } from 'config/Config';
import { DateRange, ISO8601Date } from '../types';
import { EventInfo, SeriesDetail } from '../src/model';

import { AbstractDataController } from './AbstractDataController';
import { EventDataController, } from './EventDataController';
import { getDefaultDateRange } from '../src/dateUtils';
import { isInDateRange } from '../src/isInDateRange';

export class EventFilterController extends AbstractDataController {
  private eventDataController!:EventDataController;

  public set EventDataController(edc:EventDataController) {
    this.eventDataController = edc;
  }

  eventMatchesFormatFilter(filter: Filter, eventFormat: string) {
    if (filter) {
      if (!eventFormat) {
        return true;
      } else if (filter.Format && eventFormat && formatMatches(filter.Format, eventFormat)) {
        return true;
      }
      return false;
    }
    return true;
  }

  eventPassesDateFilter(filter: Filter, isoDate: ISO8601Date) {
    const dates: DateRange = {
      fromDate: filter.FromDate,
      toDate: filter.ToDate,
    };
    return isInDateRange(isoDate, dates);
  }

  eventPassesFilter(filter: Filter, event: EventInfo): boolean {
    if (!this.eventMatchesFormatFilter(filter, event.format)) {
      return false;
    }
    if (event.date && !this.eventPassesDateFilter(filter, event.date)) {
      return false;
    }
    return true;
  }

  hasDateFilter(configSeries: ConfigSeries|SeriesDetail): boolean {
    if (configSeries.filter && (configSeries.filter.FromDate || configSeries.filter.ToDate)) {
      return true;
    }
    return false;
  }

  dateFilterAllowsOverride(configSeries:ConfigSeries|SeriesDetail):boolean {
    if (!configSeries.filter ||
      configSeries.allowsDateOverride == false) {
      return false;
    }
    return true;
  }

  getDateRange(dateRange:DateRange|undefined = undefined, filter:Filter|undefined):DateRange {
    let outputDateRange:DateRange;
    if (dateRange) {
      outputDateRange = { ...dateRange };
    } else {
      outputDateRange = getDefaultDateRange();
    }

    if (filter && filter.FromDate != undefined) {
      outputDateRange.fromDate = filter.FromDate;
    }

    if (filter && filter.ToDate != undefined) {
      outputDateRange.toDate = filter.ToDate;
    }

    if (outputDateRange.fromDate && outputDateRange.toDate) {
      console.debug(`Showing results for all dates ${outputDateRange.fromDate} to ${outputDateRange.toDate}`);
    } else if (outputDateRange.fromDate && !outputDateRange.toDate) {
      console.debug(`Showing results from ${outputDateRange.fromDate}`);
    } else if (outputDateRange.toDate && !outputDateRange.fromDate) {
      console.debug(`Showing results up to ${outputDateRange.toDate}`);
    }
    return outputDateRange;
  }

  isEventInDateRange(event: EventInfo, dateRange: DateRange): boolean {
    if (!this.eventDataController) {
      throw new Error('EventDataController not set when calling EventFilterController.isEventInDateRange');
    }
    if (!event.date) {
      return false;
    }
    const eventDate: string | undefined = this.eventDataController.getEventDate(event);
    if (!eventDate) {
      return false;
    }
    return isInDateRange(eventDate, dateRange);
  }
}

export function formatMatches(filterFormat: string|string[], eventFormat: string): boolean {
  if (Array.isArray(filterFormat)) {
    for (let i = 0; i < filterFormat.length; i++) {
      if (filterFormat[i].toLowerCase() == eventFormat.toLowerCase()) {
        return true;
      }
    }
  } else if (filterFormat.toLowerCase() == eventFormat.toLowerCase()) {
    return true;
  }
  return false;
}
