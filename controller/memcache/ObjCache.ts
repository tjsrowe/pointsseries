'use strict';

import DataSource from '../datasource/DataSource';

export abstract class ObjCache<T, idtype> implements DataSource<T, idtype> {
  private _data:Map<idtype, T>;
  constructor() {
    this._data = new Map<idtype, T>();
  }
  save(item: T): Promise<idtype> {
    const id:idtype = this.getItemId(item);
    this.cacheItem(id, item);
    return Promise.resolve(id);
  }

  abstract getItemId(item: T): idtype;

  cacheItem(key:idtype, item:T):void {
    this._data.set(key, item);
  }

  get(id: idtype): Promise<T> {
    return new Promise((resolve, reject) => {
      if (!this._data.has(id)) {
        reject(new Error(`Item not found in cache with key ${id}`));
      } else {
        resolve(this._data.get(id)!);
      }
    });
  }

  getAll():Promise<T[]> {
    const output:T[] = [];
    this._data.forEach((element:T) => {
      const copy:T = { ...element };
      output.push(copy);
    });
    return Promise.resolve(output);
  }
}
