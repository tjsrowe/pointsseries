'use strict';

import { EventDataDetail } from '../../src/model';
import { ObjCache } from './ObjCache';

export class EventCache extends ObjCache<EventDataDetail, string> {
  getItemId(item: EventDataDetail): string {
    return item.id!;
  }
}
