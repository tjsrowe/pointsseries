'use strict';

import { ObjCache } from './ObjCache';
import { SeriesDataDetail } from '../../src/model';

export class SeriesCache extends ObjCache<SeriesDataDetail, number> {
  private aliases:Map<string, number> = new Map<string, number>();

  getItemId(item: SeriesDataDetail): number {
    return item.id!;
  }

  cacheItem(key: number, item: SeriesDataDetail): void {
    if (item.alias) {
      const alias:string = item.alias.toLocaleLowerCase();
      this.aliases.set(alias, item.id);
    }
    super.cacheItem(key, item);
  }

  getByAlias(alias:string):Promise<SeriesDataDetail> {
    const id:number|undefined = this.aliases.get(alias.toLocaleLowerCase());
    if (id) {
      return super.get(id);
    }
    const errMsg = `No id for alias of Series ${alias}`;
    console.log(errMsg);
    return Promise.reject(new Error(errMsg));
  }
}
