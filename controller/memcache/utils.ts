'use strict';

import { EventDataDetail, SeriesDataDetail } from '../../src/model';

import { EventCache } from './EventCache';
import JSZip from 'jszip';
import { SeriesCache } from './SeriesCache';

export interface CacheSet {
  series:SeriesCache;
  events:EventCache;
  dataSource?:string;
}

export const createCacheSetFromZip = (body:any) => {
  const datazip = new JSZip();
  const zipLoader:Promise<CacheSet> = new Promise<CacheSet>((resolve, reject) => {
    const output:CacheSet = {
      events: new EventCache(),
      series: new SeriesCache(),
    };
    const promiseList:Promise<void>[] = [];

    datazip.loadAsync(body).then((zip:JSZip) => {
      zip.forEach((path:string) => {
        promiseList.push(new Promise((resolveFile, rejectFile) => {
          zip.file(path)?.async('string').then((data:string) => {
            try {
              if (path.startsWith('series')) {
                const parsedSeries:SeriesDataDetail = JSON.parse(data);
                output.series.save(parsedSeries).then(() => {
                  resolveFile();
                });
              } else if (path.startsWith('events')) {
                const parsedEvent:EventDataDetail = JSON.parse(data);
                output.events.save(parsedEvent).then(() => {
                  resolveFile();
                });
              } else {
                console.log(`Resolved unknown ${path}`);
                resolveFile();
              }
            } catch (err) {
              console.log(`Rejected: ${err}`);
              rejectFile(err);
            }
          });
        }));
      });
      Promise.all(promiseList).then(() => {
        resolve(output);
      }).catch((err) => reject(err));
    });
  });
  return zipLoader;
};
