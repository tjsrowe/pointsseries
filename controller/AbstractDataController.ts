'use strict';

import { Config, ConfigManager } from './ConfigManager';

export abstract class AbstractDataController {
  _config!: Config;

  public set config(cfg: Config) {
    this._config = cfg;
  }

  public get config(): Config {
    return this._config;
  }

  _cfgMgr!: ConfigManager;

  public set configManager(cm: ConfigManager) {
    this._cfgMgr = cm;
  }

  public get configManager(): ConfigManager {
    return this._cfgMgr;
  }
}
