import * as os from 'os';

import { Config, ConfigSeries, ResultDataAliases } from '../config/Config';

import fs from 'fs';
import { replaceEnvValues } from '../src/envutils';

const DEFAULT_LICENSE_FIELD_ALIASES = ['LicenseNumber', 'LicenceNumber', 'License', 'Licence', 'MTBANumber'];

const DEFAULT_LICENSE_TYPES = ['AC', 'MTBA', 'CA', 'BMXA', 'UCI'];

const DEFAULT_FIELD_ALIASES = {
  Category: ['NationalCupCategory', 'NatCupCategory', 'NatCupCat', 'Cat', 'EventCategory', 'NationalCupAgeCategory'],
  Firstname: ['Givenname'],
  IsIneligible: ['Ineligible', 'Expired'],
  Result: [
    'Race_Position',
    'EligiblePIC',
    'Rank',
    'EventRank',
    'Categoryposition',
    'CategoryPosition',
    'NatCupRank',
    'NationalCupRank',
  ],
  Surname: ['Lastname'],
};

const DEFAULT_DOB_FIELDS = ['DOB', 'DateOfBirth'];

class ConfigManager {
  public constructor(cfg: Config) {
    this._config = cfg;
  }

  private _config: Config;

  public get Config(): Config {
    return this._config;
  }

  public findSeriesById(id: string, cfg: Config | undefined = undefined): ConfigSeries | undefined {
    if (cfg == undefined) {
      cfg = this._config;
    }
    if (!id) {
      return undefined;
    } else if (cfg.series.length == 0) {
      throw new Error('Config series array had length of 0');
    } else {
      for (let i = 0; i < cfg.series.length; i++) {
        const seriesItem: ConfigSeries = cfg.series[i];
        if (parseInt(id) == seriesItem.id) {
          return seriesItem;
        } else if (seriesItem.alias && seriesItem.alias.toLowerCase() == id.toLowerCase()) {
          return seriesItem;
        }
      }
    }
    return undefined;
  }

  public getLicenseFields(cfg: Config | undefined = undefined): string[] {
    const config: Config = cfg ? cfg : this._config;
    if (config.licenseFields) {
      return config.licenseFields;
    } else {
      return DEFAULT_LICENSE_FIELD_ALIASES;
    }
  }

  public getLicenseTypes(cfg: Config | undefined = undefined): string[] {
    const config: Config = cfg ? cfg : this._config;

    if (config.licenseTypes) {
      return config.licenseTypes;
    } else {
      return DEFAULT_LICENSE_TYPES;
    }
  }

  public getResultDataAliases(cfg: Config | undefined = undefined): ResultDataAliases {
    const config: Config = cfg ? cfg : this._config;

    if (config.licenseTypes) {
      return config.resultDataAliases;
    } else {
      return DEFAULT_FIELD_ALIASES;
    }
  }

  public getDobFields(cfg: Config | undefined = undefined) {
    const config: Config = cfg ? cfg : this._config;

    if (config.dobFields) {
      return config.dobFields;
    } else {
      return DEFAULT_DOB_FIELDS;
    }
  }

  public getTempUploadFilePath(cfg: Config | undefined = undefined): string {
    const config: Config = cfg ? cfg : this._config;

    if (config.tempUploadPath) {
      return config.tempUploadPath;
    } else {
      return os.tmpdir();
    }
  }

  static getDefaultConfig(defaults?: any | undefined): Config {
    const cfg: Config = {
      cacheLocation: os.tmpdir(),
      dobFields: DEFAULT_DOB_FIELDS,
      eventCacheOutputLocation: os.tmpdir(),
      eventDataFileLocation: os.tmpdir(),
      licenseFields: DEFAULT_LICENSE_FIELD_ALIASES,
      licenseTypes: DEFAULT_LICENSE_TYPES,
      resultDataAliases: DEFAULT_FIELD_ALIASES,
      series: [],
      tempUploadPath: os.tmpdir(),
      ...defaults,
    };
    return cfg;
  }

  private validateAllSeries(seriesList: ConfigSeries[]): void {
    const idSet: Set<number> = new Set<number>();
    seriesList.forEach((series, idx) => {
      if (series.id && idSet.has(series.id)) {
        throw new Error(`Series ID ${series.id} is not unique in config file.`);
      } else if (series.id == undefined) {
        throw new Error(`Series requires id property which is missing at object at index ${idx}`);
      } else if (series.id) {
        idSet.add(series.id);
      } else if (!series.name) {
        throw new Error(`Series ID ${series.id} has no name property`);
      }
    });
    seriesList.forEach((series) => {
      this.validateSeries(series);
    });
  }

  private validateSeries(series: ConfigSeries): void {
    if (series.id && typeof series.id !== 'number') {
      throw new Error(`series.id for ${series.id} must be a number, not a string`);
    }
  }

  public validateConfig(cfg: Config | undefined = undefined): void {
    if (!cfg) {
      cfg = this._config;
    }

    if (cfg.series == undefined) {
      throw new Error('config.series is undefined and must contain at least one valid series.');
    }

    if (cfg.series.length == 0) {
      throw new Error('config.series must have at least one series configured.');
    }

    this.validateAllSeries(cfg.series);
  }

  public static readConfig(fileToRead: string): Promise<Config> {
    const prConfig: Promise<Config> = new Promise((resolve, reject) => {
      if (!fs.existsSync(fileToRead)) {
        console.warn(`Config file ${fileToRead} does not exist.`);
      } else {
        const data: any = fs.readFileSync(fileToRead, 'utf8');
        const fixedData: any = replaceEnvValues(data);
        try {
          const parsed: any = JSON.parse(fixedData);
          const config: Config = {
            ...parsed,
          };

          console.log('Config loaded from ' + fileToRead);
          resolve(config);
        } catch (err) {
          console.error('Failed parsing config file at ' + fileToRead + ': ' + err);
          reject(err);
        }
      }
    });
    return prConfig;
  }

  public static loadDefaultConfig(): Promise<ConfigManager> {
    const pr: Promise<ConfigManager> = new Promise((resolve, reject) => {
      let configFile: string;
      if (process.env['config']) {
        console.log('Using env.config: ' + process.env['config']);

        configFile = process.env['config'];
      } else {
        configFile = './config/config.json';
      }
      ConfigManager.readConfig(configFile)
        .then((c: Config) => {
          const mgr: ConfigManager = new ConfigManager(c);
          resolve(mgr);
        })
        .catch((err) => {
          reject(err);
        });
    });

    return pr;
  }
}

const seriesIdentifier = (series: ConfigSeries): string => {
  let output = 'Series';
  if (series.id) {
    output += ' ID: ' + series.id;
  }
  if (series.alias) {
    output += ' Alias: ' + series.alias;
  } else if (series.description) {
    output += ' Desc: ' + series.description;
  }
  return output;
};

export { Config, ConfigManager, seriesIdentifier };
