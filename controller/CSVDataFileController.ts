'use strict';
import * as fs from 'fs';

import { AbstractDataController } from './AbstractDataController';
import { parse as csvReader } from 'csv-parse';
import dumpError from '../src/dumpError';
import { strEndsWith } from '../src/endsWith';

export class CSVDataFileController extends AbstractDataController {
  constructor() {
    super();
  }

  csvAsJsonSync(filePath: string, onSuccess: Function, onFailure: Function): void {
    if (!fs.existsSync(filePath)) {
      onFailure(Error('File ' + filePath + ' does not exist.'));
    }
    try {
      const stream = fs.createReadStream(filePath);
      const jsonData = this.syncCsvAsJson(stream);
      onSuccess(jsonData);
    } catch (err) {
      onFailure(err);
    }
  }

  syncCsvAsJson(stream: any): any[] {
    const records: any = [];

    const parser = stream.pipe(csvReader({}));

    parser.on('readable', function () {
      let record;
      while ((record = parser.read()) != null) {
        records.push(record);
      }
    });
    return records;
  }

  csvAsJson(filePath: string, onSuccess: Function, onFailure: Function): void {
    if (!fs.existsSync(filePath)) {
      onFailure(Error('File ' + filePath + ' does not exist.'));
    }
    const fileContents = fs.readFileSync(filePath);
    try {
      csvReader(fileContents, {}, function (err: Error | undefined, output: any) {
        if (err) {
          onFailure(err);
        } else {
          try {
            onSuccess(output, true);
          } catch (err) {
            console.debug(err);
          }
        }
      });
    } catch (err) {
      dumpError(err);
    }
  }

  isCsvFile(filePath: string): boolean {
    return strEndsWith(filePath, '.csv');
  }
}
