'use strict';

import fs from 'fs';

export class JsonDataFileController {
  loadJsonFile(filePath: string): Promise<any> {
    const pr: Promise<any> = new Promise((resolve, reject) => {
      fs.readFile(filePath, (err: NodeJS.ErrnoException | null, fileData: Buffer) => {
        if (!err) {
          resolve(fileData);
        } else {
          reject(err);
        }
      });
    });
    return pr;
  }
}
