'use strict';

import { CacheableDataFileLoad, DataFileController } from './DataFileController';
import {
  ChampionshipEventDetails,
  EventDataDetail,
  EventInfo,
  EventResult,
  SeriesEvent,
  SeriesEventFile
} from '../src/model/event';
import { Entrant, EventEntrant } from '../src/model/entrant';
import { SeriesDataDetail, SeriesDetail, SeriesResult } from '../src/model/series';
import { currentTimeMs, dateRangeToString, finishTimer } from '../src/dateUtils';

import { AbstractDataController } from './AbstractDataController';
import { Category } from '../src/model/category';
import { CategoryController } from './CategoryController';
import { CategorySet } from '../src/utils/categorySet';
import { ConfigSeries } from 'config/Config';
import DataSource from './datasource/DataSource';
import { DateRange } from '../types';
import { EntrantDataController } from './EntrantDataController';
import { EntrantMatchController } from './EntrantMatchController';
import { EventDataController } from './EventDataController';
import { EventDataFileController } from './EventDataFileController';
import { EventFilterController } from './EventFilterController';
import { Series } from '../src/utils/series';
import { SeriesEntrant } from '../src/model/seriesentrant';
import { SeriesEntrantController } from './SeriesEntrantController';
import assert from 'assert';
import { seriesIdentifier } from './ConfigManager';

const LOG_EVENT_FILTERS: boolean = process.env.LOG_EVENT_FILTERS === 'true' || false;
const LOG_MEMORY_CACHE:boolean = process.env.LOG_MEMORY_CACHE === 'true' || false;

export type SeriesLoadResult = {
  eventsData: EventDataDetail[];
  numberLoaded: number;
  numberSkipped: number;
  retrieveTime: number;
  dataSource: string;
  series?: Series;
};

type EventAddToSeriesStats = {
  ridersAdded: number;
  linesSkipped: number;
  totalTime: number;
};

export type PointsMap = Map<number | string, number>;
export type NamedPointsMap = Map<string, PointsMap>;

export class SeriesDataController extends AbstractDataController {
  private categoryController!: CategoryController;
  private dataFileController!: DataFileController;
  private entrantDataController!: EntrantDataController;
  private entrantMatchController!: EntrantMatchController;
  private eventDataFileController!: EventDataFileController;
  private eventDataController!: EventDataController;
  private eventFilterController!: EventFilterController;
  private seriesEntrantController!: SeriesEntrantController;
  private _maxCatId = 0;
  private _dataSource?: DataSource<SeriesDataDetail, number>;
  private _seriesPointsMaps: Map<number, NamedPointsMap> = new Map<number, NamedPointsMap>();

  public get maxCatId(): number {
    return this._maxCatId;
  }

  public set CategoryController(cc: CategoryController) {
    this.categoryController = cc;
  }

  public set DataFileController(dfc: DataFileController) {
    this.dataFileController = dfc;
  }

  public set EntrantMatchController(emc: EntrantMatchController) {
    this.entrantMatchController = emc;
    if (this.entrantDataController != undefined && this.entrantDataController.EntrantMatchController == undefined) {
      this.entrantDataController.EntrantMatchController = emc;
    }
  }

  public set EventDataController(edc: EventDataController) {
    this.eventDataController = edc;
  }

  public set EventDataFileController(edc: EventDataFileController) {
    this.eventDataFileController = edc;
  }

  public set EntrantDataController(edc: EntrantDataController) {
    this.entrantDataController = edc;
  }

  public set SeriesEntrantController(sec: SeriesEntrantController) {
    this.seriesEntrantController = sec;
  }

  public set EventFilterController(efc: EventFilterController) {
    this.eventFilterController = efc;
  }

  private findSeriesById(seriesId: string): ConfigSeries | undefined {
    return this.configManager.findSeriesById(seriesId);
  }

  public set DataSource(ds: DataSource<SeriesDataDetail, number>) {
    this._dataSource = ds;
  }

  private createSeriesDataFromConfig(configSeries: ConfigSeries): Series {
    const seriesDetail: SeriesDetail = this.getSeriesDetailFromConfig(configSeries);
    const cs: CategorySet = this.categoryController.createCategorySet(seriesDetail.categories);

    const seriesMetadata: Series = new Series(cs);
    seriesMetadata.SeriesDetail = seriesDetail;
    return seriesMetadata;
  }

  getSeriesDetailById(id: number): Promise<SeriesDataDetail> {
    if (this._dataSource) {
      return this._dataSource.get(id);
    } else {
      throw new Error('Need data source to call getSeriesDetail');
    }
  }

  getSeriesDetails(seriesId: string): SeriesDetail {
    const series: ConfigSeries | undefined = this.findSeriesById(seriesId);
    if (!series) {
      throw new Error(`Series for ${seriesId} doesn't exist.`);
    }
    return this.getSeriesDetailFromConfig(series);
  }

  getDisplayname(series: ConfigSeries): string {
    if (series.name) {
      return series.name;
    }
    if (series.alias) {
      return series.alias;
    }
    if (series.description) {
      return series.description;
    }
    throw Error(`No display name available in configSeries ${series}`);
  }

  getLink(series: ConfigSeries): string {
    if (series.alias) {
      return series.alias;
    }
    if (series.id) {
      return series.id.toString();
    }
    throw Error(`No alias or id name available in configSeries ${series}`);
  }

  getIdListFromEventsList(seriesFiles: SeriesEventFile[]): string[] {
    const ids: string[] = [];
    seriesFiles.forEach((file: SeriesEventFile) => {
      ids.push(file.id);
    });
    return ids;
  }

  getSeriesDataDetailFromConfig(series: ConfigSeries): Promise<SeriesDataDetail> {
    const seriesDetail: SeriesDetail = this.getSeriesDetailFromConfig(series);

    const pr: Promise<SeriesDataDetail> = new Promise<SeriesDataDetail>((resolve, reject) => {
      const prEventList: Promise<SeriesEventFile[]> = this.retrieveEventListForSeries(series);
      const prSeriesPoints: Promise<NamedPointsMap> = this.getPointsForSeries(series);
      Promise.all([prEventList, prSeriesPoints])
        .then(([events, seriesPoints]) => {
          const idList: string[] = this.getIdListFromEventsList(events);
          const dataDetail: SeriesDataDetail = {
            ...seriesDetail,
            eventIdList: idList,
            events: events,
            scoringSystem: Object.fromEntries(seriesPoints)
          };
          resolve(dataDetail);
        })
        .catch((err) => reject(err));
    });
    return pr;
  }

  getSeriesDetailFromConfig(series: ConfigSeries): SeriesDetail {
    if (!this.categoryController) {
      throw new Error(
        'CategoryController not set in SeriesDataController ' + 'when trying to convert SeriesConfig to SeriesDetail'
      );
    }
    const info: SeriesDetail = {
      alias: series.alias,
      allowsDateOverride: seriesAllowsDateOverride(series),
      categories: this.categoryController.getCategoryList(series),
      description: series.description,
      displayname: this.getDisplayname(series),
      enabled: series.enabled == false ? false : true,
      filter: series.filter,
      id: series.id,
      link: this.getLink(series),
      name: series.name!
    };
    return info;
  }

  createErrorMessageFromDataFileErrors(errs: any): string {
    return '';
  }

  retrieveSeries(series: ConfigSeries, dateRange: DateRange | undefined = undefined): Promise<SeriesLoadResult> {
    if (series.name == undefined && series.description == undefined) {
      throw new Error(`Series ${series.id} has no name property which is required`);
    }
    const useDateRange: DateRange = this.eventFilterController.getDateRange(dateRange, series.filter);

    const preLoad: number = currentTimeMs();
    const prSeries: Promise<SeriesLoadResult> = new Promise<SeriesLoadResult>(async (resolve, reject) => {
      try {
        const seriesEvents: SeriesEventFile[] = await this.retrieveEventListForSeries(series);
        // .then(async (seriesEvents: SeriesEventFile[]) => {
        const fileDirectory: string = this.dataFileController.getFileDirectory(series.eventsList!);

        const cacheLoadTime: number = finishTimer(preLoad);
        try {
          const data: SeriesLoadResult = await this.retrieveEventDataFromList(
            seriesEvents, fileDirectory, series, useDateRange);
          // .then((data: SeriesLoadResult) => {
          const xlsProcessTime: number = finishTimer(preLoad) - cacheLoadTime;

          const drs: string = dateRangeToString(useDateRange);
          console.log(`${seriesIdentifier(series)} loaded ${data.numberLoaded} (skipped ` +
            `${data.numberSkipped}) events between ${drs} for series ${series.id} ` +
            `with load times times: CacheLoad: ${cacheLoadTime} XLS Process: ${xlsProcessTime}`);
          data.retrieveTime = finishTimer(preLoad);
          data.eventsData.forEach((d) => {
            if (d.eventRawData.length == 0) throw Error(`Empty data returned for event ${d.resultFileName}`);
          });
          resolve(data);
        } catch (err: any) {
        // } catch (err: EventDataDetail[]) {
          if (err.message !== undefined && err.stack !== undefined) {
            console.error(err.message);
            reject(err);
          } else {
            const joinedMessage = (err as EventDataDetail[]).map((e:EventDataDetail) => e.errorList).join(', ');
            const logMsg = `Failed processing series xls data: ${joinedMessage}`;
            console.warn(logMsg);
            reject(err);
          }
        }
        // })
        // .catch((err: EventDataDetail[]) => {
        // // });
      } catch (err) {
      // .catch((err) => {
        console.log('Failed while getting event list XLS of series ID ' + series.id + ': ' + err);
        reject(err);
      }
    });
    return prSeries;
  }

  retrieveEventListForSeries(series: ConfigSeries): Promise<SeriesEventFile[]> {
    if (!series || !series.eventsList) {
      throw new Error('Provided series doesn\'t have an event list file.');
    }

    const eventListPromise: Promise<SeriesEventFile[]> = new Promise((resolve, reject) => {
      this.dataFileController
        .loadCachedDataFile(series.eventsList!, false)
        .then((loadResult: CacheableDataFileLoad) => {
          if (LOG_MEMORY_CACHE) {
            if (loadResult.isMemoryCache) {
              console.log(`Series file ${series.eventsList} loaded from memory cache.`);
            }
            if (loadResult.isDiskCache) {
              console.log(`Series file ${series.eventsList} loaded from disk cache.`);
            }
            if (loadResult.isDiskLoad) {
              console.log(`Series file ${series.eventsList} loaded from disk.`);
            }
          }
          this.eventDataFileController
            .convertEventJsonToEventMeta(loadResult.dataFileJsonData)
            .then((eventMeta: SeriesEventFile[]) => {
              resolve(eventMeta);
            })
            .catch((err) => {
              reject(err);
            });
        })
        .catch((err) => {
          reject(err);
        });
    });
    return eventListPromise;
  }

  public findEntrantInCategory(series: Series, entry: Entrant): SeriesEntrant | undefined {
    assert(
      this.entrantMatchController != undefined,
      'EntrantMatchController must be configuired on SeriesDataController'
    );

    const seriesEntrants: SeriesEntrant[] | undefined = series.getEntrantsInEntrantCategory(entry);
    if (seriesEntrants != undefined) {
      const matched: SeriesEntrant | undefined = seriesEntrants.find(
        (check) => this.entrantMatchController.matchEntrantPerson(entry, check),
        entry
      );

      return matched;
    }
    return undefined;
  }

  private createEntrant(series: Series, entry: EventEntrant): SeriesEntrant {
    let errMsg: string | undefined;
    if (Object.keys(entry).length == 0) {
      errMsg = `Object ${JSON.stringify(entry)} in has no properties, skipping`;
    } else if (!this.categoryController) {
      errMsg = 'categoryController not set in SeriesDataController while calling createSeriesEntry';
    } else if (
      entry.Firstname == undefined &&
      entry.Surname == undefined &&
      entry.FullName == undefined &&
      (entry.Firstname == undefined || entry.Surname == undefined)
    ) {
      errMsg = `Object ${JSON.stringify(entry)} doesn't have required name fields`;
    } else if (!entry.Category) {
      errMsg = `Object ${JSON.stringify(entry)} doesn't have a recognised category field`;
    }
    const cat: Category | undefined = series.getCategory(entry.Category);

    if (cat == undefined) {
      errMsg = `No category defined for name ${entry.Category}`;
    }
    if (errMsg) {
      throw Error(errMsg);
    }

    let nameElements = 0;
    const seriesEntry: SeriesEntrant = {
      Category: cat!.name,
      Gender: cat!.gender,
      results: [],

    };
    if (entry.Firstname) {
      seriesEntry.Firstname = entry.Firstname;
      nameElements = 1;
    }

    if (entry.Surname) {
      seriesEntry.Surname = entry.Surname.trim().toUpperCase();
      nameElements++;
    }

    try {
      if (entry.FullName) {
        splitNameTo(entry.FullName, seriesEntry);
        nameElements = 2;
      }
    } catch (err) {
      console.debug(`Got some weirdness while trying to split name [${entry.FullName}] so can't parse`);
    }

    if (nameElements != 2) {
      const nameErrMsg = `Object ${JSON.stringify(entry)} didn't have enough info to get a full name.`;
      throw Error(nameErrMsg);
    }
    return seriesEntry;
  }

  getRiderSeriesEntry(series: Series, entry: EventEntrant, eventName: string): SeriesEntrant {
    assert(this.entrantDataController, 'EntrantDataController must be defined on SeriesDataController.');

    let seriesEntry: SeriesEntrant | undefined = undefined;
    try {
      seriesEntry = this.findEntrantInCategory(series, entry);
      if (seriesEntry == undefined) {
        seriesEntry = this.createEntrant(series, entry);
        series.addEntrant(seriesEntry);
      }
    } catch (e: any) {
      e.message = `Exception while finding entrant in series in event ${eventName}: ${e.message}`;
      throw e;
    }
    this.entrantDataController.mergeEntrantIntoExisting(entry, seriesEntry);
    return seriesEntry;
  }

  addRiderToSeries(series: Series, eventId: number, entry: EventEntrant): boolean {
    assert(series.categoryCount > 0, 'Can\'t add riders to a series that has no categories.');
    assert(series.Events.length > 0, 'Cant add riders to series when events list has 0 length.');
    assert(series.Events[eventId] != undefined, `Series array didnt have index ${eventId}`);
    const eventName: string = series.Events[eventId].name;

    const seriesEntry: SeriesEntrant = this.getRiderSeriesEntry(series, entry, eventName);
    const er: EventResult | undefined = createEventResult(eventId, entry);


    if (er) {
      seriesEntry.results.push(er);
      return true;
    } else {
      throw new Error(`No result added because we couldn't create an \
EventResult for ${entry} in event ${eventName}`);
    }
  }

  addEventEntriesToSeries(series: Series, eventEntrants: EventEntrant[], eventId: number): EventAddToSeriesStats {
    assert(series.categoryCount > 0, 'Can\'t add entries to a series that has no categories.');
    let validEntrants: EventEntrant[] = this.getEntrantsWithValidResult(eventEntrants);
    validEntrants = this.getEntrantsWithValidCategory(series, validEntrants);
    const validResults: EventAddToSeriesStats = this.addValidEntrantsToSeries(series, validEntrants, eventId);
    validResults.linesSkipped += eventEntrants.length - validEntrants.length;
    return validResults;
  }

  public addValidEntrantsToSeries(
    series: Series,
    validEntrants: EventEntrant[],
    eventId: number
  ): EventAddToSeriesStats {
    const startTime: number = currentTimeMs();
    let ridersAdded = 0;
    let linesSkipped = 0;

    validEntrants.forEach((entrant: EventEntrant) => {
      this.ensureCategoryExists(series, entrant.Category);
      const added: boolean = this.addRiderToSeries(series, eventId, entrant);

      if (added) {
        ridersAdded++;
      } else {
        linesSkipped++;
      }
    });
    const totalTime: number = finishTimer(startTime);
    return { linesSkipped, ridersAdded, totalTime };
  }

  // Returns false if the category was created, or true if it already existed.
  ensureCategoryExists(series: Series, category: string): boolean {
    assert(category != undefined);
    if (!series.categoryExists(category)) {
      series.createUnrecognisedCategory(category);
      return false;
    }
    return true;
  }

  getEntrantsWithValidResult(eventRawData: EventEntrant[]): EventEntrant[] {
    const validEntrants: EventEntrant[] = eventRawData.filter((entrant: EventEntrant) => {
      if (entrant.Result == undefined) {
        return false;
      }
      if (Object.keys(entrant).length == 1) {
        return false;
      }
      return true;
    });
    return validEntrants;
  }

  getEntrantsWithValidCategory(series: Series, entrants: EventEntrant[]): EventEntrant[] {
    return entrants.filter((entrant: EventEntrant) => {
      if (entrant.Category == undefined) {
        return false;
      }
      return !entrant.Category.startsWith(' ');
      // series.hasCategory(entrant.Category);
    });
  }

  private getPointsFileName(id: string): string | undefined {
    const series: ConfigSeries | undefined = this.findSeriesById(id);
    if (!series) {
      return undefined;
    }
    return series.points;
  }

  private getExcludedRidersFileName(id: string): string | undefined {
    const series: ConfigSeries | undefined = this.findSeriesById(id);
    if (!series) {
      return undefined;
    }
    return series.excludedRiders;
  }

  private eventBlockToSeries(eventData: EventDataDetail[], configSeries: ConfigSeries): Series {
    const startTime: number = currentTimeMs();
    const series: Series = this.createSeriesDataFromConfig(configSeries);
    assert(
      series.categoryCount > 0,
      `Series must have >0 categories when created from config: ${JSON.stringify(configSeries)}.`
    );

    let numRidersAdded = 0;
    for (let i = 0; i < eventData.length; i++) {
      const event: EventDataDetail = eventData[i];
      event.eventId = i;
      numRidersAdded += this.addEventToSeries(series, event);
    }
    const totalTime: number = finishTimer(startTime);
    console.debug(`Added ${numRidersAdded} riders to series ${seriesIdentifier(configSeries)} [${totalTime}ms.]`);
    return series;
  }

  addEventToSeries(series: Series, event: EventDataDetail): number {
    assert(series.categoryCount > 0, 'Can\'t add event to a series that has no categories.');
    assert(event.eventId !== undefined, `EventID was undefined when adding riders to series in event ${event.name}.`);

    assert(event.eventId! >= 0, `EventID was <0 when adding riders to series in event ${event.name}.`);

    const eventId: number = event.eventId;

    const eventMetadata: SeriesEventFile = this.convertEventMetadataForSeries(event);
    series.addEvent(eventMetadata);
    // series.events.push(eventMetadata);
    const eventEntrants: EventEntrant[] = event.eventRawData;
    const addResult: EventAddToSeriesStats = this.addEventEntriesToSeries(series, eventEntrants, eventId);
    return addResult.ridersAdded;
  }

  private convertEventMetadataForSeries(event: EventDataDetail): SeriesEventFile {
    const seriesEvent: SeriesEventFile = {
      cacheFileName: event.cacheFileName,
      championshipStatus: event.championshipStatus,
      dataFileLoadTime: event.dataFileLoadTime,
      dataFileTime: event.dataFileTime,
      date: event.date,
      format: event.format,
      id: event.id,
      isFinal: event.isFinal,
      location: event.location,
      name: event.name,
      organiser: event.organiser,
      pointsFactor: event.pointsFactor,
      pointsSystem: event.pointsSystem,
      resultFileName: event.resultFileName,
      state: event.state,
      subtitle: event.subtitle,

    };
    return seriesEvent;
  }

  private hasPointsConfigured(configSeries: ConfigSeries): boolean {
    if (configSeries.points !== undefined) {
      return true;
    }
    if (configSeries.pointsSystems !== undefined && Object.keys(configSeries.pointsSystems).length > 0) {
      return true;
    }
    return false;
  }

  public getSeriesResults(configSeries: ConfigSeries, dateRange?: DateRange | undefined): Promise<SeriesResult> {
    if (!this.hasPointsConfigured(configSeries)) {
      const errMsg = 'No points file (s) specified.' ;
      console.trace(errMsg);
      return Promise.reject(new Error(`No points file given for series ${seriesIdentifier(configSeries)}`));
    }

    const startTime: number = currentTimeMs();

    // const prereqs: Promise<any>[] = [];
    const dfc: DataFileController = this.dataFileController;
    const pa: Promise<NamedPointsMap> = this.getPointsForSeries(configSeries);
    const prSeriesData: Promise<SeriesLoadResult> = this.retrieveSeries(configSeries, dateRange);

    let excludedRidersJson: Promise<JSON[]|undefined> = Promise.resolve(undefined);
    if (configSeries.excludedRiders) {
      excludedRidersJson = dfc.dataFileAsJson(configSeries.excludedRiders);
    }

    const prereqs: [
      Promise<NamedPointsMap>,
      Promise<SeriesLoadResult>,
      Promise<JSON[]|undefined>
    ] = [pa, prSeriesData, excludedRidersJson];

    const pr: Promise<SeriesResult> = new Promise((resolve, reject) => {
      Promise.all(prereqs)
        .then((prereqResults: [
          NamedPointsMap,
          SeriesLoadResult,
          JSON[]|undefined
        ]) => {
          const seriesPoints: NamedPointsMap = prereqResults[0];
          const seriesLoadResult: SeriesLoadResult = prereqResults[1];

          const series: Series = this.eventBlockToSeries(seriesLoadResult.eventsData, configSeries);
          if (prereqResults[2]) {
            series.excludedRiders = prereqResults[2];
            const si: string = seriesIdentifier(configSeries);
            console.log(`Loaded excluded riders file ${configSeries.excludedRiders} in series ${si}`);
          }
          // series.scoringSystem = orderPointsLookup(pointsJsonData);

          const pointAllocTime = currentTimeMs();
          series.setScoringSystems(seriesPoints);
          allocatePoints(series);
          // mergeSeriesEntrants(seriesResults.entrants, configSeries);
          const allocatePointsTime: number = finishTimer(pointAllocTime);
          const sortStartTime: number = currentTimeMs();
          // this.seriesEntrantController.sortSeriesEntrants(series);
          // this.removeNonSeriesCategories(series, configSeries!);
          const endTime: number = finishTimer(startTime);
          const entrantSortTime: number = finishTimer(sortStartTime);
          console.log(`Sorting times:  Retrieve series: ${seriesLoadResult.retrieveTime}ms.  ` +
            `Allocate points: ${allocatePointsTime}ms. Entrant sort: ${entrantSortTime}ms. Total: ${endTime}ms.`);
          const output: SeriesResult = convertSeriesToResult(series);

          resolve(output);
        })
        .catch((err) => {
          reject(err);
        });
    });
    return pr;
  }

  champKey(championshipStatus: ChampionshipEventDetails): string {
    return championshipStatus.format + '_' +
      championshipStatus.region;
  }

  champHash(championshipStatus: ChampionshipEventDetails): string {
    return this.champKey(championshipStatus) + '_' +
      championshipStatus.year;
  }

  removeNonLatestChampionshipEvents(events: SeriesEventFile[]): SeriesEventFile[] {
    const formatChamps: Map<string, number> = new Map();
    events.filter((e) => e.championshipStatus !== undefined).forEach((sef) => {
      const key = this.champKey(sef.championshipStatus!);
      if (formatChamps.has(key)) {
        const currentMapYear = formatChamps.get(key)!;
        if (sef.championshipStatus!.year > currentMapYear) {
          formatChamps.set(key, sef.championshipStatus!.year);
        }
      } else {
        formatChamps.set(key, sef.championshipStatus!.year);
      }
    });

    const includeChamps: Set<string> = new Set();
    formatChamps.forEach((value, key) => {
      const setKey = [key, value].join('_');
      includeChamps.add(setKey);
    });

    const outputEvents: SeriesEventFile[] = events.filter(
      (sef) => sef.championshipStatus === undefined ||
      includeChamps.has(this.champHash(sef.championshipStatus!)));
    return outputEvents;
  }

  public getSeriesEventsById(id: string): Promise<SeriesEventFile[]> {
    const configSeries: ConfigSeries | undefined = this.findSeriesById(id);
    if (!configSeries) {
      const errMsg = `No config series for id ${id}.`;
      console.trace(errMsg);
      return Promise.reject(new Error(`No points file given for series ${id}`));
    }

    const useDateRange: DateRange = this.eventFilterController.getDateRange(undefined, configSeries.filter);

    return new Promise((resolve, reject) => {
      this.retrieveEventListForSeries(configSeries).then((sefa: SeriesEventFile[]) => {
        console.debug(`Filter: ${JSON.stringify(configSeries.filter)}`);
        let outputEvents: SeriesEventFile[] = !configSeries.filter ? sefa :
          sefa.filter((sef: SeriesEventFile) =>
            !this.shouldSkipEventLoad(sef, configSeries, useDateRange));
        outputEvents = this.removeNonLatestChampionshipEvents(outputEvents);
        resolve(outputEvents);
      }).catch((err) => reject(err));
    }
    );
  }

  public async getSeriesResultsById(id: string, dateRange?: DateRange | undefined): Promise<SeriesResult> {
    const configSeries: ConfigSeries | undefined = this.findSeriesById(id);
    if (!configSeries) {
      const errMsg = `No config series for id ${id}.`;
      console.trace(errMsg);
      return Promise.reject(new Error(`No points file given for series ${id}`));
    }

    const useDateRange: DateRange = this.eventFilterController.getDateRange(dateRange, configSeries.filter);

    return this.getSeriesResults(configSeries, useDateRange);
  }

  private isChampionshipEvent(event: EventInfo, configSeries: ConfigSeries | SeriesDetail): boolean {
    if (!this.eventFilterController) {
      throw new Error('EventFilterController not set in SeriesDataController when calling isChampionshipEvent');
    }
    if (!event.championshipStatus) {
      return false;
    }
    if (
      !configSeries.filter ||
      this.eventFilterController.eventMatchesFormatFilter(configSeries.filter, event.format)
    ) {
      return true;
    }
    return false;
  }

  private isIncludedChampionship(event: EventInfo, configSeries: ConfigSeries | SeriesDetail): boolean {
    if (!this.eventFilterController) {
      throw new Error('EventFilterController not set in SeriesDataController when calling isIncludedChampionship');
    }
    const csDetails: ChampionshipEventDetails | undefined = event.championshipStatus;
    if (!csDetails) {
      return false;
    }
    if (
      configSeries.filter &&
      !this.eventFilterController.eventMatchesFormatFilter(configSeries.filter, event.format)
    ) {
      return false;
    }
    return true;
  }

  populateEventFromData(
    event: SeriesEventFile,
    configSeries: ConfigSeries,
    fileDirectory: string
  ): Promise<EventDataDetail> | undefined {
    if (
      (event.resultFileName && configSeries && !configSeries.filter) ||
      (configSeries.filter && this.eventFilterController.eventPassesFilter(configSeries.filter, event))
    ) {
      const rf: Promise<EventDataDetail> = this.eventDataController.requestEvent(event, fileDirectory);
      return rf;
    }
    return undefined;
  }

  createRiderInCategoryNumber(categoryMap: Map<number, Entrant[]>, entry: Entrant, categoryOrder: number) {
    let categoryEntrantList: Entrant[] | undefined = [];
    if (!categoryMap.has(categoryOrder)) {
      categoryMap.set(categoryOrder, categoryEntrantList);
    } else {
      categoryEntrantList = categoryMap.get(categoryOrder);
    }
    categoryEntrantList?.push(entry);
  }

  private shouldSkipEventLoad(
    event: EventInfo,
    configSeries: ConfigSeries | SeriesDetail,
    dateRange: DateRange
  ): boolean {
    if (!this.eventFilterController) {
      throw new Error('EventFilterController not set in SeriesDataController when calling shouldSkipEventLoad');
    }

    if (!event) {
      if (LOG_EVENT_FILTERS) {
        console.log('Skipped an empty event data row.');
      }
      return true;
    }

    if (
      configSeries.filter &&
      !this.eventFilterController.eventMatchesFormatFilter(configSeries.filter, event.format)
    ) {
      if (LOG_EVENT_FILTERS) {
        console.log(`Skipped event ${event.name} because the format, ${event.format}, isn't wanted for this series.`);
      }
      return true;
    }

    const isChampionship: boolean = this.isChampionshipEvent(event, configSeries);

    if (
      this.eventFilterController.hasDateFilter(configSeries) &&
      this.eventFilterController.dateFilterAllowsOverride(configSeries) &&
      !this.eventFilterController.isEventInDateRange(event, dateRange) && !isChampionship
    ) {
      const eventDate: string | undefined = this.eventDataController.getEventDate(event);
      if (LOG_EVENT_FILTERS) {
        console.log(`Skipped event ${event.name} because date ${eventDate} ` +
          'didn\'t fall within the target date range.');
      }
      return true;
    }
    if (
      !this.eventFilterController.hasDateFilter(configSeries) &&
      !this.eventFilterController.isEventInDateRange(event, dateRange) && !isChampionship
    ) {
      const eventDate: string | undefined = this.eventDataController.getEventDate(event);
      if (LOG_EVENT_FILTERS) {
        console.log(`Skipped event ${event.name} because date ${eventDate} \
didn't fall within the default date range.`);
      }
      return true;
    }

    if (!isChampionship) {
      if (LOG_EVENT_FILTERS) {
        console.log(`Included event ${event.name} because it matches other filters and is a not a championship.`);
      }
      return false;
    }
    if (!this.isIncludedChampionship(event, configSeries)) {
      if (LOG_EVENT_FILTERS) {
        console.log(`Skipped event ${event.name} because because it is not an included championship event.`);
      }

      return true;
    }
    return false;
  }

  // private retrieveEventtDetail(eventDetail: EventDataDetail[]): Promise<EventDataDetail[]> {

  // }

  // private loadEventData(data: EventDataDetail[], configSeries: ConfigSeries): Promise<SeriesLoadResult> {
  //   return new Promise((resolve, reject) => {
  //     // Remove championship events that aren't the most recent one.

  //   });
  // }

  private retrieveEventDataFromList(
    eventMeta: SeriesEventFile[],
    fileDirectory: string,
    configSeries: ConfigSeries,
    dateRange: DateRange
  ): Promise<SeriesLoadResult> {
    const startTime: number = currentTimeMs();
    const prData: Promise<SeriesLoadResult> = new Promise<SeriesLoadResult>(async (resolve, reject) => {
      const resultsPromiseList: Promise<EventDataDetail>[] = [];
      let loaded = 0;
      let skipped = 0;
      for (let i = 0; i < eventMeta.length; i++) {
        const event: SeriesEventFile = eventMeta[i];
        if (this.shouldSkipEventLoad(event, configSeries, dateRange)) {
          skipped++;
          continue;
        }
        const rf: Promise<EventDataDetail> | undefined = this.populateEventFromData(event, configSeries, fileDirectory);
        if (rf) {
          loaded++;
          resultsPromiseList.push(rf);
        } else {
          skipped++;
        }
      }

      // return this.loadEventData(resultsPromiseList, configSeries);
      Promise.all(resultsPromiseList)
        .then((data: EventDataDetail[]) => {
          data = this.removeNonLatestChampionshipEvents(data);
          // convert the data to a rider result object
          try {
            const eventData: EventDataDetail[] = this.eventDataController.parseAllEventResults(data);
            const errorEvents: EventDataDetail[] = eventData.filter((ed) => ed.eventRawData.length === 0);
            if (errorEvents.length > 0) {
              reject(new Error(`Event ${errorEvents[0].resultFileName} has no data.`));
            } else {
              const seriesMetadata: Series = this.createSeriesDataFromConfig(configSeries);
              const result: SeriesLoadResult = {
                dataSource: configSeries.eventsList!,
                eventsData: eventData,
                numberLoaded: loaded,
                numberSkipped: skipped,
                retrieveTime: finishTimer(startTime),
                series: seriesMetadata,
              };
              resolve(result);
            }
          } catch (err) {
            reject(err);
          }
        })
        .catch((err: Error) => {
          reject(err);
        });
    });
    return prData;
  }

  public get allSeriesList(): SeriesDetail[] {
    const result: SeriesDetail[] = [];
    this.config.series.forEach((s: ConfigSeries) => {
      const sd: SeriesDetail = this.getSeriesDetailFromConfig(s);
      result.push(sd);
    });
    return result;
  }

  public getSeriesDataList(): Promise<SeriesDataDetail[]> {
    const prResults: Promise<SeriesDataDetail>[] = [];

    this.config.series.forEach((s: ConfigSeries) => {
      prResults.push(this.getSeriesDataDetailFromConfig(s));
    });

    return Promise.all(prResults);
  }

  public get seriesList(): SeriesDetail[] {
    const result: SeriesDetail[] = [];
    this.config.series.forEach((s: ConfigSeries) => {
      const sd: SeriesDetail = this.getSeriesDetailFromConfig(s);
      if (sd.enabled) {
        result.push(sd);
      }
    });
    return result;
  }

  buildNamedMapFromPointSets(namedSet: NamedPointsMap,
    scoringSystem: Map<number | string, number>,
    name: string): void {
    namedSet.set(name, scoringSystem);
  }

  getPointsForSeries(configSeries: ConfigSeries): Promise<NamedPointsMap> {
    if (!this.hasPointsConfigured(configSeries)) {
      const si: string = seriesIdentifier(configSeries);

      const errMsg = `No points file for series ${si}`;
      console.trace(errMsg);
      return Promise.reject(new Error(errMsg));
    }

    if (!this._seriesPointsMaps.has(configSeries.id)) {
      if (configSeries.pointsSystems !== undefined) {
        const namedSet: NamedPointsMap = new Map<string, PointsMap>();

        const prList: Promise<Map<number | string, number>>[] = [];

        Object.keys(configSeries.pointsSystems).forEach((name: string) => {
          const path:string = configSeries.pointsSystems[name];
          const namedList: Promise<Map<number | string, number>> = new Promise((resolve, reject) => {
            this.dataFileController
              .dataFileAsJson(path)
              .then((pointsJsonData: JSON[]) => {
                const scoringSystem: Map<number | string, number> = orderPointsLookup(pointsJsonData);
                // this._seriesPointsMaps.set(configSeries.id, scoringSystem);
                this.buildNamedMapFromPointSets(namedSet, scoringSystem, name);
                resolve(scoringSystem);
              })
              .catch((err) => reject(err));
          });
          prList.push(namedList);
        });

        const prNameList: Promise<NamedPointsMap> = new Promise<NamedPointsMap>((resolve, reject) => {
          Promise.all(prList).then((_: Map<number | string, number>[]) => {
            resolve(namedSet);
          }).catch((reason) => {
            reject(reason);
          });
        });
        return prNameList;
      } else {
        const defaultList: Promise<NamedPointsMap> = new Promise((resolve, reject) => {
          // promise on default, then promise.all on others
          this.dataFileController
            .dataFileAsJson(configSeries.points!)
            .then((pointsJsonData: JSON[]) => {
              const scoringSystem: Map<number | string, number> = orderPointsLookup(pointsJsonData);
              const namedSet: NamedPointsMap = new Map<string, PointsMap>();
              namedSet.set('default', scoringSystem);
              resolve(namedSet);
            })
            .catch((err) => reject(err));
        });
        return defaultList;
      }
    } else {
      return Promise.resolve(this._seriesPointsMaps.get(configSeries.id)!);
    }
  }

  public getPointsAllocations(): Promise<NamedPointsMap[]> {
    const prList: Promise<NamedPointsMap>[] = [];

    this.config.series.forEach((configSeries: ConfigSeries) => {
      const prPoints: Promise<NamedPointsMap> = this.getPointsForSeries(configSeries);

      prList.push(prPoints);
    });

    return Promise.all(prList);
  }
  private _pointsAllocations: Map<string | number, number>[] = [];

  public convertDataToSeriesResult(seriesData: SeriesDataDetail, dateRange: DateRange): Promise<SeriesResult> {
    assert(seriesData.categories.length > 0, 'Can\'t convert data to a series with no categories.');
    assert(seriesData != undefined, 'Can\'t convert seriesData to result for undefined SeriesDataDetail');
    assert(
      this.eventDataController != undefined,
      'EventDataController not set when calling SeriesDataController.convertDataToSeriesResult'
    );
    assert(
      this.eventFilterController != undefined,
      'EventFilterController not set when calling SeriesDataController.convertDataToSeriesResult'
    );


    if (seriesData.eventIdList == undefined) {
      return Promise.reject(new Error('Series has no event id list'));
    }
    const cs: CategorySet = this.categoryController.createCategorySet(seriesData.categories);

    const outputSeries: Series = new Series(cs);
    outputSeries.SeriesDataDetail = seriesData;

    const pr: Promise<SeriesResult> = new Promise((resolve, reject) => {
      this.eventDataController.requestEvents(seriesData.eventIdList!)
        .then((results: Map<string, EventDataDetail>) => {
          results.forEach((eventData) => {
            try {
              console.log(`ID: ${eventData.id}`);
              if (!this.shouldSkipEventLoad(eventData, seriesData, dateRange)) {
                this.safeAddEventDataToSeries(outputSeries, eventData);
              }
            } catch (err) {
              reject(err);
            }
          });
          const sr: SeriesResult = convertSeriesToResult(outputSeries);

          resolve(sr);
        })
        .catch((err) => {
          reject(err);
        });
    });
    return pr;
  }

  safeAddEventDataToSeries(outputSeries: Series, eventData: EventDataDetail) {
    assert(
      eventData.eventId != undefined,
      `EventID was undefined when adding riders to series in event ${eventData.name} (${eventData.eventId}).`
    );

    assert(eventData.eventId! > 0, `EventID was <0 when adding riders to series in event ${eventData.name}.`);
    assert(!outputSeries.hasEvent(eventData.eventId!), `Event id ${eventData.eventId} already exists in event array`);

    assert(outputSeries.HasSeriesDetail, 'Output series must have seriesInfo data to be created');

    const seriesEvent: SeriesEventFile = this.convertEventMetadataForSeries(eventData);

    outputSeries.setEvent(eventData.eventId, seriesEvent);
    // outputSeries.events[eventData.eventId!] = seriesEvent;
    const eventEntrants: EventEntrant[] = eventData.eventRawData;

    this.addEventEntriesToSeries(outputSeries, eventEntrants, eventData.eventId!);
  }
}

function createEventResult(eventId: number, entry: EventEntrant): EventResult | undefined {
  if (eventId == undefined || eventId < 0) {
    throw new Error('eventId must be >0');
  }
  if (!entry) {
    console.warn('No entry row.');
    return undefined;
  }
  if (entry.Result) {
    const result: EventResult = {
      Event: eventId,
      Result: entry.Result,

    };
    if (entry.IsIneligible) {
      result.IsIneligible = true;
    }
    return result;
  } else {
    console.warn('Entry ' + JSON.stringify(entry) + ' does not have a Result.');
    return undefined;
  }
}

function splitNameTo(name: string, entry: Entrant): void {
  let fixedName = name.trim();
  while (fixedName.indexOf('  ') > -1) {
    fixedName = fixedName.replace('  ', ' ');
  }
  const parts = fixedName.split(' ', 2);
  if (parts.length > 1) {
    entry.Firstname = parts[0].trim();
    entry.Surname = parts[1].trim().toUpperCase();
  } else if (parts.length > 0) {
    entry.Surname = parts[0].trim().toUpperCase();
    const logMsg = `Name provided for ${name} had no spaces so can't ' +
  'split to firstname/surname - used whole value as Surname`;
    console.warn(logMsg);
  } else {
    throw new Error(`Name provided for ${name} had no parts so have no data to use as name.`);
  }
}

function getPointsFactorFromEvent(events: SeriesEventFile[], eventId:number) {
  const seriesEventFile: SeriesEventFile = events[eventId];
  const pf: number | undefined = seriesEventFile.pointsFactor;
  if (pf == undefined) {
    return 1;
  }
  return pf;
}

function getResultPointsFromScoringSystem(scoringSystem: PointsMap, rank: number|string):number {
  if (scoringSystem.has(rank)) {
    const rankPoints: number | undefined = scoringSystem.get(rank);
    if (rankPoints === undefined) {
      throw new Error(`Rank can't appear in scoring system for ${rank} because lookup value was undefined`);
    }
    return rankPoints;
  }

  if (typeof rank === 'string' && scoringSystem.has(parseInt(rank))) {
    const rankPoints: number | undefined = scoringSystem.get(parseInt(rank));
    if (rankPoints == undefined) {
      throw new Error(`Rank can't appear in scoring system for int ${rank} because lookup value was undefined`);
    }
    return rankPoints;
  }
  return 0;
}

function getPointsSystemForEvent(scoringSystem: NamedPointsMap, events: SeriesEventFile[], eventId:number): PointsMap {
  const seriesEventFile: SeriesEventFile = events[eventId];
  if (seriesEventFile.pointsSystem && scoringSystem.has(seriesEventFile.pointsSystem)) {
    return scoringSystem.get(seriesEventFile.pointsSystem)!;
  }
  return scoringSystem.get('default')!;
}

function allocatePointsToEntrantResult(
  result:EventResult,
  scoringSystems: NamedPointsMap,
  events: SeriesEventFile[]
):number {
  if (result.IsIneligible) {
    result.IsIneligible = true;
    result.Points = 0;
    return 0;
  }
  if (scoringSystems == undefined || scoringSystems.size == 0) {
    throw new Error('No scoring systems defined, including default');
  }
  if (result) {
    const scoringSystem: PointsMap = getPointsSystemForEvent(scoringSystems, events, result.Event);
    if (scoringSystem === undefined) {
      const pointsSystemToUse: string|undefined = events[result.Event].pointsSystem;
      if (pointsSystemToUse === '' || pointsSystemToUse === undefined) {
        throw new Error(`Couldn't find points system for event ${result.Event}`);
      } else {
        throw new Error(`Couldn't find points system ${pointsSystemToUse} for event ${result.Event}`);
      }
    }
    const pointsForResult: number = getResultPointsFromScoringSystem(scoringSystem, result.Result);
    const pf: number = getPointsFactorFromEvent(events, result.Event);
    const points = pointsForResult * pf;
    result.Points = points;
    return points;
  }
  return 0;
}

function allocatePointsFromMaxRounds(maxRounds:number, pointsArr:number[], entrant:SeriesEntrant):void {
  pointsArr = pointsArr.sort();
  pointsArr = pointsArr.reverse();
  let pointsTotal = 0;
  for (let k = 0; k < pointsArr.length && k < maxRounds; k++) {
    pointsTotal += pointsArr[k];
  }
  entrant.Points = pointsTotal;
}

function allocatePointsToEntrant(series:Series, entrant:SeriesEntrant, maxRounds:number):void {
  const pointsArr:number[] = [];
  const scoringSystems: NamedPointsMap|undefined = series.ScoringSystems;
  if (scoringSystems == undefined) {
    throw new Error('Scoring systems undefined when allocating points.');
  }
  const events: SeriesEventFile[] = series.Events;

  for (let j = 0; j < entrant.results.length; j++) {
    const result: EventResult = entrant.results[j];

    const allocatedPoints:number = allocatePointsToEntrantResult(result, scoringSystems, events);
    pointsArr.push(allocatedPoints);
  }
  allocatePointsFromMaxRounds(maxRounds, pointsArr, entrant);
}

function assignPointsToEntrants(series:Series, entrants:SeriesEntrant[], maxRounds:number):void {
  for (let i = 0; i < entrants.length; i++) {
    const entrant: SeriesEntrant = entrants[i];
    allocatePointsToEntrant(series, entrant, maxRounds);
  }
}

function allocatePoints(series: Series): void {
  assert(series.HasScoringSystem, 'Scoring system must be configured to allocate points against series.');
  assert(series.EntrantCount > 0, 'Must have >0 entrants to allocate series points.');
  const MAX_ROUNDS = 12;

  series.CategoryEntrants.forEach((entrants:SeriesEntrant[]) => {
    assignPointsToEntrants(series, entrants, MAX_ROUNDS);
  });
}

function orderPointsLookup(pointsData: any[]): Map<string | number, number> {
  const orderedPoints: Map<string | number, number> = new Map<string | number, number>();
  for (let i = 0; i < pointsData.length; i++) {
    const pd = pointsData[i];
    orderedPoints.set(pd.Rank, pd.Points);
  }
  return orderedPoints;
}

function seriesAllowsDateOverride(series: ConfigSeries | SeriesDetail): boolean {
  if (series.filter) {
    if (series.filter.FromDate || series.filter.ToDate) {
      return false;
    }
  }
  return true;
}

function compareByPoints(e1:SeriesEntrant, e2:SeriesEntrant):number {
  const e1p:number = e1 == undefined || e1.Points == undefined ? 0 : e1.Points;
  const e2p:number = e2 == undefined || e2.Points == undefined ? 0 : e2.Points;

  return e2p - e1p;
}

function sortBySeriesPoints(entrants:SeriesEntrant[]):SeriesEntrant[] {
  return entrants.sort(compareByPoints);
}

function sortCategoryEntrants(entrants:SeriesEntrant[][]):SeriesEntrant[][] {
  const copiedList:SeriesEntrant[][] = [];
  entrants.forEach((categoryEntrants:SeriesEntrant[]) => {
    const sortedEntrants:SeriesEntrant[] = sortBySeriesPoints(categoryEntrants);
    copiedList.push(sortedEntrants);
  });
  return copiedList;
}

function convertSeriesToResult(series: Series): SeriesResult {
  assert(series.HasScoringSystem, 'Series requires a scoringSystem to be converted to a SeriesResult; was undefined');
  assert(series.HasSeriesDetail, 'Can\'t convert an incomplete Series structure to a result - missing SeriesInfo');
  assert(
    series.EntrantCount > 0,
    'Can\'t possibly have >0 entrants if there are no events in a series - series data must be malformed'
  );
  const scoringSystem: Object = series.ScoringSystems ? Object.fromEntries(series.ScoringSystems) : {};

  console.log(`convertSeriesToResult: Series had ${series.Events.length} events`);
  console.debug(series.Events);
  allocatePoints(series);

  const output: SeriesResult = {
    // entrants: series.Entrants,
    categoryEntrants: sortCategoryEntrants(series.CategoryEntrants),
    events: convertEventFilesToSeriesResultEvents(series.Events),
    scoringSystem: Object.entries(scoringSystem),
    ...series.SeriesDetail
  };
  assert(scoringSystem.hasOwnProperty('NaN') == false);

  return output;
}

export const convertEventFilesToSeriesResultEvents = (events: SeriesEventFile[]): SeriesEvent[] => {
  return events.map((seriesEventFile: SeriesEventFile) => {
    const event: SeriesEvent = {
      championshipStatus: seriesEventFile.championshipStatus,
      date: seriesEventFile.date,
      format: seriesEventFile.format,
      id: seriesEventFile.id,
      isFinal: seriesEventFile.isFinal,
      location: seriesEventFile.location,
      name: seriesEventFile.name,
      organiser: seriesEventFile.organiser,
      pointsFactor: seriesEventFile.pointsFactor,
      pointsSystem: seriesEventFile.pointsSystem,
      state: seriesEventFile.state,
      subtitle: seriesEventFile.subtitle,
    };

    return event;
  });
};
