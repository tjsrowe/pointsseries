'use strict';

import * as XLSX from 'xlsx';
import { ExcelFileUtils } from '../src/ExcelFileUtils';
import { JsonUtils } from '../src/jsonUtils';
import StringUtils from '../src/StringUtils';

export const DEFAULT_SHEET_NAME_ORDER: string[] = ['ResultsImport', 'ResultImport', 'Sheet1'];

type ExcelFileLoadErrorTypes = {
  message: string;
  file?: string;
  worksheet?: string;
};

class ExcelFileLoadError extends Error {
  private _file?: string;
  private _sheet?: string;
  constructor({ message, file, worksheet }: ExcelFileLoadErrorTypes) {
    super(message);
    if (file) {
      this._file = file;
    }
    if (worksheet) {
      this._sheet = worksheet;
    }
  }
  get file(): string | undefined {
    return this._file;
  }

  get sheet(): string | undefined {
    return this._sheet;
  }
}

export type ExcelFileOptions = {
  Header?: string;
  Worksheet: string | undefined;
};

export class ExcelDataFileController {
  getWorksheetName(filePath: string, options?: ExcelFileOptions): string | undefined {
    let worksheet: string | undefined;

    if (!options || !options.Worksheet) {
      if (ExcelFileUtils.excelFilenameHasSheet(filePath)) {
        worksheet = ExcelFileUtils.getExcelSheetFromPath(filePath);
        filePath = ExcelFileUtils.getExcelFilenameForPath(filePath);
      } else {
        const defaultSheet: string | undefined = this.getSheetFromDefaultList(filePath);
        if (!defaultSheet) {
          throw new Error(`No default sheet found with sheet name to load not specified from ${filePath}`);
        }
        worksheet = defaultSheet;
      }
    } else if (options) {
      worksheet = options.Worksheet;
    }
    return worksheet;
  }

  getSheetFromDefaultList(filePath: string): string | undefined {
    const sheets: string[] = ExcelFileUtils.getSheetsFromFile(filePath);
    let matchingSheet: string | undefined;
    DEFAULT_SHEET_NAME_ORDER.forEach(async (sheet) => {
      if (matchingSheet == undefined && sheets.includes(sheet)) {
        matchingSheet = sheet;
      }
    });
    return matchingSheet;
  }

  getSheetFromFileDetails(filePath: string, options?: ExcelFileOptions): XLSX.WorkSheet | undefined {
    const worksheet: string | undefined = this.getWorksheetName(filePath, options);
    const filenameOnly: string = ExcelFileUtils.getExcelFilenameForPath(filePath);

    const workbook: XLSX.WorkBook = XLSX.readFile(filenameOnly);
    let sheet: XLSX.WorkSheet | undefined;
    if (worksheet == undefined) {
      const sheetKeys = Object.keys(workbook.Sheets);
      sheet = workbook.Sheets[sheetKeys[0]];
    } else if (workbook.Sheets[worksheet]) {
      sheet = workbook.Sheets[worksheet];
    } else {
      for (let i = 0; i < Object.keys(workbook.Sheets).length; i++) {
        const checkWorksheet = workbook.Sheets[i];
        if (checkWorksheet && checkWorksheet.SheetNames[i] == worksheet) {
          sheet = workbook.Sheets[i];
          break;
        }
      }
    }
    return sheet;
  }

  xlsxAsJson(filePath: string, options?: ExcelFileOptions): Promise<JSON> {
    const returnPromise = new Promise<JSON>((resolve, reject) => {
      const worksheetName: string | undefined = this.getWorksheetName(filePath, options);
      const sheet: XLSX.WorkSheet | undefined = this.getSheetFromFileDetails(filePath, options);
      let header = undefined;

      if (options && options.Header) {
        header = options.Header;
      }

      if (sheet) {
        let json: any;
        if (header) {
          const letters: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'];
          const rows = [];
          for (let j = 1; j < 4096; j++) {
            const row = [];
            const obj: any = {};
            for (let r = 0; r < letters.length; r++) {
              const col: string = letters[r];
              const cell: string = col + j;
              if (!sheet[cell]) {
                break;
              }
              const value = sheet[cell].v;

              const headerKey = StringUtils.removeSpaces(header[r]);
              obj[headerKey] = value;

              row.push(value);
            }
            if (row.length > 0) {
              // rows.push(row);
              rows.push(obj);
            }
          }
          sheet.ref = 'A1:G1200';
          json = rows;
        } else {
          json = XLSX.utils.sheet_to_json(sheet);
          JsonUtils.removeSpacesFromKeys(json);
        }
        json.Columns = JsonUtils.getColumnsUsed(json);
        resolve(json);
      } else {
        const err: ExcelFileLoadError = new ExcelFileLoadError({
          file: filePath,
          message: `In xlsxAsJson, no worksheet '${worksheetName}' found in file ${filePath}`,
          worksheet: worksheetName,
        });
        reject(err);
      }
    });
    return returnPromise;
  }
}
