'use strict';

import { SeriesDataDetail, SeriesDetail, SeriesResult } from './src/model/series';

import { EventDDB } from './controller/dynamo/EventDDB';
import { Filter } from './config/Config';
import { ISO8601Date } from './src/isoDateStringToDate';
import { SeriesDDB } from './controller/dynamo/SeriesDDB';
import { SeriesEvent } from './src/model/event';

export type DateRange = {
  fromDate?: ISO8601Date;
  toDate?: ISO8601Date;
};

export const ALL_DATES:DateRange = {};

export type EntrantResult = {
  Event: number;
  Result: string | number;
  Points: number;
};

export type CategoryResultEntry = {
  Firstname: string;
  Surname: string;
  Points: number;
  results: EntrantResult[];
};

export type CategoryResult = {
  name: string;
  entrants: CategoryResultEntry[];
};

export type { Filter, ISO8601Date, SeriesDetail, SeriesEvent, SeriesResult, SeriesDataDetail };
export type { EventDDB, SeriesDDB };
